using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Com.OblivionWorkshop{
    [CustomEditor(typeof(AppGM))]
    public class CustomAppGMButton : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            AppGM myScript = (AppGM)target;
            if (GUILayout.Button("Clear Prefs"))
            {
                myScript.ClearPrefs();
            }
            if (GUILayout.Button("Delete Game Data"))
            {
                myScript.DeleteGameData();
            }
        }

    }
}
