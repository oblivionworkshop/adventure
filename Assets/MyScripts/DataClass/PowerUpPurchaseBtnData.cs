using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public class PowerUpPurchaseBtnData
    {
        public CommonButton btn;
        public IEnum.PowerUp powerUp;
        public int cost;
        public short slotID;
        public bool canBuy;

        public PowerUpPurchaseBtnData(CommonButton _btn, IEnum.PowerUp _powerUp, int _cost, short _slotID, bool _isPositive)
        {
            this.btn = _btn;
            this.powerUp = _powerUp;
            this.cost = _cost;
            this.slotID = _slotID;
            this.canBuy = _isPositive;
        }
    }
}
