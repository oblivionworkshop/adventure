using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.OblivionWorkshop.StartPage;

namespace Com.OblivionWorkshop
{
    public class LoginErrorPopup : MonoBehaviour
    {
        PopupAnim popUpAnim;
        [SerializeField]
        PopupCloseButton closeBtn;
        [SerializeField]
        TMP_Text title, content;

        private void Awake()
        {
            popUpAnim = GetComponent<PopupAnim>();
        }

        public void BasicSetup()
        {

        }

        public void SettingsBeforeOpen(string errorMsg)
        {
            content.text = errorMsg;
        }


        #region Getter
        public PopupAnim GetAnim()
        {
            return popUpAnim;
        }

        public PopupCloseButton GetCloseBtn()
        {
            return closeBtn;
        }
        #endregion
    }
}
