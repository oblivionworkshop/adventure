using CBS.UI;
using CBS.Models;
using CBS.Scriptable;
using UnityEngine;
using TMPro;
using Com.OblivionWorkshop.StartPage;

namespace Com.OblivionWorkshop
{
    public class AdventureLoginForm : MonoBehaviour
    {
        public static AdventureLoginForm instance;

        /*[SerializeField]
        private InputField MailInput;
        [SerializeField]
        private InputField PasswordInput;*/
        [SerializeField]
        private TMP_InputField CustomIDInput;

        [Space(10f)]
        [SerializeField]
        private CommonButton googleBtn;
        [SerializeField]
        private CommonButton guestBtn, customIDBtn;
        [SerializeField]
        private LoginErrorPopup errorPopup;

        public UnityEngine.Events.UnityAction<bool> allowInputAct;
        public UnityEngine.Events.UnityAction<bool, bool, bool> showLoadingAct;

        public event UnityEngine.Events.UnityAction<CBSLoginResult> OnLogined;

        private CBS.IAuth Auth { get; set; }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            Auth = CBS.CBSModule.Get<CBS.CBSAuthModule>();
        }

        private void OnDisable()
        {
            Clear();
        }

        public void BasicSetup()
        {
            googleBtn.clickAct = DefaultBtnClick;
            googleBtn.afterClickAct = () => { allowInputAct(true); };
            googleBtn.ButtonSetup();

            guestBtn.clickAct = DefaultBtnClick;
            guestBtn.afterClickAct = OnLoginWithDeviceID;
            guestBtn.ButtonSetup();

            customIDBtn.clickAct = DefaultBtnClick;
            customIDBtn.afterClickAct = OnLoginWithCustomID;
            customIDBtn.ButtonSetup();

            errorPopup.BasicSetup();
        }

        // button click
        /*public void OnLoginWithMail()
        {
            if (InputValid())
            {
                // get credentials
                string mail = MailInput.text;
                string password = PasswordInput.text;
                // start login
                var loginRequest = new CBSMailLoginRequest
                {
                    Mail = mail,
                    Password = password
                };
                // login request
                new PopupViewer().ShowLoadingPopup();
                Auth.LoginWithMailAndPassword(loginRequest, OnUserLogined);
            }
            else
            {
                // show error message
                new PopupViewer().ShowSimplePopup(new PopupRequest
                {
                    Title = AuthTXTHandler.ErrorTitle,
                    Body = AuthTXTHandler.InvalidInput
                });
            }
        }*/

        public void OnLoginWithDeviceID()
        {
            showLoadingAct(true, false, false);
            Auth.LoginWithDevice(OnUserLogined);
        }

        public void OnLoginWithCustomID()
        {
            string customID = CustomIDInput.text;
            if (!string.IsNullOrEmpty(customID))
            {
                showLoadingAct(true, false, false);
                Auth.LoginWithCustomID(customID, OnUserLogined);
            }
        }

        private void OnUserLogined(CBSLoginResult result)
        {
            if (result.IsSuccess)
            {
                // goto main screen
                gameObject.SetActive(false);
                OnLogined?.Invoke(result);
            }
            else
            {
                // show error message
                showLoadingAct(false, false, false);

                // **** Need handle Error Popup
                AppGM.instance.PlaySFX(IEnum.AudioList.popupOpen);
                errorPopup.SettingsBeforeOpen(result.Error.Message);


                errorPopup.gameObject.SetActive(true);

                errorPopup.GetAnim().afterOpenAnim = null;
                errorPopup.GetAnim().afterOpenAnim = () => { allowInputAct(true); };
                errorPopup.GetAnim().Open();

                errorPopup.GetCloseBtn().clickAct = DefaultBtnClick;
                errorPopup.GetCloseBtn().afterClickAct = CloseLoginErrorPopup;
                errorPopup.GetCloseBtn().ButtonSetup();
            }
        }

        void CloseLoginErrorPopup()
        {
            errorPopup.GetAnim().afterCloseAnim = null;
            errorPopup.GetAnim().afterCloseAnim = () => {
                errorPopup.GetAnim().gameObject.SetActive(false);
                allowInputAct(true);
            };
            errorPopup.GetAnim().Close();
            AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
        }

        /*public bool IsSubscribeToLogin()
        {
            return OnLogined.GetInvocationList().Length != 0;
        }

        public void OnRegistration()
        {
            var registrationPrefab = AuthUIData.RegisterForm;
            UIView.ShowWindow(registrationPrefab);
            HideWindow();
        }

        public void OnFogotPassword()
        {
            var recoveryPrefab = AuthUIData.RecoveryForm;
            UIView.ShowWindow(recoveryPrefab);
            HideWindow();
        }*/

        // check if inputs is not null
        /*private bool InputValid()
        {
            bool mailValid = !string.IsNullOrEmpty(MailInput.text);
            bool passwordValid = !string.IsNullOrEmpty(PasswordInput.text);
            return mailValid && passwordValid;
        }*/

        // reset view
        private void Clear()
        {
            /*MailInput.text = string.Empty;
            PasswordInput.text = string.Empty;*/
            CustomIDInput.text = string.Empty;
        }

        private void HideWindow()
        {
            gameObject.SetActive(false);
        }

        void DefaultBtnClick()
        {
            allowInputAct(false);
            AppGM.instance.PlaySFX(IEnum.AudioList.buttonClick);
        }
    }
}
