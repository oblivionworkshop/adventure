using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;

namespace Com.OblivionWorkshop
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField]
        Canvas loadingScreenCanvas;
        [SerializeField]
        Animator loadingAnim;
        public UnityEngine.Events.UnityAction<bool> afterLoadingCloseAnimAct;
        public UnityEngine.Events.UnityAction afterLoadingStartAnimAct;

        [SerializeField]
        UnityEngine.UI.Image loadingImg;
        [SerializeField]
        List<Sprite> loadingImgs = new List<Sprite>();

        #region Anim Callback
        /// <summary>
        /// Call from Animation Inspector
        /// </summary>
        [SkipRename]
        void AfterLoadingCloseAnim()
        {
            if (afterLoadingCloseAnimAct != null )
            {
                loadingScreenCanvas.gameObject.SetActive(false);
                afterLoadingCloseAnimAct(true);
            }
        }

        /// <summary>
        /// Call from Animation Inspector
        /// </summary>
        [SkipRename]
        void AfterLoadingStartAnim()
        {
            if (afterLoadingStartAnimAct != null)
            {
                afterLoadingStartAnimAct();
            }
        }
        #endregion

        public void StartLoading(bool startLoad, bool skipAnim = false)
        {
            if (startLoad)
            {
                loadingImg.sprite = loadingImgs[UnityEngine.Random.Range(0, loadingImgs.Count)];

                loadingScreenCanvas.gameObject.SetActive(true);
                if (!skipAnim)
                {
                    loadingAnim.SetTrigger("open");
                }
                else
                {
                    loadingAnim.Play("LoadingEnter", 0, 1f);
                }
                
                ResourceUI.instance.LoadingScreenTransition();
            }
            else
            {
                loadingAnim.SetTrigger("close");
            }
        }
    }
}
