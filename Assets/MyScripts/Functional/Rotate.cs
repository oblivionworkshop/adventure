﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public class Rotate : MonoBehaviour
    {
        Transform _transform;

        [SerializeField]
        Vector3 rotation;

        private void Start()
        {
            _transform = transform;
        }

        private void Update()
        {
            _transform.Rotate(rotation * Time.deltaTime);
        }
    }
}