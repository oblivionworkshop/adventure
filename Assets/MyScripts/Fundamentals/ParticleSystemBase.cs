using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public abstract class ParticleSystemBase : MonoBehaviour
    {
        public UnityEngine.Events.UnityAction afterParticleStopAct;

        private void OnParticleSystemStopped()
        {
            if (afterParticleStopAct != null)
            {
                afterParticleStopAct();
            }
        }
    }
}
