using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Beebyte.Obfuscator;

namespace Com.OblivionWorkshop{
    public class SingleRadioButton : ButtonBase
    {
        bool isPressed;

        public UnityAction<bool> afterPressAct;

        public override void ButtonSetup()
        {
            base.anim = gameObject.GetComponent<Animator>();
            base.btn = gameObject.GetComponent<Button>();

            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(CheckStatus);
        }

        void CheckStatus()
        {
            if (AppGM.instance.CanInput())
            {
                if (isPressed)
                {
                    Release();
                }
                else
                {
                    Press();
                }
            }
        }

        public void Press()
        {
            if (AppGM.instance.CanInput())
            {
                clickAct();
                anim.SetTrigger("press");
            }
        }

        public void Release()
        {
            if (AppGM.instance.CanInput())
            {
                clickAct();
                anim.SetTrigger("release");
            }
        }

        public void PressInstant()
        {
            isPressed = true;
            anim.Play("BtnPress", 0, 1f);
        }
        
        public void ReleaseInstant()
        {
            isPressed = false;
            anim.Play("BtnRelease", 0, 1f);
        }

        [SkipRename]
        protected override void AfterClickAnim()
        {
            base.AfterClickAnim();
            isPressed = !isPressed;
            if (isPressed)
            {
                afterPressAct(true);
            }
            else
            {
                afterPressAct(false);
            }
        }
    }
}
