using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
	public class SliderBase : MonoBehaviour
	{
        //protected Animator anim;
        protected Slider sld;

        public UnityAction<float> slideAct;

        public virtual void SliderSetup()
        {
            sld = gameObject.GetComponent<Slider>();

            sld.onValueChanged.RemoveAllListeners();
            sld.onValueChanged.AddListener(slideAct);
        }

        public void SetSliderValue(float val)
        {
            sld.value = val;
        }
    }
}
