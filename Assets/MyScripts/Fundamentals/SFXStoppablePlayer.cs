using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    public class SFXStoppablePlayer : MonoBehaviour
    {

        AudioSource sfxPlayer;
        bool isUsing;
        float expireTime = -1f;
        
        public void BasicSetup()
        {
            isUsing = false;

            sfxPlayer = gameObject.GetComponent<AudioSource>();
            if (!sfxPlayer)
            {
                sfxPlayer = gameObject.AddComponent<AudioSource>();
            }
        }

        public void PlayStoppableSFX(Sound sound, float playerSFXPrefVol, float _expireTime, bool mute, float _playbackTime, float? _pitch)
        {
            isUsing = true;
            expireTime = _expireTime;

            sfxPlayer.clip = sound.clip;
            sfxPlayer.volume = sound.volume * playerSFXPrefVol;
            sfxPlayer.pitch = _pitch.HasValue? _pitch.Value : sound.pitch;
            sfxPlayer.mute = mute;
            
            if (_playbackTime > 0f)
            {
                sfxPlayer.time = _playbackTime;
            }
            else
            {
                sfxPlayer.time = 0f;
            }

            sfxPlayer.Play();
            if (GamePage.GameGM.instance != null)
            {
                if (GamePage.GameGM.instance.GetGameState() != IEnum.GameState.playing)
                {
                    sfxPlayer.Pause();
                }
            }
            
            StartCoroutine(SFXExpire());
        }

        IEnumerator SFXExpire()
        {
            float timeCounter = 0f;
            int numCounter = 0;

            while (timeCounter < expireTime && numCounter < 200)
            {
                if (numCounter >= 200)
                {
                    Debug.LogError("numCounter in SFXExpire() in GameObject " + gameObject.name + " has over 200 need to immediately stop!");
                    yield break;
                }

                if ((GamePage.GameGM.instance != null && GamePage.GameGM.instance.GetGameState() == IEnum.GameState.playing) || GamePage.GameGM.instance == null)
                {
                    numCounter++;
                    timeCounter = timeCounter + 0.25f;
                    yield return new WaitForSeconds(0.25f);
                }
                else if (GamePage.GameGM.instance != null)
                {
                    if (GamePage.GameGM.instance.GetGameState() == IEnum.GameState.pause)
                    {
                        yield return null;
                    }
                    else if (GamePage.GameGM.instance.GetGameState() == IEnum.GameState.none || GamePage.GameGM.instance.GetGameState() == IEnum.GameState.gameOver)
                    {
                        timeCounter = expireTime;
                    }
                }

            }
            
            if (timeCounter >= expireTime)
            {
#if STOPPABLE_SFX_DEBUG
                Debug.Log("time counter = " + timeCounter + " for gameObject " + gameObject.name);
#endif
                ResetPlayer();
            }
        }

        void ResetPlayer()
        {
            isUsing = false;
            StopAllCoroutines();

            expireTime = -1f;
            sfxPlayer.clip = null;
        }

        public void PauseStoppableSFX(bool flag)
        {
            if (flag)
            {
                sfxPlayer.Pause();
            }
            else
            {
                sfxPlayer.UnPause();
            }
            
        }

        public void MuteStoppableSFX(bool flag)
        {
            sfxPlayer.mute = flag;
        }

        public void StopStoppableSFX()
        {
            sfxPlayer.Stop();
            ResetPlayer();
        }

        public bool IsUsing()
        {
            return isUsing;
        }
    }
}
