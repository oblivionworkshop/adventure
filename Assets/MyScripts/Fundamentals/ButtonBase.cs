using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Beebyte.Obfuscator;

namespace Com.OblivionWorkshop{
	public abstract class ButtonBase : MonoBehaviour
	{
        protected Animator anim;
        protected Button btn;

        public UnityAction clickAct, afterClickAct;

        public virtual void ButtonSetup()
        {
            anim = gameObject.GetComponent<Animator>();
            btn = gameObject.GetComponent<Button>();

            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(Click);
        }

        protected void Click()
        {
            if (AppGM.instance.CanInput())
            {
                clickAct();
                if (anim)
                {
                    anim.SetTrigger("click");
                }
            }
        }

        protected void Clear()
        {
            anim = gameObject.GetComponent<Animator>();
            btn = gameObject.GetComponent<Button>();

            btn.onClick.RemoveAllListeners();
        }

        #region Anim Callback
        [SkipRename]
        protected virtual void AfterClickAnim()
        {
            if (afterClickAct != null)
            {
                afterClickAct();
            }
            else
            {
                Debug.LogWarning("afterClickAct is null and perform nothing!");
            }
        }
        #endregion
    }
}
