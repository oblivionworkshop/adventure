﻿namespace Com.OblivionWorkshop
{
    public class IEnum
    {
        public enum GameMode
        {
            none = -1,
            practice = 0,
            traditional = 1,
            challenge = 2,
        }

        public enum GameState
        {
            none = 0,
            playing = 1,
            pause = 2,
            gameOver = 3,
        }

        public enum Direction
        {
            none = -1,
            north = 0,
            east = 1,
            south = 2,
            west = 3,
            northEast = 4,
            southEast = 5,
            southWest = 6,
            northWest = 7,
        }

        public enum BulletType
        {
            sting = 0,
            bomb = 1,
        }

        public enum CharacterType
        {
            none = 0,
            rabbit = 1,
        }

        public enum PowerUp
        {
            none = 0,
            carrotTrio = 1,
            bubblyShield = 2,
            extraHop = 3,
        }

        public enum SceneName
        {
            none = 0,
            AdventureLogin = 1,
            StartPage = 2,
            Game = 3,
        }

        public enum AudioList
        {
            none = 0,
            startPageTitle = 1,
            gamePageNormal_1 = 2,
            gamePageNormal_2 = 3,
            gamePageDifficult_1 = 4,
            gamePageDifficult_2 = 5,
            gamePageDifficult_3 = 6,
            buttonClick = 101,
            alert = 102,
            passSubLevel = 103,
            passLevel = 104,
            gameOver1 = 105,
            gameOver2 = 106,
            dash1 = 107,
            dash2 = 108,
            dash3 = 109,
            dash4 = 110,
            eatPop = 111,
            eatPowerup = 112,
            dieFromTrap = 113,
            blockOnceBreak = 114,
            bounceBouncy = 115,
            bounceStatic = 116,
            revive = 117,
            buttonError = 118,
            boostFullNotification = 119,
            getCoin = 120,
            popupOpen = 121,
            popupClose = 122,
        }
    }
}