﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public interface IHitable
    {
        void HitEffect();
    }
}
