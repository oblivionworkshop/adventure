using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
	public class BulletBase : TrapBase
	{
        [Header("Bullet Base")]
        [SerializeField]
        float bulletSpeed;
        [SerializeField]
        float expireDistance;
        [SerializeField]
        float expireTime;
        
        float spawnStartTime, timeCounter;
        Vector3 spawnPos;
        float distanceCounter;

        protected int id;

        public UnityAction<bool> bulletExpireAct;

        protected override void Awake()
        {
            base.Awake();

            spawnStartTime = Time.time;
            timeCounter = spawnStartTime;

            spawnPos = _transform.position;

            if (rd == null)
            {
                Debug.LogWarning("Cannot Find compulsory RD for Bullet! Add one immediately!");
                rd = gameObject.AddComponent<Rigidbody2D>();
                rd.bodyType = RigidbodyType2D.Kinematic;
                rd.simulated = true;
            }
        }

        protected override void Update()
        {
            base.Update();
            
            if (IsPlayingAndAlive())
            {
                //_transform.TransformDirection(Vector3.up) represents the local Vector3.up;
                rd.MovePosition(_transform.position + (_transform.TransformDirection(Vector3.up) * bulletSpeed * Time.deltaTime));

                //timeCounter
                if (expireTime > 0f)
                {
                    timeCounter += Time.deltaTime;
                    if ((timeCounter - spawnStartTime) >= expireTime)
                    {
                        if (GamePage.GameGM.instance.DetermineIfColInsideCamera(base.col))
                        {
                            // when Time is up but the bullet is still inside Camera frustum, extend its life again
                            expireTime = expireTime + 5f;
                        }
                        else
                        {
                            BulletExpire(false);
                        }
                    }
                }

                //distanceCounter
                if (expireDistance > 0f)
                {
                    distanceCounter = Vector3.Distance(spawnPos, _transform.position);
                    if (distanceCounter >= expireDistance)
                    {
                        if (GamePage.GameGM.instance.DetermineIfColInsideCamera(base.col))
                        {
                            // when Distance is enough but the bullet is still inside Camera frustum, extend its life again
                            expireDistance = expireDistance + 20f;
                        }
                        else
                        {
                            BulletExpire(false);
                        }
                    }
                }

            }
        }

        public void BasicSetup(float _speed, float _distance = 30f, float _time = 0f)
        {
            bulletSpeed = _speed;
            expireDistance = _distance;
            expireTime = _time;

            SetTransformVar();
        }

        public void SetID(int _id)
        {
            id = _id;
        }

        public BounceDataClass DetermineIfCanStepOn(Vector2 normal)
        {
            float angle = (_transform.eulerAngles.z) % 360f;
            float perpenX = Vector2.Perpendicular(_transform.up).x;
            float perpenY = Vector2.Perpendicular(_transform.up).y;
            float perpenNegX = -perpenX;
            float perpenNegY = -perpenY;

            float backX = -_transform.up.x;
            float backY = -_transform.up.y;

            float diffPerpenX = Mathf.Abs(perpenX - normal.x);
            float diffPerpenY = Mathf.Abs(perpenY - normal.y);
            float diffBackX = Mathf.Abs(backX - normal.x);
            float diffBackY = Mathf.Abs(backY - normal.y);
            float diffPerpenNegX = Mathf.Abs(perpenNegX - normal.x);
            float diffPerpenNegY = Mathf.Abs(perpenNegY - normal.y);

            bool diffAcceptPX = diffPerpenX <= 0.1f;
            bool diffAcceptPY = diffPerpenY <= 0.1f;
            bool diffAcceptBX = diffBackX <= 0.1f;
            bool diffAcceptBY = diffBackY <= 0.1f;
            bool diffAcceptPNegX = diffPerpenNegX <= 0.1f;
            bool diffAcceptPNegY = diffPerpenNegY <= 0.1f;

            IEnum.Direction direction;

            Debug.Log("diffAcceptPX = " + diffAcceptPX + ", diffAcceptPY = " + diffAcceptPY + ", diffAcceptBX = " + diffAcceptBX + ", diffAcceptBY = " + diffAcceptBY);

            if ((angle > -1f && angle < 1f) || (angle > 359f && angle <= 360f))
            {
                // bullet point to north
                direction = IEnum.Direction.north;
#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif

                if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(false, true, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -91f && angle < -89f) || (angle > 269f && angle < 271f))
            {
                // bullet point to east
                direction = IEnum.Direction.east;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif

                if (diffAcceptPX && diffAcceptPY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(false, true, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -181f && angle < -179f) || (angle > 179f && angle < 181f))
            {
                // bullet point to south
                direction = IEnum.Direction.south;
#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif

                if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -271f && angle < -269f) || (angle > 89f && angle < 91f))
            {
                // bullet point to west
                direction = IEnum.Direction.west;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif
                if (IsStraightFall(normal))
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(false, true, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -46f && angle < -44f) || (angle > 314f && angle < 316f))
            {
                // bullet point to north east
                direction = IEnum.Direction.northEast;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif

                if (diffAcceptPX && diffAcceptPY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if ((normal.y > -0.1f && normal.y < 0.1f) && (normal.x > -1.1f && normal.x < -0.9f))
                {
                    // hit from left to right
                    return new BounceDataClass(false, true, false, direction);
                }
                else if ((normal.y > -1.1f && normal.y < -0.9f) && (normal.x > -0.1f && normal.x < 0.1f))
                {
                    // hit from bot to top
                    return new BounceDataClass(false, true, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(false, true, false, direction);
                }
                else if (IsStraightFall(normal))
                {
                    return new BounceDataClass(false, false, true, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -136f && angle < -134f) || (angle > 224f && angle < 226f))
            {
                // bullet point to south east
                direction = IEnum.Direction.southEast;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif
                if (IsStraightFall(normal))
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptPX && diffAcceptPY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -226f && angle < -224f) || (angle > 134f && angle < 136f))
            {
                // bullet point to south west
                direction = IEnum.Direction.southWest;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif
                if (IsStraightFall(normal))
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if (diffAcceptPNegX && diffAcceptPNegY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if ((normal.y > -0.1f && normal.y < 0.1f) && (normal.x > 0.9f && normal.x < 1.1f))
                {
                    // hit from right to left
                    return new BounceDataClass(false, true, false, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else if ((angle > -316f && angle < -314f) || (angle > 44f && angle < 46f))
            {
                // bullet point to north west
                direction = IEnum.Direction.northWest;

#if _DEBUG_Trap
                Debug.Log("Direction = " + direction.ToString());
#endif
                
                if (diffAcceptPNegX && diffAcceptPNegY)
                {
                    return new BounceDataClass(true, false, false, direction);
                }
                else if ((normal.y > -0.1f && normal.y < 0.1f) && (normal.x > 0.9f && normal.x < 1.1f))
                {
                    // hit from right to left
                    return new BounceDataClass(false, true, false, direction);
                }
                else if ((normal.y > -1.1f && normal.y < -0.9f) && (normal.x > -0.1f && normal.x < 0.1f))
                {
                    // hit from bot to top
                    return new BounceDataClass(false, true, false, direction);
                }
                else if (diffAcceptBX && diffAcceptBY)
                {
                    return new BounceDataClass(false, true, false, direction);
                }
                else if (IsStraightFall(normal))
                {
                    return new BounceDataClass(false, false, true, direction);
                }
                else
                {
                    return new BounceDataClass(false, false, true, direction);
                }
            }
            else
            {
                Debug.LogError("Error Case!");
                return new BounceDataClass(false, true, false);
            }
            

        }

        bool IsStraightFall(Vector2 _normal)
        {
            if ((_normal.y > 0.9f && _normal.y < 1.1f) && (_normal.x > -0.1f && _normal.x < 0.1f))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function will return the used bullet to the pool. If you want to Destroy the bullet, use base.DestroyThis() instead.
        /// </summary>
        public virtual void BulletExpire(bool willDestroy)
        {
            // return the used bullet to the pool
            if (bulletExpireAct != null)
            {
                bulletExpireAct(willDestroy);
            }
            else
            {
                Debug.LogError("bulletExpireAct() is null! GameObject Name = " + gameObject.name);
            }
        }

        protected override void DestroyThis()
        {
            // Need to remove the bullet from bullet list before Destroy
            BulletExpire(true);
            //----------------------------------------------------

            // This should put at last
            base.DestroyThis();
        }

        #region Getter
        public int GetID()
        {
            return id;
        }
        #endregion

        #region Testing
        [ContextMenu("Test Vector")]
        void TestVector()
        {
            Vector2 vec = Vector2.Perpendicular(transform.up);
            Debug.Log(vec);
        }

        [ContextMenu("Test Vector 2")]
        void TestVector2()
        {
            float perpenX = Vector2.Perpendicular(transform.up).x;
            float perpenY = Vector2.Perpendicular(transform.up).y;

            float backX = -transform.up.x;
            float backY = -transform.up.y;

            Debug.Log("perpenX = " + perpenX + ", perpenY = " + perpenY + ", backX = " + backX + ", backY = " + backY);
        }
        #endregion
    }
}
