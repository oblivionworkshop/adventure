using System.Collections.Generic;
using System.Linq;

namespace Com.OblivionWorkshop{
    [System.Serializable]
	public class GameData
	{
        public Dictionary<string, bool> clearStage;
        public int highestLevel;
        public int highestSubLevel;

        public int coin;
        public Dictionary<string, int> powerUpInventory;

        public GameData()
        {
            clearStage = new Dictionary<string, bool>();
            highestLevel = 0;
            highestSubLevel = 0;
            coin = 0;
            powerUpInventory = new Dictionary<string, int>();
        }

        public bool CheckIsDiff(GameData src)
        {
            if (this.clearStage.Keys.Count == src.clearStage.Keys.Count && 
                this.clearStage.Keys.All(k => src.clearStage.ContainsKey(k) && object.Equals(src.clearStage[k], this.clearStage[k])))
            {
                // Dict equal
            }
            else
            {
                return true;
            }

            if (this.powerUpInventory.Keys.Count == src.powerUpInventory.Keys.Count &&
                this.powerUpInventory.Keys.All(k => src.powerUpInventory.ContainsKey(k) && object.Equals(src.powerUpInventory[k], this.powerUpInventory[k])))
            {
                // Dict equal
            }
            else
            {
                return true;
            }

            if (this.highestLevel != src.highestLevel)
            {
                return true;
            }
            if (this.highestSubLevel != src.highestSubLevel)
            {
                return true;
            }
            if (this.coin != src.coin)
            {
                return true;
            }

            return false;
        }
    }
}
