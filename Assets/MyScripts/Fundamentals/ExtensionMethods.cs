﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Com.OblivionWorkshop
{
    public static class ExtensionMethods
    {
        public static T GetInterface<T>(this Component com) where T : class
        {
            Assert.IsTrue(typeof(T).IsInterface, "It is not an interface!");

            return com.GetComponent(typeof(T)) as T;
        }
    }
}