using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{

    [System.Serializable]
	public class CharData
	{
        public IEnum.CharacterType charType;
        public int maxEnergy;
        public int startEnergy;

        public CharData(IEnum.CharacterType _type, int _maxE, int _startE)
        {
            charType = _type;
            maxEnergy = _maxE;
            startEnergy = _startE;
        }
    }
}
