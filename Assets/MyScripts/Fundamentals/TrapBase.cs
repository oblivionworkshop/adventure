using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
	public class TrapBase : MonoBehaviour, IHitable
	{
        [Header("Trap Base")]
        [SerializeField]
        protected Collider2D col;
        [SerializeField]
        protected List<SpriteRenderer> spRendererList;
        [SerializeField]
        bool killChar = true;
        [SerializeField]
        bool fallAfterDeath = true;
        [SerializeField]
        List<TrapBase> fallingChild = new List<TrapBase>();
        [SerializeField]
        Vector2 fallingChildRandomFallSpeed;
        [SerializeField]
        protected bool rotateAfterDeath = true;
        [SerializeField]
        protected Transform deathRotateTarget;
        [SerializeField]
        protected Vector3 deathRotateVector;
        [SerializeField]
        protected GamePage.TrapTopParent deathDestroyTarget;
        [Space(10f)]
        [SerializeField]
        protected bool fadeAfterHit = false;
        [SerializeField]
        protected bool invulnerable = true;
        [SerializeField]
        protected int startingHitPoint = 99;
        protected int currentHitPoint;

        protected Rigidbody2D rd;
        protected int rdIndex;
        protected bool rdReg = false;

        protected bool stopUpdate = false;
        protected bool died = false;
        protected float diedPosY;
        protected float deathYRange = 60f;

        protected UnityAction dieAct;
        UnityAction<int> setRDIndexAct;

        public Transform _transform { get; private set; }

        protected virtual void Awake()
        {
            SetTransformVar();
            SetLayer();
            if (gameObject.GetComponent<Rigidbody2D>())
            {
                rd = gameObject.GetComponent<Rigidbody2D>();
                setRDIndexAct = SetRDIndex;
            }
        }

        protected void SetTransformVar()
        {
            if (_transform == null)
            {
                _transform = gameObject.transform;
            }
        }

        void SetLayer()
        {
            if (gameObject.layer != LayerMask.NameToLayer("Trap"))
            {
                foreach (Collider2D col in gameObject.GetComponentsInChildren<Collider2D>(true))
                {
                    col.gameObject.layer = LayerMask.NameToLayer("Trap");
                }
            }
        }

        protected virtual void Start()
        {
            currentHitPoint = startingHitPoint;
            rdIndex = -1;

            for (int i = 0; i < spRendererList.Count; i++)
            {
                if (spRendererList[i].sortingLayerName != "Trap")
                {
                    spRendererList[i].sortingLayerName = "Trap";
                }
            }

            if (rd)
            {
                GamePage.GameGM.instance.RegRDIntoDict(rd, setRDIndexAct);
            }
        }

        protected virtual void Update()
        {
            if (died && !stopUpdate && GamePage.GameGM.instance.GetGameState() == IEnum.GameState.playing)
            {
                if (rotateAfterDeath)
                {
                    if (deathRotateTarget == null)
                    {
                        _transform.Rotate(deathRotateVector * Time.deltaTime);
                    }
                    else
                    {
                        deathRotateTarget.Rotate(deathRotateVector * Time.deltaTime);
                    }
                }

                CheckShouldTrapDestroy();
            }
        }

        public virtual void HitEffect()
        {
            //Debug.Log("Trap Parent HitEffect");
            if (!died)
            {
                if (!invulnerable)
                {
                    currentHitPoint--;
                    if (currentHitPoint <= 0)
                    {
                        Die();
                        diedPosY = _transform.position.y;
                    }
                }

                if (fadeAfterHit)
                {
                    if (currentHitPoint >= 0)
                    {

                        for (int i = 0; i < spRendererList.Count; i++)
                        {
                            Color32 _color = spRendererList[i].color;
                            _color.a = (byte)(255f * (float)currentHitPoint / (float)startingHitPoint);
                            spRendererList[i].color = _color;
                        }

                        if (currentHitPoint <= 0)
                        {
                            for (int i = 0; i < spRendererList.Count; i++)
                            {
                                spRendererList[i].enabled = false;
                            }

                            if (col)
                            {
                                col.enabled = false;
                            }
                        }
                    }
                }

                if (fallAfterDeath && currentHitPoint <= 0)
                {
                    Fall();

                    for (int i = 0; i < fallingChild.Count; i++)
                    {
                        if (fallingChildRandomFallSpeed.x != 0f && fallingChildRandomFallSpeed.y != 0f)
                        {
                            fallingChildRandomFallSpeed.x = Mathf.Clamp(fallingChildRandomFallSpeed.x, 0.5f, 10f);
                            fallingChildRandomFallSpeed.y = Mathf.Clamp(fallingChildRandomFallSpeed.y, 0.5f, 10f);

                            fallingChild[i].Fall(Random.Range(fallingChildRandomFallSpeed.x, fallingChildRandomFallSpeed.y));
                        }
                        else
                        {
                            fallingChild[i].Fall();
                        }
                    }
                }
            }
        }

        public void Fall(float fallSpeed = 1f)
        {
            if (!died)
            {
                Die();
            }
            if (col)
            {
                col.isTrigger = true;
            }

            if (rd)
            {
                rd.bodyType = RigidbodyType2D.Dynamic;
                rd.gravityScale = fallSpeed;
            }
        }

        void Die()
        {
            died = true;
            killChar = false;
            currentHitPoint = 0;
            invulnerable = false;

            if (dieAct != null)
            {
                dieAct();
            }
        }

        void CheckShouldTrapDestroy()
        {
            if (died && !stopUpdate)
            {
                float rangeDiff = Mathf.Abs(_transform.position.y - diedPosY);
                if (rangeDiff >= deathYRange)
                {
                    stopUpdate = true;

                    if (deathDestroyTarget != null)
                    {
                        deathDestroyTarget.Kill();
                    }
                    else
                    {
                        DestroyThis();
                    }
                }
            }
        }

        protected void SetRDIndex(int _index)
        {
            rdIndex = _index;

            AddRDIndexToParentList(rdIndex);
        }

        protected void AddRDIndexToParentList(int _index)
        {
            GamePage.TrapTopParent trapParent = GetComponentInParent<GamePage.TrapTopParent>();
            if (trapParent)
            {
                trapParent.AddTrapIndexToList(_index);
            }

            // Must be put at last
            rdReg = true;
        }

        protected virtual void OnDestroy()
        {
            // If this trap will kill itself when falling, need to notify GameGM RD Dict & Trap Top Parent
            if (rd && rdReg)
            {
                GamePage.TrapTopParent trapParent = GetComponentInParent<GamePage.TrapTopParent>();
                #if _DEBUG_Trap
                Debug.Log("<color=green>TrapBase OnDestroy. GameObejct name = " + gameObject.name + "</color>");
#endif
                if (trapParent)
                {
                    #if _DEBUG_Trap
                    Debug.Log("<color=yellow>Have Trap Parent</color>");
#endif
                    trapParent.RemoveTrapIndexFromList(rdIndex);
                }

                GamePage.GameGM.instance.RemoveRDFromDict(rdIndex);

                // Must be put at last
                rdReg = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("TrapBoundary"))
            {
                DestroyThis();
            }
        }

        protected virtual void DestroyThis()
        {
            Destroy(this.gameObject);
        }

        #region Getter

        public bool GetKillChar()
        {
            return killChar;
        }

        public bool IsPlayingAndAlive()
        {
            return (GamePage.GameGM.instance.GetGameState() == IEnum.GameState.playing && !died);
        }
#endregion
    }
}
