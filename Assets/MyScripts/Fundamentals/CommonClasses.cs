﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    [System.Serializable]
    public class BlockLevel
    {
        public List<BlockSubLevel> level;
    }

    [System.Serializable]
    public class BlockSubLevel
    {
        public GamePage.LevelStage subLevel;
    }

    [System.Serializable]
    public class Sound
    {
        public IEnum.AudioList identifier;
        public AudioClip clip;
        [Range(0f, 1f)]
        public float volume = 1f;
        [Range(0f, 1f)]
        public float pitch = 1f;

        public Sound(IEnum.AudioList identifier, AudioClip clip, float volume, float pitch)
        {
            this.identifier = identifier;
            this.clip = clip;
            this.volume = volume;
            this.pitch = pitch;
        }
    }

    [System.Serializable]
    public class UIAudioDataClass
    {
        public bool bgmMute, sfxMute;
        public float bgmVol, sfxVol;
    }

    [System.Serializable]
    public class BounceDataClass
    {
        public bool bounce;
        public bool neutral;
        public bool die;
        public IEnum.Direction direction;

        public BounceDataClass(bool shouldBounce, bool shouldNeutral, bool shouldDie, IEnum.Direction dir = IEnum.Direction.none)
        {
            this.bounce = shouldBounce;
            this.neutral = shouldNeutral;
            this.die = shouldDie;
            this.direction = dir;
        }
    }

}