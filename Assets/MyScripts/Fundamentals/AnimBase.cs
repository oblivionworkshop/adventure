using Beebyte.Obfuscator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
	public class AnimBase : MonoBehaviour
	{
        Animator anim;
        public UnityAction afterOpenAnim, afterCloseAnim;

        private void Awake()
        {
            if (gameObject.GetComponent<Animator>() != null)
            {
                anim = gameObject.GetComponent<Animator>();
            }
            else
            {
                Debug.LogError("GameObject name " + gameObject.name + " cannot default get Anim");
            }
        }

        public void Open()
        {
            anim.SetTrigger("open");
        }

        public void Close()
        {
            anim.SetTrigger("close");
        }

        public void OpenInstant()
        {
            anim.SetTrigger("openInstant");
        }

        public void CloseInstant()
        {
            anim.SetTrigger("closeInstant");
        }

        [SkipRename]
        void AfterOpenAnim()
        {
            if (afterOpenAnim != null)
            {
                afterOpenAnim();
            }
            else
            {
                Debug.LogWarning("afterOpenAnim is null!");
            }
        }

        [SkipRename]
        void AfterCloseAnim()
        {
            if (afterCloseAnim != null)
            {
                afterCloseAnim();
            }
            else
            {
                Debug.LogWarning("afterCloseAnim is null!");
            }
        }
    }
}
