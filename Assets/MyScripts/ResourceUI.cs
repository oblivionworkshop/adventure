using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DamageNumbersPro;
using Com.OblivionWorkshop.GamePage;
using DG.Tweening;
using Com.OblivionWorkshop.StartPage;

namespace Com.OblivionWorkshop
{
    public class ResourceUI : MonoBehaviour
    {
        public static ResourceUI instance;
        
        [SerializeField]
        Text coinTxt, coinTxtShadow;
        int coinAmount;
        [SerializeField]
        RectTransform coinsSet;
        [SerializeField]
        Image coinImg;
        public Image CoinImg { get { return coinImg; } }
        [SerializeField]
        Image coinAddBtnImg;
        [SerializeField]
        CoinShopButton coinShopBtn;
        Canvas canvas;
        int initSortingOrder = 98;

        [Space(10f)]
        GameUI gameUI;
        StartPageUI startPageUI;

        [Space(10f)]
        [SerializeField]
        CoinShop coinShop;
        [SerializeField]
        PopupAnim coinShopAnim;
        [SerializeField]
        PopupCloseButton coinShopCloseBtn;

        [Header("Prefabs")]
        [SerializeField]
        DamageNumber getCoinPrefab;
        [SerializeField]
        DamageNumber minusCoinPrefab;

        [Header("Global Color Config")]
        [SerializeField]
        Color32 feeTxtPositiveWhiteColor;
        public Color32 FeeTxtPositiveWhiteColor { get { return feeTxtPositiveWhiteColor; } }
        [SerializeField]
        Color32 feeTxtPositiveBlueColor;
        public Color32 FeeTxtPositiveBlueColor { get { return feeTxtPositiveBlueColor; } }
        [SerializeField]
        Color32 feeTxtNegativeRedColor;
        public Color32 FeeTxtNegativeRedColor { get { return feeTxtNegativeRedColor; } }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
            canvas = gameObject.GetComponent<Canvas>();
        }

        public void Activate(bool active)
        {
            coinsSet.gameObject.SetActive(active);
        }

        public void SetupByScene(IEnum.SceneName _scene, Camera _cam)
        {
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = _cam;

            if (_scene == IEnum.SceneName.Game)
            {
                Activate(true);
                gameUI = GameGM.instance.gameUI;
                startPageUI = null;

                canvas.sortingOrder = initSortingOrder;

                DisableOpenCoinShopBtn();

                coinsSet.anchoredPosition = new Vector2(-260f, -95f);
            }
            else if (_scene == IEnum.SceneName.StartPage)
            {
                Activate(true);
                startPageUI = StartPageGM.instance.ui;
                gameUI = null;

                canvas.sortingOrder = initSortingOrder;

                coinsSet.anchoredPosition = new Vector2(-117f, -113f);

                SetupOpenCoinShopBtn();

                coinShopCloseBtn.clickAct = DefaultBtnClick;
                coinShopCloseBtn.afterClickAct = CloseCoinShop;
                coinShopCloseBtn.ButtonSetup();
            }
            else if (_scene == IEnum.SceneName.AdventureLogin)
            {
                startPageUI = null;
                gameUI = null;
            }

            SetCoinNum(AppGM.instance.GetCoinAmount());
        }

        public void LoadingScreenTransition()
        {
            canvas.renderMode = RenderMode.WorldSpace;
            canvas.sortingOrder = -1;
        }

        public void ShowOnTop(bool top)
        {
            if (top)
            {
                canvas.sortingOrder = 100;
            }
            else
            {
                if (AppGM.instance.CurrentScene == IEnum.SceneName.Game)
                {
                    canvas.sortingOrder = initSortingOrder;
                }
                else
                {
                    canvas.sortingOrder = initSortingOrder;
                }
                
            }
            
        }


        void SetCoinNum(int coin)
        {
            coinAmount = coin;
            coinTxt.text = coin.ToString();
            coinTxtShadow.text = coin.ToString();
        }

        public void AddCoin(int getVal, bool immediate)
        {
            if (immediate)
            {
                // Case for immediate get coin. The UI should reflect immediately on the actual value from in AppGM
                if (getVal >= 0)
                {
                    DamageNumber coin = getCoinPrefab.Spawn(Vector3.one, getVal);
                    coin.SetAnchoredPosition(coinTxt.transform, coinTxt.rectTransform.anchoredPosition);
                }
                else
                {
                    DamageNumber coin = minusCoinPrefab.Spawn(Vector3.one, getVal);
                    coin.SetAnchoredPosition(coinTxt.transform, coinTxt.rectTransform.anchoredPosition);
                }

                SetCoinNum(AppGM.instance.GetCoinAmount());
            }
            else
            {
                // Case for NOT immediate get coin. Maybe due to animation time, UI value is different to AppGM value
                // and need to rely on previous snapshot of UI to calculate the difference
                if (getVal > 0)
                {
                    DamageNumber coin = getCoinPrefab.Spawn(Vector3.one, getVal);
                    coin.SetAnchoredPosition(coinTxt.transform, coinTxt.rectTransform.anchoredPosition);
                }
                else
                {
                    DamageNumber coin = minusCoinPrefab.Spawn(Vector3.one, getVal);
                    coin.SetAnchoredPosition(coinTxt.transform, coinTxt.rectTransform.anchoredPosition);
                }

                SetCoinNum(coinAmount + getVal);
            }
            
        }

        #region Coin Shop
        public void SetupOpenCoinShopBtn()
        {
            //coinShopBtn.clickAct += DefaultBtnClick;
            coinShopBtn.clickAct += BlockInput;
            coinShopBtn.clickAct += OpenCoinShop;
            coinShopBtn.ButtonSetup();

            coinAddBtnImg.gameObject.SetActive(true);
        }

        public void DisableOpenCoinShopBtn()
        {
            coinShopBtn.Clear();
            coinShopBtn.clickAct = null;

            coinAddBtnImg.gameObject.SetActive(false);
        }

        void OpenCoinShop()
        {
            OpenPopupDefault();
            //beforeStartGamePopup.SettingsBeforeOpen(_indexLevel, stageBtnAfterClickAct);
            ShowOnTop(true);
            coinShop.gameObject.SetActive(true);

            coinShop.GetAnim().afterOpenAnim = null;
            coinShop.GetAnim().afterOpenAnim = AllowInput;
            coinShop.GetAnim().Open();

            // Deactivate the Open Coin Shop function from the Coin Btn
            DisableOpenCoinShopBtn();
        }

        void CloseCoinShop()
        {
            coinShop.GetAnim().afterCloseAnim = null;
            coinShop.GetAnim().afterCloseAnim = () => {
                if (AppGM.instance.CurrentScene == IEnum.SceneName.StartPage)
                {
                    ShowOnTop(false);
                }
                SetupOpenCoinShopBtn();
                coinShop.GetAnim().gameObject.SetActive(false);
                ClosePopupDefault();
                AllowInput();
            };
            coinShop.GetAnim().Close();
            AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
        }
        #endregion

        #region Default Popup config
        void DefaultBtnClick()
        {
            BlockInput();
            AppGM.instance.PlaySFX(IEnum.AudioList.buttonClick);
        }

        void OpenPopupDefault()
        {
            BlockInput();
            AppGM.instance.PlaySFX(IEnum.AudioList.popupOpen);

            if (AppGM.instance.CurrentScene == IEnum.SceneName.Game)
            {
                //greyGamePanelBG.SetActive(true);
            }
            else
            {
                startPageUI.GetGreyGamePanelBG().SetActive(true);

                coinShopAnim.gameObject.SetActive(true);
                coinShopAnim.afterOpenAnim = null;
                coinShopAnim.afterOpenAnim = AllowInput;
                coinShopAnim.Open();
            }
            
        }

        void ClosePopupDefault()
        {
            if (AppGM.instance.CurrentScene == IEnum.SceneName.Game)
            {
                //greyGamePanelBG.SetActive(false);
            }
            else
            {
                startPageUI.GetGreyGamePanelBG().SetActive(false);
            }
        }
        #endregion

        #region Input
        void BlockInput()
        {
            if (AppGM.instance.CurrentScene == IEnum.SceneName.Game)
            {
                gameUI.allowInputAct(false);
            }
            else
            {
                startPageUI.allowInputAct(false);
            }
        }

        void AllowInput()
        {
            if (AppGM.instance.CurrentScene == IEnum.SceneName.Game)
            {
                gameUI.allowInputAct(true);
            }
            else
            {
                startPageUI.allowInputAct(true);
            }
        }
        #endregion

        #region Getter
        public Vector2 GetCoinImgWorldPos()
        {
            RectTransform rt = coinImg.GetComponent<RectTransform>();
            return rt.TransformPoint(rt.anchoredPosition);
        }
        #endregion
    }
}
