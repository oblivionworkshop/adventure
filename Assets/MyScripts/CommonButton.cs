using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public class CommonButton : ButtonBase
    {
        // This class act as a child class of ButtonBase to avoid naming each button with its own name.
        // Should be directly inherit from ButtonBase and not making any changes
        // If there is any needs to change, change the Base Class (virtual) directly
    }
}
