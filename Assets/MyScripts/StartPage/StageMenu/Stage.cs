using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
    namespace StartPage
    {
        public class Stage : MonoBehaviour
        {
            [SerializeField]
            StageLevelButton stageLvBtn;
            
            int level;

            public void Setup(int _level, UnityAction _clickAct, UnityAction<int> _stageBtnAfterClickAct, bool available, bool _maxLevel)
            {
                level = _level;
                stageLvBtn.level = _level;

                stageLvBtn.Available(available, _clickAct, _stageBtnAfterClickAct, _maxLevel);
            }
            
            public int GetLevel()
            {
                return level;
            }
        }
    }
}
