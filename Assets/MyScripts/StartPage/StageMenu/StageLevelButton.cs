using Beebyte.Obfuscator;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace Com.OblivionWorkshop{
    namespace StartPage
    {
        public class StageLevelButton : ButtonBase
        {
            public UnityAction<int> stageBtnAfterClickAct;
            public int level;

            [Header("Source")]
            [SerializeField]
            Sprite btnActiveSprite;
            [SerializeField]
            Sprite btnHighestStageSprite;
            [SerializeField]
            Sprite btnInactiveSprite;

            [Space(10f)]
            [SerializeField]
            TMP_Text stageTxt;
            [SerializeField]
            Image btnImg;

            [Space(10f)]
            [SerializeField]
            GameObject btnActiveShadow;
            [SerializeField]
            GameObject btnHighestStageIndicator;
            [SerializeField]
            GameObject stageLockSet;
            [SerializeField]
            GameObject starsSet;

            public void Available(bool flag, UnityAction _clickAct, UnityAction<int> _stageBtnAfterClickAct, bool maxLevel)
            {
                stageTxt.gameObject.SetActive(flag);
                stageLockSet.SetActive(!flag);
                btnActiveShadow.SetActive(flag);

                if (flag)
                {
                    stageBtnAfterClickAct = _stageBtnAfterClickAct;
                    clickAct = _clickAct;
                    ButtonSetup();

                    //-------- Turn off animation to prevent animation stuck from changing values--------
                    if (anim != null)
                    {
                        anim.enabled = false;
                    }

                    btnImg.sprite = maxLevel? btnHighestStageSprite : btnActiveSprite;
                    btnHighestStageIndicator.SetActive(maxLevel);

                    stageTxt.text = (level + 1).ToString();
                    stageTxt.fontSize = (level + 1) >= 1000 ? 63 : (level + 1) >= 100 ? 74 : (level + 1) >= 10 ? 85 : 99;
                    //----------------------------------------------------------

                    anim.enabled = true;
                }
                else
                {
                    stageBtnAfterClickAct = null;
                    clickAct = null;
                    Clear();

                    //-------- Turn off animation to prevent animation stuck from changing values--------
                    if (anim != null)
                    {
                        anim.enabled = false;
                    }

                    btnImg.sprite = btnInactiveSprite;
                    //----------------------------------------------------------

                    anim.enabled = true;
                }
            }

            [SkipRename]
            protected override void AfterClickAnim()
            {
                stageBtnAfterClickAct(level);
            }

        }
    }
}
