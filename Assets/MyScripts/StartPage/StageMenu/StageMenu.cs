using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DanielLochner.Assets.SimpleScrollSnap;

namespace Com.OblivionWorkshop{
    namespace StartPage
    {
        public class StageMenu : MonoBehaviour
        {
            [SerializeField]
            StageBackButton backBtn;
            [SerializeField]
            CommonButton nextBtn, prevBtn;
            [SerializeField]
            SimpleScrollSnap sss;
            [SerializeField]
            Animator paginationAnim;

            [SerializeField]
            StagesParent stagesParentPrefab;
            List<StagesParent> stagesParentList;
            Transform _transform;

            int curPage = 0;

            public UnityAction<bool> allowInputAct;
            public UnityAction clickAct, backToHomePageAct;
            public UnityAction<int> stageBtnAfterClickAct;

            private void Awake()
            {
                _transform = transform;
            }

            public void StageMenuSetup(int currentMaxLevel)
            {
                backBtn.clickAct = clickAct;
                backBtn.afterClickAct = backToHomePageAct;
                backBtn.ButtonSetup();

                sss.goNextPanelAct = NextPanel;
                sss.goPrevPanelAct = PrevPanel;
                sss.onPanelChanged.RemoveAllListeners();
                sss.onPanelChanged.AddListener(OnPanelChanged);
                sss.SSSSetup();

                nextBtn.clickAct += clickAct;
                nextBtn.clickAct += sss.GoToNextPanel;
                nextBtn.ButtonSetup();

                prevBtn.clickAct += clickAct;
                prevBtn.clickAct += sss.GoToPreviousPanel;
                prevBtn.ButtonSetup();
                
                /*-------------- Debug Purpose-------------------
                sss.onPanelChanged.AddListener(OnPanelChanged);
                sss.onPanelChanging.AddListener(OnPanelChanging);
                sss.onPanelSelected.AddListener(OnPanelSelected);
                sss.onPanelSelecting.AddListener(OnPanelSelecting);*/

                stagesParentList = new List<StagesParent>();
                for (int i = 0; i < sss.Content.childCount; i++)
                {
                    stagesParentList.Add(sss.Content.GetChild(i).GetComponent<StagesParent>());
                }

                int midPage = Mathf.FloorToInt((float)stagesParentList.Count / 2f);

                if (currentMaxLevel <= ((midPage + 1) * 10))
                {
                    curPage = Mathf.FloorToInt(currentMaxLevel / 10);

                    for (int i = 0; i < stagesParentList.Count; i++)
                    {
                        List<Stage> stageList = new List<Stage>();
                        stageList = stagesParentList[i].GetStageList();

                        for (int j = 0; j < stageList.Count; j++)
                        {
                            int btnLevel = i * 10 + j;
                            stageList[j].Setup(btnLevel, clickAct, stageBtnAfterClickAct, btnLevel <= currentMaxLevel ? true : false, btnLevel == currentMaxLevel ? true : false);
                        }
                    }
                }
                else if (currentMaxLevel > (AppGM.instance.GetMaxStage() - (midPage*10)))
                {
                    curPage = stagesParentList.Count - Mathf.CeilToInt((float)(AppGM.instance.GetMaxStage() - currentMaxLevel) / 10f);
                    int starter = Mathf.FloorToInt(currentMaxLevel / 10);

                    for (int i = 0; i < stagesParentList.Count; i++)
                    {
                        List<Stage> stageList = new List<Stage>();
                        stageList = stagesParentList[i].GetStageList();

                        for (int j = 0; j < stageList.Count; j++)
                        {
                            int btnLevel = (starter - curPage + i) * 10 + j;
                            stageList[j].Setup(btnLevel, clickAct, stageBtnAfterClickAct, btnLevel <= currentMaxLevel ? true : false, btnLevel == currentMaxLevel ? true : false);
                        }
                    }
                }
                else
                {
                    curPage = 3;
                    int starter = Mathf.FloorToInt(currentMaxLevel / 10);

                    for (int i = 0; i < stagesParentList.Count; i++)
                    {
                        List<Stage> stageList = new List<Stage>();
                        stageList = stagesParentList[i].GetStageList();

                        for (int j = 0; j < stageList.Count; j++)
                        {
                            int btnLevel = (starter - curPage + i) * 10 + j;
                            stageList[j].Setup(btnLevel, clickAct, stageBtnAfterClickAct, btnLevel <= currentMaxLevel ? true : false, btnLevel == currentMaxLevel ? true : false);
                        }
                    }
                }

                sss.snappingSpeed = 999f;
                sss.hardSnap = true;
                sss.GoToPanel(curPage);
                // Force run the Occlusion Culling. Otherwise, when the curPage = 0 (i.e. no velocity) the OcclusionCulling function does not run and will determine all pages as disable
                sss.OnOcclusionCulling(true);

                // Set NExt / Prev Button disable if match criteria
                if (curPage == 0)
                {
                    prevBtn.gameObject.SetActive(false);
                }
                else if (curPage == stagesParentList.Count - 1)
                {
                    nextBtn.gameObject.SetActive(false);
                }

                StartCoroutine(DelayOpenStageMenu(false));
            }

            public IEnumerator DelayOpenStageMenu(bool show)
            {
                Animator anim = GetComponent<Animator>();

                if (!show)
                {
                    anim.Play("StartPage_StagePageOut", 0, 1f);
                }
                else
                {
                    anim.Play("StartPage_StagePageIn", 0, 1f);
                }
                yield return null;
                _transform.localScale = Vector3.one;

                sss.snappingSpeed = 10f;

                SetDefaultPagination();
            }

            public void CloseStageMenu()
            {
                _transform.localScale = Vector3.zero;
            }

            #region Scroll Rect & Pagination

            void SetDefaultPagination()
            {
                switch (curPage)
                {
                    case 0:
                        paginationAnim.Play("StartPage_StageMenu_Page1To0", 0, 1f);
                        break;
                    case 1:
                        paginationAnim.Play("StartPage_StageMenu_Page0To1", 0, 1f);
                        break;
                    case 2:
                        paginationAnim.Play("StartPage_StageMenu_Page1To2", 0, 1f);
                        break;
                    case 3:
                        paginationAnim.Play("StartPage_StageMenu_Page2To3", 0, 1f);
                        break;
                    case 4:
                        paginationAnim.Play("StartPage_StageMenu_Page3To4", 0, 1f);
                        break;
                    case 5:
                        paginationAnim.Play("StartPage_StageMenu_Page4To5", 0, 1f);
                        break;
                    case 6:
                        paginationAnim.Play("StartPage_StageMenu_Page5To6", 0, 1f);
                        break;
                    default:
                        Debug.LogError("CurPage is Out Of Expectation!");
                        break;
                }
            }

            void NextPanel()
            {
                Debug.Log("Next Panel");
                allowInputAct(false);

                switch (curPage)
                {
                    case 0:
                        paginationAnim.SetTrigger("page0to1");
                        curPage++;
                        break;
                    case 1:
                        paginationAnim.SetTrigger("page1to2");
                        curPage++;
                        break;
                    case 2:
                        paginationAnim.SetTrigger("page2to3");
                        curPage++;
                        break;
                    case 3:
                        int mid = Mathf.FloorToInt((float)stagesParentList.Count / 2f);
                        int midPageStarterLevel = stagesParentList[mid].GetStageList()[0].GetLevel();

                        if ((midPageStarterLevel+1) > (AppGM.instance.GetMaxStage() - ((mid+1) * 10)))
                        {
                            paginationAnim.SetTrigger("page3to4");
                            curPage++;
                        }
                        else
                        {
                            paginationAnim.SetTrigger("page3to4_small6");

                            // Delete the first Panel. Add a new panel on the last. Set it up.
                            stagesParentList.RemoveAt(0);
                            sss.RemoveFromFront();
                            sss.AddToBack(stagesParentPrefab.gameObject);
                            stagesParentList.Add(sss.Content.GetChild(sss.Content.childCount-1).GetComponent<StagesParent>());

                            List<Stage> stageListOfSecondLastPanel = stagesParentList[stagesParentList.Count - 2].GetStageList();
                            int starter = stageListOfSecondLastPanel[stageListOfSecondLastPanel.Count-1].GetLevel() + 1;

                            List<Stage> stageList = new List<Stage>();
                            stageList = stagesParentList[stagesParentList.Count - 1].GetStageList();

                            for (int i = 0; i < stageList.Count; i++)
                            {
                                int btnLevel = starter + i;
                                stageList[i].Setup(btnLevel, clickAct, stageBtnAfterClickAct, btnLevel <= AppGM.instance.GetPlayerStageMaxProgress() ? true : false, btnLevel == AppGM.instance.GetPlayerStageMaxProgress() ? true : false);
                            }
                            // --------------------------------
                        }
                        break;
                    case 4:
                        paginationAnim.SetTrigger("page4to5");
                        curPage++;
                        break;
                    case 5:
                        paginationAnim.SetTrigger("page5to6");
                        curPage++;
                        if (nextBtn.gameObject.activeInHierarchy)
                        {
                            nextBtn.gameObject.SetActive(false);
                        }
                        break;
                    default:
                        break;
                }

                if (curPage > 0 && !prevBtn.gameObject.activeInHierarchy)
                {
                    prevBtn.gameObject.SetActive(true);
                }
            }

            void PrevPanel()
            {
                Debug.Log("Prev Panel");
                allowInputAct(false);

                switch (curPage)
                {
                    case 1:
                        paginationAnim.SetTrigger("page1to0");
                        curPage--;
                        if (prevBtn.gameObject.activeInHierarchy)
                        {
                            prevBtn.gameObject.SetActive(false);
                        }
                        break;
                    case 2:
                        paginationAnim.SetTrigger("page2to1");
                        curPage--;
                        break;
                    case 3:
                        int mid = Mathf.FloorToInt((float)stagesParentList.Count / 2f);
                        int midPageStarterLevel = stagesParentList[mid].GetStageList()[0].GetLevel();

                        if ((midPageStarterLevel+1) <= (mid * 10) + 1)
                        {
                            paginationAnim.SetTrigger("page3to2");
                            curPage--;
                        }
                        else
                        {
                            paginationAnim.SetTrigger("page3to2_small0");

                            // Delete the last Panel. Add a new Panel at the front. Set it up.
                            stagesParentList.RemoveAt(stagesParentList.Count - 1);
                            sss.RemoveFromBack();
                            sss.AddToFront(stagesParentPrefab.gameObject);
                            stagesParentList.Insert(0, sss.Content.GetChild(0).GetComponent<StagesParent>());

                            int starter = stagesParentList[1].GetStageList()[0].GetLevel() - stagesParentList[0].GetStageList().Count;

                            List<Stage> stageList = new List<Stage>();
                            stageList = stagesParentList[0].GetStageList();

                            for (int i = 0; i < stageList.Count; i++)
                            {
                                int btnLevel = starter + i;
                                stageList[i].Setup(btnLevel, clickAct, stageBtnAfterClickAct, btnLevel <= AppGM.instance.GetPlayerStageMaxProgress() ? true : false, btnLevel == AppGM.instance.GetPlayerStageMaxProgress() ? true : false);
                            }
                            // --------------------------------
                        }
                        break;
                    case 4:
                        paginationAnim.SetTrigger("page4to3");
                        curPage--;
                        break;
                    case 5:
                        paginationAnim.SetTrigger("page5to4");
                        curPage--;
                        break;
                    case 6:
                        paginationAnim.SetTrigger("page6to5");
                        curPage--;
                        break;
                    default:
                        break;
                }

                if (curPage < stagesParentList.Count - 1 && !nextBtn.gameObject.activeInHierarchy)
                {
                    nextBtn.gameObject.SetActive(true);
                }
            }

            IEnumerator PanelFinishChanged()
            {
                yield return new WaitForSeconds(0.25f);
                allowInputAct(true);
            }

            void OnPanelChanged()
            {
                //Debug.Log("OnPanelChanged");
                AppGM.instance.StartCoroutineStoppable(PanelFinishChanged());
            }

            void OnPanelChanging()
            {
                Debug.Log("OnPanelChanging");
            }

            void OnPanelSelecting()
            {
                Debug.Log("OnPanelSelecting");
            }

            void OnPanelSelected()
            {
                Debug.Log("OnPanelSelected");
            }
            #endregion
        }
    }
}
