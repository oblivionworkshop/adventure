using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace StartPage
    {
        public class StagesParent : MonoBehaviour
        {
            [SerializeField]
            List<Stage> stageList;

            public List<Stage> GetStageList()
            {
                return stageList;
            }
        }
    }
}
