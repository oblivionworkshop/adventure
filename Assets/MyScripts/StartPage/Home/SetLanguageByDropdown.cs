using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using TMPro;
using System.Linq;
using System;
using UnityEngine.UI;
using System.Reflection;
using Unity.Mathematics;
using static Unity.Burst.Intrinsics.X86.Avx;
using static UnityEngine.Rendering.VolumeComponent;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class SetLanguageByDropdown : MonoBehaviour
        {
            List<string> langList;

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_4_OR_NEWER
            void OnEnable()
            {
                TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();
                if (dropdown == null)
                    return;

                var currentLanguage = LocalizationManager.CurrentLanguage;
                if (LocalizationManager.Sources.Count == 0) LocalizationManager.UpdateSources();
                var languages = LocalizationManager.GetAllLanguages();
                langList = new List<string>(languages);


                List<string> translatedList = new List<string>();
                foreach ( string language in languages )
                {
                    string tmp = LocalizationManager.GetTranslation(language);
                    translatedList.Add(tmp);
                }

                // Fill the dropdown elements
                dropdown.ClearOptions();
                dropdown.AddOptions(translatedList);

                dropdown.value = languages.IndexOf(currentLanguage);
                dropdown.onValueChanged.RemoveListener(OnValueChanged);
                dropdown.onValueChanged.AddListener(OnValueChanged);
            }

            void OnValueChanged(int index)
            {
                TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();

                if (index < 0)
                {
                    index = 0;
                    dropdown.value = index;
                }

                if (langList != null)
                {
                    LocalizationManager.CurrentLanguage = langList[index];
                }

                List<string> translatedList = new List<string>();
                foreach (string language in langList)
                {
                    string tmp = LocalizationManager.GetTranslation(language);
                    translatedList.Add(tmp);
                }

                for (int i = 0; i < dropdown.options.Count; i++)
                {
                    dropdown.options[i].text = translatedList[i];
                }

                dropdown.RefreshShownValue();

                IEnumerator IE = ForceSetDDValue(index);
                AppGM.instance.StartCoroutineStoppable(IE);
            }

            IEnumerator ForceSetDDValue(int _index)
            {
                yield return null;

                TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();
                
                int num;
                do
                {
                    num = UnityEngine.Random.Range(0, dropdown.options.Count);
                } while (num == _index);

                dropdown.SetValueWithoutNotify(num);
                dropdown.SetValueWithoutNotify(_index);
            }
#endif
        }
    }
    
}
