﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class StartPageUI : MonoBehaviour
        {
            [SerializeField]
            StartButton startGameBtn;
            [SerializeField]
            CharListButton charListBtn;
            [SerializeField]
            SelectStageButton selectStageBtn;
            [SerializeField]
            TutorialButton tutorialBtn;
            [SerializeField]
            CharChangeButton charChangeLeftBtn, charChangeRightBtn;

            [Space(10f)]
            [SerializeField]
            GameObject greyGamePanelBG;
            [SerializeField]
            ShareButton shareBtn;

            [Space(10f)]
            [SerializeField]
            SettingsButton settingsBtn;
            [SerializeField]
            PopupAnim settingsPopupAnim;
            [SerializeField]
            PopupCloseButton settingsPopupCloseBtn;

            [Space(10f)]
            [SerializeField]
            BeforeStartGamePopup beforeStartGamePopup;
            [SerializeField]
            PowerUpPurchasePopup powerUpPurchasePopup;

            [Space(10f)]
            [SerializeField]
            Text levelTxt;

            [Space(10f)]
            [SerializeField]
            StageMenu stageMenu;

            [Space(10f)]
            [SerializeField]
            Animator charAnim;
            [SerializeField]
            Animator stageMenuPageAnim;
            [SerializeField]
            Animator homePageAnim;

            [Space(10f)]
            [SerializeField]
            ParticleSystem bgParticle;
            [SerializeField]
            Sprite[] bgParticleSprites;

            int currentUILevel;

            public UnityAction<bool> allowInputAct;
            public UnityAction tutorialBtnFinishClickAct;
            public UnityAction<int, int, List<IEnum.PowerUp>> stageBtnAfterClickAct;

            public void UISetup(int currentLevel)
            {
                currentUILevel = currentLevel;

                stageMenu.gameObject.SetActive(true);
                stageMenu.CloseStageMenu();

                startGameBtn.clickAct = DefaultBtnClick;
                startGameBtn.afterClickAct = StartBtnFinishClick;
                startGameBtn.ButtonSetup();

                tutorialBtn.clickAct = DefaultBtnClick;
                tutorialBtn.afterClickAct = TutorialBtnFinishClick;
                tutorialBtn.ButtonSetup();

                selectStageBtn.clickAct = DefaultBtnClick;
                selectStageBtn.afterClickAct = HomePageToStagePage;
                selectStageBtn.ButtonSetup();

                charListBtn.clickAct = DefaultBtnClick;
                charListBtn.afterClickAct = CharListBtnFinishClick;
                charListBtn.ButtonSetup();

                charChangeLeftBtn.clickAct = DefaultBtnClick;
                charChangeLeftBtn.afterClickAct = CharChangeBtnFinishClick;
                charChangeLeftBtn.ButtonSetup();

                charChangeRightBtn.clickAct = DefaultBtnClick;
                charChangeRightBtn.afterClickAct = CharChangeBtnFinishClick;
                charChangeRightBtn.ButtonSetup();

                settingsBtn.clickAct = DefaultBtnClick;
                settingsBtn.afterClickAct = SettingsBtnFinishClick;
                settingsBtn.ButtonSetup();

                settingsPopupCloseBtn.clickAct = DefaultBtnClick;
                settingsPopupCloseBtn.afterClickAct = CloseSettingsPopup;
                settingsPopupCloseBtn.ButtonSetup();

                beforeStartGamePopup.BasicSetup(DefaultBtnClick, AllowInput, OpenPowerUpPurchasePopup, currentUILevel);

                beforeStartGamePopup.GetCloseBtn().clickAct = DefaultBtnClick;
                beforeStartGamePopup.GetCloseBtn().afterClickAct = CloseBeforeStartGamePopup;
                beforeStartGamePopup.GetCloseBtn().ButtonSetup();

                powerUpPurchasePopup.GetCloseBtn().clickAct = DefaultBtnClick;
                powerUpPurchasePopup.GetCloseBtn().afterClickAct = ClosePowerUpPurchasePopup;
                powerUpPurchasePopup.GetCloseBtn().ButtonSetup();

                shareBtn.clickAct = DefaultBtnClick;
                shareBtn.afterClickAct = ShareBtnFinishClick;
                shareBtn.ButtonSetup();

                levelTxt.text = "Level  " + currentLevel.ToString();

                stageMenu.allowInputAct = allowInputAct;
                stageMenu.clickAct = DefaultBtnClick;
                //stageMenu.stageBtnAfterClickAct = stageBtnAfterClickAct;
                stageMenu.stageBtnAfterClickAct = OpenBeforeStartGamePopup;
                stageMenu.backToHomePageAct = StagePageToHomePage;
                stageMenu.StageMenuSetup(AppGM.instance.GetPlayerStageMaxProgress());

                SetRandomParticle();
            }

            #region Button Callback

            void DefaultBtnClick()
            {
                BlockInput();
                AppGM.instance.PlaySFX(IEnum.AudioList.buttonClick);
            }

            void StartBtnFinishClick()
            {
                OpenBeforeStartGamePopup(currentUILevel - 1);
            }

            void TutorialBtnFinishClick()
            {
                tutorialBtnFinishClickAct();
            }

            void CharListBtnFinishClick()
            {
                // Perform Actions
                //...
                //----------------------------------

                AllowInput();
            }

            void CharChangeBtnFinishClick()
            {
                // Perform Actions
                //...
                //----------------------------------

                AllowInput();
            }

            void SettingsBtnFinishClick()
            {
                OpenSettingsPopup();
            }

            void ShareBtnFinishClick()
            {
                // Perform Actions
                //...
                //----------------------------------

                AllowInput();
            }
            #endregion

            #region Default Popup config
            void OpenPopupDefault()
            {
                BlockInput();
                AppGM.instance.PlaySFX(IEnum.AudioList.popupOpen);
                greyGamePanelBG.SetActive(true);
            }

            void ClosePopupDefault()
            {
                greyGamePanelBG.SetActive(false);
            }

            public GameObject GetGreyGamePanelBG()
            {
                return greyGamePanelBG;
            }
            #endregion

            #region Settings Popup
            void OpenSettingsPopup()
            {
                OpenPopupDefault();
                settingsPopupAnim.gameObject.SetActive(true);

                settingsPopupAnim.afterOpenAnim = null;
                settingsPopupAnim.afterOpenAnim = AllowInput;
                settingsPopupAnim.Open();
            }

            void CloseSettingsPopup()
            {
                settingsPopupAnim.afterCloseAnim = null;
                settingsPopupAnim.afterCloseAnim = ()=> {
                    settingsPopupAnim.gameObject.SetActive(false);
                    ClosePopupDefault();
                    AllowInput();
                };
                settingsPopupAnim.Close();
                AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
            }
            #endregion

            #region Before Start Game Popup
            void OpenBeforeStartGamePopup(int _indexLevel)
            {
                OpenPopupDefault();
                beforeStartGamePopup.SettingsBeforeOpen(_indexLevel, stageBtnAfterClickAct);
                beforeStartGamePopup.gameObject.SetActive(true);

                beforeStartGamePopup.GetAnim().afterOpenAnim = null;
                beforeStartGamePopup.GetAnim().afterOpenAnim = AllowInput;
                beforeStartGamePopup.GetAnim().Open();
            }

            void CloseBeforeStartGamePopup()
            {
                beforeStartGamePopup.GetAnim().afterCloseAnim = null;
                beforeStartGamePopup.GetAnim().afterCloseAnim = () => {
                    beforeStartGamePopup.ResetAllSlots();
                    beforeStartGamePopup.GetAnim().gameObject.SetActive(false);
                    ClosePopupDefault();
                    AllowInput();
                };
                beforeStartGamePopup.GetAnim().Close();
                AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
            }

            void OpenPowerUpPurchasePopup(PowerUpPurchaseSO _powerUpData, short slotID)
            {
                beforeStartGamePopup.GetAnim().afterCloseAnim = null;
                beforeStartGamePopup.GetAnim().afterCloseAnim = () =>
                {
                    OpenPopupDefault();

                    powerUpPurchasePopup.InfoSetup(_powerUpData, slotID, currentUILevel-1, PowerUpPurchaseBtnSetup);
                    powerUpPurchasePopup.gameObject.SetActive(true);

                    powerUpPurchasePopup.GetAnim().afterOpenAnim = null;
                    powerUpPurchasePopup.GetAnim().afterOpenAnim = AllowInput;
                    powerUpPurchasePopup.GetAnim().Open();
                };
                beforeStartGamePopup.GetAnim().Close();
                AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
            }

            void ClosePowerUpPurchasePopup()
            {
                powerUpPurchasePopup.GetAnim().afterCloseAnim = null;
                powerUpPurchasePopup.GetAnim().afterCloseAnim = () => {
                    PowerUpPurchasePopupAfterClose();
                };
                powerUpPurchasePopup.GetAnim().Close();
                AppGM.instance.PlaySFX(IEnum.AudioList.popupClose);
            }

            void PowerUpPurchasePopupAfterClose()
            {
                powerUpPurchasePopup.gameObject.SetActive(false);

                // As PowerUpPurchasePopup must go back to the original beforeStartGamePopup
                // , the 'SelectedStageLevelIndex' should have been stored previously
                OpenBeforeStartGamePopup(beforeStartGamePopup.SelectedStageLevelIndex);
            }

            void PowerUpPurchaseBtnSetup(PowerUpPurchaseBtnData _data)
            {
                _data.btn.clickAct = DefaultBtnClick;
                if (_data.canBuy)
                {
                    _data.btn.afterClickAct = ()=> {
                        AppGM.instance.PurchasePowerUp(_data.powerUp, _data.cost);
                        beforeStartGamePopup.UpdatePowerUpSlotInventory(_data.slotID);
                        beforeStartGamePopup.SelectPowerUpSlot(_data.slotID);
                        ClosePowerUpPurchasePopup();
                    };
                }
                else
                {
                    _data.btn.afterClickAct = AllowInput;
                }

                _data.btn.ButtonSetup();
            }
            #endregion


            #region Page Navigation

            void HomePageToStagePage()
            {
                StartCoroutine(_HomePageToStagePage());
            }

            IEnumerator _HomePageToStagePage()
            {
                BlockInput();
                homePageAnim.SetTrigger("out");
                yield return new WaitForSeconds(0.46f);
                charAnim.SetTrigger("out");
                yield return new WaitForSeconds(0.16f);
                stageMenuPageAnim.SetTrigger("in");
                yield return new WaitForSeconds(0.7f);
                AllowInput();
            }

            public void HomePageToStagePageInstant()
            {
                homePageAnim.Play("StartPage_HomePageOut", 0, 1f);
                charAnim.Play("StartPage_CharOut", 0, 1f);
                stageMenuPageAnim.Play("StartPage_StagePageIn", 0, 1f);
            }

            void StagePageToHomePage()
            {
                StartCoroutine(_StagePageToHomePage());
            }

            IEnumerator _StagePageToHomePage()
            {
                BlockInput();
                stageMenuPageAnim.SetTrigger("out");
                yield return new WaitForSeconds(0.46f);
                charAnim.SetTrigger("in");
                yield return new WaitForSeconds(0.16f);
                homePageAnim.SetTrigger("in");
                yield return new WaitForSeconds(0.7f);
                AllowInput();
            }

            #endregion

            #region BG Particles
            void SetRandomParticle()
            {
                int randomNum = Random.Range(0, bgParticleSprites.Length);
                int counter = 0;

                while (counter < 100)
                {
                    if (bgParticleSprites[randomNum] != null)
                    {
                        bgParticle.textureSheetAnimation.SetSprite(0, bgParticleSprites[randomNum]);
                        break;
                    }
                    else
                    {
                        counter++;
                        if (counter >= 100)
                        {
                            Debug.LogError("No Available Sprite can be found!");
                            bgParticle.gameObject.SetActive(false);
                        }
                    }
                }
            }
            #endregion

            void BlockInput()
            {
                allowInputAct(false);
            }

            void AllowInput()
            {
                allowInputAct(true);
            }
        }
    }
}