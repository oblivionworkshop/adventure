using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Beebyte.Obfuscator;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class BeforeStartGamePopup : MonoBehaviour
        {
            PopupAnim popUpAnim;
            [SerializeField]
            PopupCloseButton closeBtn;
            [SerializeField]
            CommonButton goBtn;
            [SerializeField]
            Image goBtnImg;
            [SerializeField]
            TMP_Text stageTxt;
            [SerializeField]
            TMP_Text stageEntryFee;
            [SerializeField]
            Sprite goBtnEnableImg, goBtnDisableImg;

            // Save for PowerUpPurchase popup usage, when closing the purchase pop up, this pop-up need to be setup again,
            // then the indexLevel becomes crucial on setting up
            int selectedStageLevelIndex;
            public int SelectedStageLevelIndex { get { return selectedStageLevelIndex; } }

            [SerializeField]
            List<PowerUpSlot> powerUpSlots;

            [SerializeField]
            GameObject clearBonusSticker, firstClearSticker;
            bool levelPass;

            [Header("Bonus Multiplier")]
            [SerializeField]
            BonusMultiplierButton bonusMulBtn;
            [SerializeField]
            Text bonusMulTxt, bonusMulShadowTxt;
            [SerializeField]
            Animator bonusMulTxtAnim;
            [SerializeField]
            BonusMulCoinAnim bonusMulCoinAnim;
            Animator bonusMulCoinAnimator;
            [SerializeField]
            ParticleSystem whiteStarPS, flyCoinPS_1to2, flyCoinPS_2to4;
            [SerializeField]
            List<Image> coinMul2List = new List<Image>(), coinMul4List = new List<Image>();
            [SerializeField]
            int[] multiplierOptions = new int[3];
            int currentMulIndex;

            [Header("Bonus Multiplier (Repeat)")]
            [SerializeField]
            BonusMulCoinAnim bonusRepeatMulCoinAnim;
            Animator bonusRepeatMulCoinAnimator;
            [SerializeField]
            ParticleSystem flyCoinPS_1to2_Repeat, flyCoinPS_2to4_Repeat;
            [SerializeField]
            List<Image> coinRepeatMul2List = new List<Image>(), coinRepeatMul4List = new List<Image>();
            [SerializeField]
            int[] repeatMultiplierOptions = new int[3];
            int currentRepeatMulIndex;



            UnityAction defaultBtnAct, allowAct;
            UnityAction<int, int, List<IEnum.PowerUp>> stageBtnAfterClickAct;

            private void Awake()
            {
                popUpAnim = GetComponent<PopupAnim>();
            }

            public void BasicSetup(UnityAction _defaultBtnAct, UnityAction _allowAct, UnityAction<PowerUpPurchaseSO, short> _showPurchaseAct, int _currentUILevel)
            {
                for (short i = 0; i < powerUpSlots.Count; i++)
                {
                    powerUpSlots[i].BasicSetup(i, _defaultBtnAct, _allowAct, _showPurchaseAct, _currentUILevel);
                }

                defaultBtnAct = _defaultBtnAct;
                allowAct = _allowAct;

                currentMulIndex = 0;
                currentRepeatMulIndex = 0;

                bonusMulCoinAnimator = bonusMulCoinAnim.gameObject.GetComponent<Animator>();
                bonusMulCoinAnim.flyCoinAct = BonusMulAnimCallback;

                bonusRepeatMulCoinAnimator = bonusRepeatMulCoinAnim.gameObject.GetComponent<Animator>();
                bonusRepeatMulCoinAnim.flyCoinAct = BonusRepeatMulAnimCallback;

                flyCoinPS_1to2.gameObject.GetComponent<CommonParticleSystem>().afterParticleStopAct = allowAct;
                flyCoinPS_2to4.gameObject.GetComponent<CommonParticleSystem>().afterParticleStopAct = allowAct;

                flyCoinPS_1to2_Repeat.gameObject.GetComponent<CommonParticleSystem>().afterParticleStopAct = allowAct;
                flyCoinPS_2to4_Repeat.gameObject.GetComponent<CommonParticleSystem>().afterParticleStopAct = allowAct;
            }

            public void SettingsBeforeOpen(int indexLevel, UnityAction<int, int, List<IEnum.PowerUp>> _stageBtnAfterClickAct)
            {
                selectedStageLevelIndex = indexLevel;
                stageBtnAfterClickAct = _stageBtnAfterClickAct;

                LocalizedString locString = "GamePage/Level";
                stageTxt.text = locString + " " + (selectedStageLevelIndex + 1).ToString();

                for (int i = 0; i < powerUpSlots.Count; i++)
                {
                    powerUpSlots[i].SetUnlockText();
                }

                levelPass = AppGM.instance.CheckIfStageCleared(selectedStageLevelIndex);
                
                clearBonusSticker.SetActive(true);
                firstClearSticker.SetActive(!levelPass);

                bonusMulBtn.clickAct = defaultBtnAct;
                bonusMulBtn.afterClickAct = () =>
                {
                    ChangeMultiplier(levelPass);
                }; 
                bonusMulBtn.ButtonSetup();

                MulAreaSetup(levelPass);
            }

            public void UpdatePowerUpSlotInventory(short _slot)
            {
                powerUpSlots[_slot].UpdateSlotInventoryUI();
            }

            public void SelectPowerUpSlot(short _slot)
            {
                powerUpSlots[_slot].SelectPowerUp();
            }

            public void ResetAllSlots()
            {
                for (int i = 0; i < powerUpSlots.Count; i++)
                {
                    powerUpSlots[i].ResetSlotBtn();
                    powerUpSlots[i].UpdateSlotInventoryUI();
                }
            }

            void HandleEntryFeeRelatedLogic()
            {
                int fee = AppGM.instance.GetStandardEntryFee(selectedStageLevelIndex, GetBonusMultiplier(), out bool positive);

                stageEntryFee.text = fee.ToString();
                if (positive)
                {
                    goBtnImg.sprite = goBtnEnableImg;
                    stageEntryFee.color = ResourceUI.instance.FeeTxtPositiveBlueColor;
                }
                else
                {
                    goBtnImg.sprite = goBtnDisableImg;
                    stageEntryFee.color = ResourceUI.instance.FeeTxtNegativeRedColor;
                }

                // Setup Go Button
                goBtn.clickAct = defaultBtnAct;
                if (positive)
                {
                    goBtn.afterClickAct = () =>
                    {
                        stageBtnAfterClickAct(selectedStageLevelIndex, GetBonusMultiplier(), GetSelectedPowerUps());
                    };
                }
                else
                {
                    goBtn.afterClickAct = allowAct;
                }
                goBtn.ButtonSetup();
            }

            #region Bonus Multiplier
            void MulAreaSetup(bool _levelPass)
            {
                bonusMulCoinAnim.gameObject.SetActive(!_levelPass);
                bonusRepeatMulCoinAnim.gameObject.SetActive(_levelPass);

                if (!_levelPass)
                {
                    for (int i = 0; i < coinMul2List.Count; i++)
                    {
                        coinMul2List[i].gameObject.transform.localScale = Vector3.zero;
                    }
                    for (int i = 0; i < coinMul4List.Count; i++)
                    {
                        coinMul4List[i].gameObject.transform.localScale = Vector3.zero;
                    }

                    switch (currentMulIndex)
                    {
                        case 0:
                            break;
                        case 1:
                            for (int i = 0; i < coinMul2List.Count; i++)
                            {
                                coinMul2List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            break;
                        case 2:
                            for (int i = 0; i < coinMul2List.Count; i++)
                            {
                                coinMul2List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            for (int i = 0; i < coinMul4List.Count; i++)
                            {
                                coinMul4List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            break;
                    }
                }
                else
                {
                    for (int i = 0; i < coinRepeatMul2List.Count; i++)
                    {
                        coinRepeatMul2List[i].gameObject.transform.localScale = Vector3.zero;
                    }
                    for (int i = 0; i < coinRepeatMul4List.Count; i++)
                    {
                        coinRepeatMul4List[i].gameObject.transform.localScale = Vector3.zero;
                    }

                    switch (currentRepeatMulIndex)
                    {
                        case 0:
                            break;
                        case 1:
                            for (int i = 0; i < coinRepeatMul2List.Count; i++)
                            {
                                coinRepeatMul2List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            break;
                        case 2:
                            for (int i = 0; i < coinRepeatMul2List.Count; i++)
                            {
                                coinRepeatMul2List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            for (int i = 0; i < coinRepeatMul4List.Count; i++)
                            {
                                coinRepeatMul4List[i].gameObject.transform.localScale = Vector3.one;
                            }
                            break;
                    }
                }

                bonusMulTxt.text = "x " + GetBonusMultiplier().ToString();
                bonusMulShadowTxt.text = "x " + GetBonusMultiplier().ToString();
                HandleEntryFeeRelatedLogic();
            }
            
            void ChangeMultiplier(bool _levelPass)
            {
                if (!_levelPass)
                {
                    if (currentMulIndex < multiplierOptions.Length - 1)
                    {
                        currentMulIndex++;
                    }
                    else
                    {
                        currentMulIndex = 0;
                    }

                    switch (currentMulIndex)
                    {
                        case 0:
                            bonusMulCoinAnimator.SetBool("To1", true);
                            bonusMulCoinAnimator.SetBool("1to2", false);
                            bonusMulCoinAnimator.SetBool("2to4", false);
                            break;
                        case 1:
                            whiteStarPS.Clear();
                            whiteStarPS.Play();
                            bonusMulCoinAnimator.SetBool("1to2", true);
                            bonusMulCoinAnimator.SetBool("2to4", false);
                            bonusMulCoinAnimator.SetBool("To1", false);
                            break;
                        case 2:
                            whiteStarPS.Clear();
                            whiteStarPS.Play();
                            bonusMulCoinAnimator.SetBool("2to4", true);
                            bonusMulCoinAnimator.SetBool("1to2", false);
                            bonusMulCoinAnimator.SetBool("To1", false);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (currentRepeatMulIndex < repeatMultiplierOptions.Length - 1)
                    {
                        currentRepeatMulIndex++;
                    }
                    else
                    {
                        currentRepeatMulIndex = 0;
                    }

                    switch (currentRepeatMulIndex)
                    {
                        case 0:
                            bonusRepeatMulCoinAnimator.SetBool("To1", true);
                            bonusRepeatMulCoinAnimator.SetBool("1to2", false);
                            bonusRepeatMulCoinAnimator.SetBool("2to4", false);
                            break;
                        case 1:
                            whiteStarPS.Clear();
                            whiteStarPS.Play();
                            bonusRepeatMulCoinAnimator.SetBool("1to2", true);
                            bonusRepeatMulCoinAnimator.SetBool("2to4", false);
                            bonusRepeatMulCoinAnimator.SetBool("To1", false);
                            break;
                        case 2:
                            whiteStarPS.Clear();
                            whiteStarPS.Play();
                            bonusRepeatMulCoinAnimator.SetBool("2to4", true);
                            bonusRepeatMulCoinAnimator.SetBool("1to2", false);
                            bonusRepeatMulCoinAnimator.SetBool("To1", false);
                            break;
                        default:
                            break;
                    }
                }

                bonusMulTxt.text = "x " + GetBonusMultiplier().ToString();
                bonusMulShadowTxt.text = "x " + GetBonusMultiplier().ToString();

                HandleEntryFeeRelatedLogic();
            }


            /// <summary>
            /// Call from Animation
            /// </summary>
            [SkipRename]
            void BonusMulAnimCallback()
            {
                switch (currentMulIndex)
                {
                    case 0:
                        // temporarily Disable the Animator GameObject so the coin Images scale are not controlled by Animator,
                        // then perform the reset
                        bonusMulCoinAnimator.gameObject.SetActive(false);
                        for (int i = 0; i < coinMul2List.Count; i++)
                        {
                            coinMul2List[i].gameObject.transform.localScale = Vector3.zero;
                        }
                        for (int i = 0; i < coinMul4List.Count; i++)
                        {
                            coinMul4List[i].gameObject.transform.localScale = Vector3.zero;
                        }
                        bonusMulCoinAnimator.gameObject.SetActive(true);
                        allowAct();
                        break;
                    case 1:
                        flyCoinPS_1to2.Clear();
                        flyCoinPS_1to2.Play();
                        bonusMulTxtAnim.SetTrigger("enlarge");
                        break;
                    case 2:
                        flyCoinPS_2to4.Clear();
                        flyCoinPS_2to4.Play();
                        bonusMulTxtAnim.SetTrigger("enlarge");
                        break;
                    default:
                        break;
                }
            }

            /// <summary>
            /// Call from Animation
            /// </summary>
            [SkipRename]
            void BonusRepeatMulAnimCallback()
            {
                switch (currentRepeatMulIndex)
                {
                    case 0:
                        // temporarily Disable the Animator GameObject so the coin Images scale are not controlled by Animator,
                        // then perform the reset
                        bonusRepeatMulCoinAnimator.gameObject.SetActive(false);
                        for (int i = 0; i < coinRepeatMul2List.Count; i++)
                        {
                            coinRepeatMul2List[i].gameObject.transform.localScale = Vector3.zero;
                        }
                        for (int i = 0; i < coinRepeatMul4List.Count; i++)
                        {
                            coinRepeatMul4List[i].gameObject.transform.localScale = Vector3.zero;
                        }
                        bonusRepeatMulCoinAnimator.gameObject.SetActive(true);
                        allowAct();
                        break;
                    case 1:
                        flyCoinPS_1to2_Repeat.Clear();
                        flyCoinPS_1to2_Repeat.Play();
                        bonusMulTxtAnim.SetTrigger("enlarge");
                        break;
                    case 2:
                        flyCoinPS_2to4_Repeat.Clear();
                        flyCoinPS_2to4_Repeat.Play();
                        bonusMulTxtAnim.SetTrigger("enlarge");
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region Getter
            public PopupAnim GetAnim()
            {
                return popUpAnim;
            }

            public PopupCloseButton GetCloseBtn()
            {
                return closeBtn;
            }

            public CommonButton GetGoBtn()
            {
                return goBtn;
            }

            List<IEnum.PowerUp> GetSelectedPowerUps()
            {
                List<IEnum.PowerUp> activatedList = new List<IEnum.PowerUp>();

                for (int i = 0; i < powerUpSlots.Count; i++)
                {
                    if (powerUpSlots[i].IsSelected)
                    {
                        activatedList.Add(powerUpSlots[i].GetPowerUp());
                    }
                }

                return activatedList;
            }

            int GetBonusMultiplier()
            {
                if (!levelPass)
                {
                    return multiplierOptions[currentMulIndex];
                }
                else
                {
                    return repeatMultiplierOptions[currentRepeatMulIndex];
                }
            }
            #endregion
        }
    }
}
