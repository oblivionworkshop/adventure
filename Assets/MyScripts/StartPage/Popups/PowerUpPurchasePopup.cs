using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class PowerUpPurchasePopup : MonoBehaviour
        {
            PopupAnim popUpAnim;
            short fromSlotId;

            [SerializeField]
            PopupCloseButton closeBtn;
            [SerializeField]
            TMP_Text titleTxt, descriptionTxt, priceTxt;
            [SerializeField]
            Image powerUpImg, ImgShadow;
            [SerializeField]
            Image purchaseBtnImg;
            [SerializeField]
            CommonButton purchaseBtn;
            [SerializeField]
            HorizontalLayoutGroup purchaseBtnLayout;

            [Space(10f)]
            [SerializeField]
            Sprite purchaseBtnEnableImg;
            [SerializeField]
            Sprite purchaseBtnDisableImg;

            private void Awake()
            {
                popUpAnim = GetComponent<PopupAnim>();
            }

            public void InfoSetup(PowerUpPurchaseSO _data, short _slotID, int _currentIndexLevel, UnityAction<PowerUpPurchaseBtnData> _btnSetupcallback)
            {
                fromSlotId = _slotID;
                titleTxt.text = _data.powerUpName;
                descriptionTxt.text = _data.powerUpDescription;
                powerUpImg.sprite = ImgShadow.sprite = _data.powerUpImg;

                bool positive;
                int cost = AppGM.instance.GetPowerUpCost(_data.powerUp, _currentIndexLevel, out positive);

                priceTxt.text = cost.ToString();
                if (positive)
                {
                    purchaseBtnImg.sprite = purchaseBtnEnableImg;
                    priceTxt.color = ResourceUI.instance.FeeTxtPositiveWhiteColor;
                }
                else
                {
                    purchaseBtnImg.sprite = purchaseBtnDisableImg;
                    priceTxt.color = ResourceUI.instance.FeeTxtNegativeRedColor;
                }

                PowerUpPurchaseBtnData data = new PowerUpPurchaseBtnData(purchaseBtn, _data.powerUp, cost, fromSlotId, positive);

                _btnSetupcallback(data);
            }

            public PopupAnim GetAnim()
            {
                return popUpAnim;
            }

            public PopupCloseButton GetCloseBtn()
            {
                return closeBtn;
            }
        }
    }
}
