using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.Rendering;
using I2.Loc;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class PowerUpSlot : MonoBehaviour
        {
            public PowerUpSlotButton slotBtn;
            [SerializeField]
            PowerUpSlotButton lockSlotBtn;
            [SerializeField]
            Animator slotInventoryAnim;
            int amount;
            short slotID;
            bool isSelected;
            public bool IsSelected { get { return isSelected; } }

            [SerializeField]
            Image icon;
            [SerializeField]
            Image squareBtnImg, circleBtnImg;
            [SerializeField]
            TMP_Text slotInventoryAmountTxt;
            [SerializeField]
            Image slotInventoryAmountIcon;

            [Space(10f)]
            [SerializeField]
            TMP_Text unlockLevelTxt;
            [SerializeField]
            GameObject enableSet, lockSet;
            

            [Header("Sprites")]
            [SerializeField]
            Sprite plusIcon;
            [SerializeField]
            Sprite tickIcon, greenCircleBtn, pinkCircleBtn, greenSquareBtn, originalSqaureBtn;

            [Header("Power Up")]
            [SerializeField]
            PowerUpPurchaseSO powerUpData;

            UnityAction allowInputAct, defaultBtnClickAct;
            UnityAction<PowerUpPurchaseSO, short> showPurchasePopupAct;

            public void BasicSetup(short _slotIndex, UnityAction _defaultBtnClickAct, UnityAction _allowInputAct, UnityAction<PowerUpPurchaseSO, short> _showPurchasePopupAct, int _curUILevel)
            {
                bool unlock = _curUILevel >= powerUpData.unlockUIStage ? true : false;

                enableSet.SetActive(unlock);
                lockSet.SetActive(!unlock);

                defaultBtnClickAct = _defaultBtnClickAct;
                allowInputAct = _allowInputAct;

                if (unlock)
                {
                    ResetSlotBtn();

                    icon.sprite = powerUpData.powerUpImg;

                    slotID = _slotIndex;
                    showPurchasePopupAct = _showPurchasePopupAct;
                    SetupSlotUI();
                }
                else
                {
                    lockSlotBtn.clickAct = () => {
                        defaultBtnClickAct();
                    };
                    lockSlotBtn.afterClickAct = () => {
                        allowInputAct();
                    };
                    lockSlotBtn.ButtonSetup();
                }
            }

            void SetupSlotUI()
            {
                amount = AppGM.instance.GetPowerUpInventory(powerUpData.powerUp);
                if (amount <= 0)
                {
                    // Show Plus Icon
                    slotInventoryAmountTxt.gameObject.SetActive(false);
                    slotInventoryAmountIcon.sprite = plusIcon;
                    slotInventoryAmountIcon.gameObject.SetActive(true);

                    slotBtn.clickAct = () => {
                        defaultBtnClickAct();
                        slotInventoryAnim.SetTrigger("click");
                    };
                    slotBtn.afterClickAct = () => {
                        // Show Purchase Powerup popup
                        showPurchasePopupAct(powerUpData, slotID);
                    };
                    slotBtn.ButtonSetup();
                }
                else
                {
                    // Show Inventory Text
                    slotInventoryAmountIcon.gameObject.SetActive(false);
                    slotInventoryAmountTxt.text = amount.ToString();
                    slotInventoryAmountTxt.gameObject.SetActive(true);

                    slotBtn.clickAct = () => {
                        defaultBtnClickAct();
                        slotInventoryAnim.SetTrigger("click");
                    };
                    slotBtn.afterClickAct = () => {
                        SelectPowerUp();
                        allowInputAct();
                    };
                    slotBtn.ButtonSetup();
                }
            }

            public void UpdateSlotInventoryUI()
            {
                SetupSlotUI();
            }

            public void SelectPowerUp()
            {
                isSelected = true;
                squareBtnImg.sprite = greenSquareBtn;
                circleBtnImg.sprite = greenCircleBtn;
                slotInventoryAmountTxt.gameObject.SetActive(false);
                slotInventoryAmountIcon.sprite = tickIcon;
                slotInventoryAmountIcon.gameObject.SetActive(true);

                slotBtn.afterClickAct = () => {
                    UnselectPowerUp();
                    allowInputAct();
                };
            }

            void UnselectPowerUp()
            {
                isSelected = false;
                squareBtnImg.sprite = originalSqaureBtn;
                circleBtnImg.sprite = pinkCircleBtn;

                slotInventoryAmountIcon.gameObject.SetActive(false);
                slotInventoryAmountTxt.text = amount.ToString();
                slotInventoryAmountTxt.gameObject.SetActive(true);

                slotBtn.afterClickAct = () => {
                    SelectPowerUp();
                    allowInputAct();
                };
            }

            public void ResetSlotBtn()
            {
                isSelected = false;
                squareBtnImg.sprite = originalSqaureBtn;
                circleBtnImg.sprite = pinkCircleBtn;
            }

            public void SetUnlockText()
            {
                LocalizedString locString = "StartPage/BeforeStart/UnlockLv";
                unlockLevelTxt.text = locString + " " + powerUpData.unlockUIStage.ToString();
            }

            public IEnum.PowerUp GetPowerUp()
            {
                return powerUpData.powerUp;
            }
        }
    }
}
