using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class BonusMulCoinAnim : MonoBehaviour
        {
            public UnityEngine.Events.UnityAction flyCoinAct;

            [Beebyte.Obfuscator.SkipRename]
            /// <summary>
            /// Call from Animation
            /// </summary>
            void FlyCoinCallback()
            {
                if (flyCoinAct != null)
                {
                    flyCoinAct();
                }
            }
        }
    }
}
