﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop
{
    namespace StartPage
    {
        public class StartPageGM : MonoBehaviour
        {
            public static StartPageGM instance;

            public StartPageUI ui;
            [SerializeField]
            Camera startPageUICamera;
            public Camera StartPageUICamera { get { return startPageUICamera; } }

            public UnityAction<bool> blockInputAct;
            public UnityAction finishSetupAct;
            public UnityAction<IEnum.GameMode> enterTutorialGameScene;
            public UnityAction<int, int, List<IEnum.PowerUp>> stageBtnAfterClickAct;
            public UnityAction<IEnum.PowerUp, int> purchasePowerUpAct;

            private void Awake()
            {
                if (instance == null)
                {
                    instance = this;
                }
                else if (instance != this)
                {
                    Destroy(gameObject);
                }
            }

            public void SetupStartPageGM(int maxLevel)
            {
                // Setup UI
                ui.stageBtnAfterClickAct = stageBtnAfterClickAct;
                ui.tutorialBtnFinishClickAct = TutorialBtnClicked;
                ui.allowInputAct = blockInputAct;
                ui.UISetup(maxLevel);
                //------------------------------

                finishSetupAct();
            }

            public void LoadStagePageInstant()
            {
                ui.HomePageToStagePageInstant();
            }

            void TutorialBtnClicked()
            {
                enterTutorialGameScene(IEnum.GameMode.practice);
            }
        }
    }
}

        
