using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
	public class CoroutineManager : MonoBehaviour
	{
    	public void StartCoroutineStoppable(IEnumerator _IE)
        {
            StartCoroutine(_IE);
        }

        public void StopAllCoroutineStoppable()
        {
            StopAllCoroutines();
        }

    }
}
