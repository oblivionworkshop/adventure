﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

using Com.OblivionWorkshop.GamePage;
using Com.OblivionWorkshop.StartPage;
using Newtonsoft.Json;
using IngameDebugConsole;
using UnityEngine.EventSystems;

using CBS.Models;
using CBS.Scriptable;
using CBS;
using CBS.UI;
using UnityEngine.Analytics;
using System.Data;

namespace Com.OblivionWorkshop
{
    public class AppGM : MonoBehaviour
    {

        public static AppGM instance;

        [SerializeField]
        CoroutineManager coroutineManager;

        [Header("System Settings")]
        [SerializeField]
        bool isPC;
        [SerializeField]
        bool loadPrefs = true;
        [SerializeField]
        bool loadGameData = true;
        [SerializeField]
        int stageLimit = 100;           // Used in Start Page Stage Menu to set the number of stage button
        [SerializeField]
        bool walkBtnOnRightSide;
        [SerializeField]
        bool AdsFree;
        [SerializeField]
        IEnum.GameMode gameMode;

        static bool allowInput = false;


        /// <summary>
        /// This variable controls if the GameGM will load a custom start level
        /// </summary>
        [TooltipAttribute("This variable controls if the GameGM will load a custom start level. Need to turn off 'LoadGameData' before using this. Then, set the custom level in Challenge Manager")]
        [SerializeField]
        int startStage = 0;
        bool customStartLevelFromStageSelection = false;
        GameData gameDataHolder;

        [Header("Loading Screen Related")]
        [SerializeField]
        Camera topMostCamera;
        public Camera TopMostCamera { get { return topMostCamera; } }
        [SerializeField]
        LoadingScreen loadingScreen;
        [SerializeField]
        float minLoadTime;
        /// <summary>
        /// True when the Loading Screen Start Anim is completed. False when init & Loading Screen End Anim finishes
        /// </summary>
        bool loadingScreenOn = false;
        /// <summary>
        /// Used in SimpleScrollRect to determine scene ready
        /// </summary>
        [HideInInspector]
        public bool sceneReady;
        IEnum.SceneName currentScene = IEnum.SceneName.none;
        public IEnum.SceneName CurrentScene { get { return currentScene; } }

        [Header("Audio Related")]
        [SerializeField]
        AudioSource bgmPlayer;
        [SerializeField]
        AudioSource sfxPlayer;
        [SerializeField]
        AudioSource boostSFXPlayer;
        [SerializeField]
        [Range(0f,1f)]
        float bgmVolLv, sfxVolLv;       // Volume Preference set by player
        float bgmFinalVol;
        [SerializeField]
        bool bgmMute, sfxMute;          // Mute Preference set by player
        [SerializeField]
        SFXStoppablePlayer sfxStoppablePlayerPrefab;
        [SerializeField]
        Transform sfxStoppablePlayerParent;
        List<SFXStoppablePlayer> sfxStoppableList;
        [SerializeField]
        List<AudioClip> clipList = new List<AudioClip>();
        List<Sound> soundTracks = new List<Sound>();
        IEnum.AudioList currentBGM, prevBGM;

        Dictionary<IEnum.AudioList, Sound> audioDict = new Dictionary<IEnum.AudioList, Sound>();

        [Header("Data")]
        CharData charData;
        public readonly string foodUIPath = "Sprites/UI/Food/";

        int coin;
        int Coin {
            get { return coin; }
            set {
                coin = Mathf.Max(0, value);
            }
        }
        List<IEnum.PowerUp> selectedPowerUpList;
        float retryDiscount = 0.125f;
        float reviveFeeMul = 10f;

        int gameBonusMultiplier;

        // CBS Plugin
        private AuthData AuthData { get; set; }
        private IAuth Auth { get; set; }
        private PlayFab.ClientModels.LoginResult loginResult { get; set; }
        private IProfile ProfileModule { get; set; }
        private ICurrency CurrencyModule { get; set; }
        private string currencyCode;
        private string profileID { get; set; }

        private void Awake()
        {
            /*RefreshRate rate = new RefreshRate() { numerator = 60, denominator = 1};
            Screen.SetResolution(2048, 1536, FullScreenMode.Windowed, rate);*/

            if (instance == null)
            {
                instance = this;

                DontDestroyOnLoad(gameObject);

                if (loadPrefs)
                {
                    LoadPrefs();
                    PerformPrefsSettings();
                }

                if (loadGameData)
                {
                    LoadData();
                }

                Time.timeScale = 1.5f;
                SetupCharData();
                SetupAudioDict();
                SetupStoppableSFXList();
                SetupLoadingScreen();

#if UNITY_STANDALONE_WIN
                isPC = true;
                Application.targetFrameRate = 60;
                Screen.SetResolution(1024, 768, FullScreenMode.Windowed, new RefreshRate() { numerator = 60, denominator = 1 });
#endif

#if UNITY_ANDROID || UNITY_IOS
            isPC = false;
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
#endif
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            // CBS Init
            CurrencyModule = CBSModule.Get<CBSCurrencyModule>();
            ProfileModule = CBSModule.Get<CBSProfileModule>();
            //----------------------------------------------

            DebugLogConsole.AddCommand("Coin", "Add 10000 coins", AddCoinTest10000);
            DebugLogConsole.AddCommand("Resolution", "Show Resolution", ShowResolution);
            StartCoroutine(_StartScene());
        }

        void ShowResolution()
        {
            int w = UnityEngine.Screen.width;
            int h = UnityEngine.Screen.height;
            Debug.Log("Screen width = " + w + ", Screen height = " + h);
        }


        /*IEnumerator _StartScene()
        {
            Scene active;
            string sceneName;

            active = SceneManager.GetActiveScene();
            sceneName = active.name;
            Scene temp = SceneManager.CreateScene("Temp");

            // ---------------------Start Loading Page---------------------
            StartLoadingPage(true, saveGameData: false, _skipAnim: true);
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;
            yield return new WaitForSeconds(0.1f);
            //--------------------------------------------------------------

            if (active.isLoaded)
            {
                AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(active, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);


                while (!asyncUnload.isDone)
                {
                    yield return null;
                }

                yield return new WaitForEndOfFrame();

                // Check if there is game data & startStage = 0.
                // If no game data & startStage = 0, then it is first time entering the game. Move instantly to Game Page and show Tutorial Panel
                // If no game data but startStage != 0, then can assume it is running the testing for a specific level
                if (gameDataHolder == null && startStage == 0)
                {
                    Debug.Log("gameDataHolder = NULL & startStage = 0");
                    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);

                    while (!asyncLoad.isDone || Time.unscaledTime - loadStartTime < minLoadTime)
                    {
                        yield return null;
                    }

                    yield return new WaitForEndOfFrame();

                    StartLoadingPage(false);

                    SetupCurrentScene(SceneManager.GetSceneByName("Game"));
                    SceneManager.sceneLoaded += SceneSetup;
                    sceneReady = true;
                    Debug.Log("AppGM: First Time Added Scene Load Event");

                }
                else
                {
                    if (gameDataHolder != null)
                    {
                        Debug.Log("gameDataHolder != NULL!");
                    }
                    if (startStage != 0)
                    {
                        Debug.Log("startStage = " + startStage);
                        customStartLevelFromStageSelection = true;
                    }

                    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);

                    while (!asyncLoad.isDone || Time.unscaledTime - loadStartTime < minLoadTime)
                    {
                        yield return null;
                    }

                    yield return new WaitForEndOfFrame();

                    StartLoadingPage(false);


                    SetupCurrentScene(SceneManager.GetSceneByName(sceneName));
                    SceneManager.sceneLoaded += SceneSetup;
                    sceneReady = true;
                    Debug.Log("AppGM: Added Scene Load Event");

                }
            }
        }*/

        IEnumerator _StartScene()
        {
            Scene active;
            string sceneName;

            active = SceneManager.GetActiveScene();
            sceneName = active.name;
            Scene temp = SceneManager.CreateScene("Temp");
            SceneManager.sceneLoaded += SceneSetup;
            AllowInput(false);

            // ---------------------Start Loading Page---------------------
            StartLoadingPage(true, saveGameData: false, _skipAnim: true);
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;
            yield return new WaitForSeconds(0.1f);
            //--------------------------------------------------------------

            if (active.isLoaded)
            {
                AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(active, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);


                while (!asyncUnload.isDone)
                {
                    yield return null;
                }

                yield return new WaitForEndOfFrame();

                //-----------------Network Init-----------------
                AuthData = CBSScriptable.Get<AuthData>();
                Auth = CBSModule.Get<CBSAuthModule>();

                // check auto login
                bool autoLogin = AuthData.AutoLogin;
                if (autoLogin)
                {
                    Auth.AutoLogin(onAuth =>
                    {
                        if (onAuth.IsSuccess)
                        {
                            StartCoroutine(_DetermineSceneAfterLogin(sceneName, loadStartTime, onAuth.Result, onAuth.ProfileID));
                        }
                        else
                        {
                            StartCoroutine(ShowLoginScreen(loadStartTime));
                        }
                    });
                }
                else
                {
                    StartCoroutine(ShowLoginScreen(loadStartTime));
                }
                //----------------------------------
            }
        }

        IEnumerator ShowLoginScreen(float _loadStartTime)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("AdventureLogin", LoadSceneMode.Single);

            while (!asyncLoad.isDone || Time.unscaledTime - _loadStartTime < minLoadTime)
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();

            StartLoadingPage(false);

            sceneReady = true;
            Debug.Log("Scene Management: Show Login Screen");
        }

        /// <summary>
        /// This is a common function. Whichever way of login (manual login / auto login) will bypass this function
        /// </summary>
        /// <param name="_sceneName"></param>
        /// <param name="_loadStartTime"></param>
        /// <param name="_loginResult"></param>
        /// <param name="_profileID"></param>
        /// <returns></returns>
        IEnumerator _DetermineSceneAfterLogin(string _sceneName, float _loadStartTime, PlayFab.ClientModels.LoginResult _loginResult, string _profileID)
        {
            loginResult = _loginResult;
            profileID = _profileID;

            // ----------------Setup Currency-----------------
            // check cache currencies
            if (AuthData.PreloadCurrency)
            {
                GetCurrencyInfo(CurrencyModule.CacheCurrencies);
            }
            else
            {
                CurrencyModule.GetProfileCurrencies(profileID, OnGetCurrencies);
            }
            //----------------------------------------------

            // Check if there is game data & startStage = 0.
            // If no game data & startStage = 0, then it is first time entering the game. Move instantly to Game Page and show Tutorial Panel
            // If no game data but startStage != 0, then can assume it is running the testing for a specific level
            if (gameDataHolder == null && startStage == 0)
            {
                Debug.Log("gameDataHolder = NULL & startStage = 0");
                AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);

                while (!asyncLoad.isDone || Time.unscaledTime - _loadStartTime < minLoadTime)
                {
                    yield return null;
                }

                yield return new WaitForEndOfFrame();

                StartLoadingPage(false);

                sceneReady = true;
                Debug.Log("AppGM: First Time Load Game");

            }
            else
            {
                if (gameDataHolder != null)
                {
                    Debug.Log("gameDataHolder != NULL!");
                }
                if (startStage != 0)
                {
                    Debug.Log("startStage = " + startStage);
                    customStartLevelFromStageSelection = true;
                }

                AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_sceneName, LoadSceneMode.Single);

                while (!asyncLoad.isDone || Time.unscaledTime - _loadStartTime < minLoadTime)
                {
                    yield return null;
                }

                yield return new WaitForEndOfFrame();

                StartLoadingPage(false);

                sceneReady = true;
                Debug.Log("AppGM: Added Scene Load Event");
            }
        }

        private void OnLoginComplete(CBSLoginResult result)
        {
            if (result.IsSuccess)
            {
                if (AdventureLoginForm.instance != null)
                {
                    AdventureLoginForm.instance.OnLogined -= OnLoginComplete;
                }

                StartCoroutine(_OnLoginComplete(result));

                Debug.Log("Scene Management: On Login Complete");
            }
        }

        private IEnumerator _OnLoginComplete(CBSLoginResult _result)
        {
            AllowInput(false);
            StopAllEssentialRunningLogic(true);

            if (!loadingScreenOn)
            {
                StartLoadingPage(true);
            }
           
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;

            yield return new WaitForSeconds(0.1f);

            StartCoroutine(_DetermineSceneAfterLogin("StartPage", loadStartTime, _result.Result, _result.ProfileID));
        }

        void SceneSetup(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("----------Scene Setup-------------");

            foreach (IEnum.SceneName name in System.Enum.GetValues(typeof(IEnum.SceneName)))
            {
                if(name.ToString() == scene.name)
                {
                    currentScene = name;
                }
            }

            switch (currentScene)
            {
                case IEnum.SceneName.StartPage:
                    // Hard Code approach to compensate that 'startPage' var will be initialised when StartPage Scene loaded so it will = 0.
                    // The best method should be set startStage Var to 'static' to avoid initialised when scene loaded. However, this will make testing inconvenient
                    if (loadGameData)
                    {
                        startStage = gameDataHolder == null ? 0 : gameDataHolder.highestLevel;
                    }

                    customStartLevelFromStageSelection = false;

                    StartPageGM.instance.enterTutorialGameScene = TutorialGameStart;
                    StartPageGM.instance.stageBtnAfterClickAct = GoToGamePageByLevel;
                    StartPageGM.instance.blockInputAct = AllowInput;

                    StartPageGM.instance.finishSetupAct = StartPageTitleFinishSetup;

                    StartPageGM.instance.SetupStartPageGM(GetPlayerStageMaxProgress() + 1);

                    ResourceUI.instance.Activate(true);
                    ResourceUI.instance.SetupByScene(currentScene, GetNormalUICamera());

                    break;
                case IEnum.SceneName.Game:
                    GameGM.instance.gameOverAct = GameOver;
                    GameGM.instance.selectStageAct = GoToStageSelectionPage;
                    GameGM.instance.gameRestartAct = GameRestart;
                    GameGM.instance.charReviveAct = CharRevive;
                    GameGM.instance.charInvulnerableAct = CharInvulnerable;
                    GameGM.instance.quitGameSceneAct = GoToStartPage;
                    GameGM.instance.allowInputAct = AllowInput;
                    GameGM.instance.pauseGameAct = PauseGame;
                    GameGM.instance.bgmMuteAct = MuteBGM;
                    GameGM.instance.sfxMuteAct = MuteSFX;
                    GameGM.instance.bgmVolChangeAct = ChangeBGMVol;
                    GameGM.instance.sfxVolChangeAct = ChangeSFXVol;
                    GameGM.instance.determineControlWalkBtnRightAct = RecordControlPanelPrefs;
                    GameGM.instance.incomingLevelAlertAct = IncomingLevelAlert;
                    GameGM.instance.enterLastSubLevelAct = PlayRandomGameDifficultBGM;
                    GameGM.instance.passLevelAct = PassLevel;
                    GameGM.instance.finishSetupAct = GamePageFinishSetup;
                    GameGM.instance.forceSaveGameDataAct = SaveGameData;
                    GameGM.instance.firstTimeEnterGame_TutorialMenuOKBtnAct = () =>
                    {
                        SaveGameData(saveToDB:false);
                        GameGM.instance.WriteLevelDict(gameDataHolder.clearStage);
                    };
                    GameGM.instance.playAdAct = PlayAd;
                    GameGM.instance.gameEndAct = ToBeContinued;

                    PassPreGameDataToGame();

                    ResourceUI.instance.Activate(true);
                    ResourceUI.instance.SetupByScene(currentScene, GetNormalUICamera());

                    switch (gameMode)
                    {
                        case IEnum.GameMode.none:
                            break;
                        case IEnum.GameMode.practice:
                            GameGM.instance.SetupGameGM(charData, gameMode);
                            break;
                        case IEnum.GameMode.traditional:
                            break;
                        case IEnum.GameMode.challenge:
                            if (gameDataHolder != null && gameDataHolder.clearStage != null)
                            {
                                GameGM.instance.WriteLevelDict(gameDataHolder.clearStage);
                            }

                            if (customStartLevelFromStageSelection)
                            {
                                GameGM.instance.SetupGameGM(charData, gameMode, true, startStage);
                            }
                            else
                            {
                                GameGM.instance.SetupGameGM(charData, gameMode, firstTimePlay: gameDataHolder == null ? true : false);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case IEnum.SceneName.AdventureLogin:
                    AdventureLoginForm.instance.showLoadingAct = StartLoadingPage;
                    AdventureLoginForm.instance.allowInputAct = AllowInput;
                    AdventureLoginForm.instance.BasicSetup();
                    AdventureLoginForm.instance.OnLogined += OnLoginComplete;

                    ResourceUI.instance.Activate(false);
                    ResourceUI.instance.SetupByScene(currentScene, GetNormalUICamera());
                    break;
                default:
                    Debug.LogError("Cannot Determine the Active Scene!");
                    break;
            }
        }

        #region Page Setup
        void StartPageTitleFinishSetup()
        {
            PlayBGM(IEnum.AudioList.startPageTitle);
            AllowInput(true);
        }

        void GamePageFinishSetup()
        {
            PlayRandomGameBGM();
            AllowInput(true);
        }
        #endregion

        #region Page Navigation
        void StartLoadingPage(bool start, bool saveGameData = true, bool _skipAnim = false)
        {
            if (start)
            {
                loadingScreen.StartLoading(true, _skipAnim);
                
                if (saveGameData)
                {
                    SaveGameData(saveToDB:true);
                }
            }
            else
            {
                loadingScreen.StartLoading(false);
            }
        }

        void GoToStartPage()
        {
            StartCoroutine(_GoToStartPage());
        }

        IEnumerator _GoToStartPage()
        {
            AllowInput(false);
            StopAllEssentialRunningLogic(true);

            StartLoadingPage(true);
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;

            yield return new WaitForSeconds(0.1f);

            ChallengeBlockManager.instance.ResetChallengeBlockManager();

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("StartPage", LoadSceneMode.Single);

            while (!asyncLoad.isDone || Time.unscaledTime - loadStartTime < minLoadTime)
            {
                yield return null;
            }

            StartLoadingPage(false);

            Debug.Log("Scene Management: Go To Start Page");
        }

        void GoToStageSelectionPage()
        {
            StartCoroutine(_GoToStageSelectionPage());
        }

        IEnumerator _GoToStageSelectionPage()
        {
            AllowInput(false);
            StopAllEssentialRunningLogic(true);

            StartLoadingPage(true);
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;

            yield return new WaitForSeconds(0.1f);

            ChallengeBlockManager.instance.ResetChallengeBlockManager();

            Scene temp = SceneManager.CreateScene("Temp");
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync("Game", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

            while (!asyncUnload.isDone)
            {
                yield return null;
            }
            yield return new WaitForEndOfFrame();
            AsyncOperation clear = Resources.UnloadUnusedAssets();
            while (!clear.isDone)
            {
                yield return null;
            }
            Debug.Log("Scene Management: Unloaded Unused Assets");

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("StartPage", LoadSceneMode.Single);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();
            StartPageGM.instance.LoadStagePageInstant();
            yield return new WaitForEndOfFrame();

            while (Time.unscaledTime - loadStartTime < minLoadTime)
            {
                yield return null;
            }

            StartLoadingPage(false);

            Debug.Log("Scene Management: Go To Stage Selection Page");
        }

        void GoToGamePageByLevel(int level, int mul, List<IEnum.PowerUp> _activatedPowerUps)
        {
            Debug.Log("Load Game Scene & start at Level " + (level + 1).ToString());

            startStage = level;
            customStartLevelFromStageSelection = true;
            gameBonusMultiplier = mul;

            AddCoin(CalculateStandardEntryFee(level, mul) * -1);
            selectedPowerUpList = new List<IEnum.PowerUp>(_activatedPowerUps);

            GoToGamePage();
        }

        void GoToGamePage()
        {
            StartCoroutine(_GoToGamePage());
        }

        IEnumerator _GoToGamePage()
        {
            AllowInput(false);
            StopAllEssentialRunningLogic(true);

            StartLoadingPage(true);
            while (!loadingScreenOn)
            {
                yield return null;
            }
            float loadStartTime = Time.unscaledTime;

            yield return new WaitForSeconds(0.1f);

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);

            while (!asyncLoad.isDone || Time.unscaledTime - loadStartTime < minLoadTime)
            {
                yield return null;
            }

            StartLoadingPage(false);

            Debug.Log("Scene Management: Go To Game Page");
        }

        void TutorialGameStart(IEnum.GameMode _mode)
        {
            gameMode = _mode;
            GoToGamePageByLevel(GetPlayerStageMaxProgress(), 0, new List<IEnum.PowerUp>());
        }
        #endregion

        #region Input Management
        void AllowInput(bool allow)
        {
            Debug.Log("<color=green>Allow Input = " + allow + "</color>");
            allowInput = allow;
        }

        public bool CanInput()
        {
            return allowInput;
        }
        #endregion

        #region Audio Related
        void PlayRandomGameBGM()
        {
            if (currentBGM != IEnum.AudioList.none)
            {
                prevBGM = currentBGM;
            }

            int randonNum = Random.Range(1, 3);
            switch (randonNum)
            {
                case 1:
                    PlayBGM(IEnum.AudioList.gamePageNormal_1);
                    break;
                case 2:
                    PlayBGM(IEnum.AudioList.gamePageNormal_2);
                    break;
                default:
                    PlayBGM(IEnum.AudioList.gamePageNormal_1);
                    break;
            }
        }

        void PlayRandomGameDifficultBGM(int level)
        {
            if (currentBGM != IEnum.AudioList.none)
            {
                prevBGM = currentBGM;
            }

            float fadePrevTime;
            if (bgmPlayer.isPlaying)
            {
                fadePrevTime = 1.5f;
            }
            else
            {
                fadePrevTime = 0f;
            }

            if ((level + 1) % 5 == 0)
            {
                PlayBGM(IEnum.AudioList.gamePageDifficult_2, fadePrevTime);
            }
            else if ((level + 1) % 3 == 0)
            {
                PlayBGM(IEnum.AudioList.gamePageDifficult_3, fadePrevTime);
            }
            else
            {
                PlayBGM(IEnum.AudioList.gamePageDifficult_1, fadePrevTime);
            }
        }

        void PlayRandomGameOverSFX()
        {
            int randonNum = Random.Range(1, 3);
            switch (randonNum)
            {
                case 1:
                    PlaySFX(IEnum.AudioList.gameOver1);
                    break;
                case 2:
                    PlaySFX(IEnum.AudioList.gameOver2);
                    break;
                default:
                    PlaySFX(IEnum.AudioList.gameOver1);
                    break;
            }
        }

        void MuteBGM(bool mute)
        {
            Debug.Log("AppGM: MuteBGM " + mute.ToString());
            bgmMute = mute;
            bgmPlayer.mute = bgmMute;
            PlayerPrefs.SetString("bgmMute", mute == true ? "True" : "False");
        }

        void ChangeBGMVol(float vol)
        {
            bgmVolLv = vol;
            bgmPlayer.volume = vol * audioDict[currentBGM].volume;
            PlayerPrefs.SetFloat("bgmVolLv", vol);
        }

        void MuteSFX(bool mute)
        {
            Debug.Log("AppGM: MuteSFX " + mute.ToString());
            sfxMute = mute;
            sfxPlayer.mute = sfxMute;
            boostSFXPlayer.mute = mute;
            MuteAllStoppableSFX(mute);
            PlayerPrefs.SetString("sfxMute", mute == true ? "True" : "False");
        }

        void ChangeSFXVol(float vol)
        {
            sfxVolLv = vol;
            PlayerPrefs.SetFloat("sfxVolLv", vol);
        }

        /// <summary>
        /// This function is called when the BGM is forcibly stopped by StopCoroutine() and need to reset the BGM settings
        /// </summary>
        void ResetBGM()
        {
            bgmPlayer.volume = bgmFinalVol;
        }

        public void PlayBGM()
        {
            bgmPlayer.Play();
        }

        public void PlayBGM(IEnum.AudioList _bgm, float fadeTime = 0f)
        {
            IEnumerator IEBgm = _PlayBGM(_bgm, fadeTime);
            StartCoroutineStoppable(IEBgm);
        }

        IEnumerator _PlayBGM(IEnum.AudioList bgm, float fadePrevTime)
        {
            if (fadePrevTime <= 0f)
            {
                bgmPlayer.Stop();
                yield return null;
            }
            else
            {
                FadeBGM(fadePrevTime);
                yield return new WaitForSeconds(fadePrevTime);
            }

            currentBGM = bgm;
            bgmPlayer.clip = audioDict[bgm].clip;
            bgmFinalVol = audioDict[bgm].volume * bgmVolLv;
            bgmPlayer.volume = bgmFinalVol;
            bgmPlayer.pitch = audioDict[bgm].pitch;

            bgmPlayer.Play();
        }

        public void PlaySFX(IEnum.AudioList sfx)
        {
            sfxPlayer.PlayOneShot(audioDict[sfx].clip, audioDict[sfx].volume * sfxVolLv);
        }

        public void PlayStoppableSFX(IEnum.AudioList audio, float expireTime = 10f, float _playbackTime = 0f, float? _pitch = null)
        {
            int listTotal = sfxStoppableList.Count;

            if (listTotal > 0)
            {
                for (int i = 0; i < listTotal; i++)
                {
                    if (!sfxStoppableList[i].IsUsing())
                    {
                        sfxStoppableList[i].PlayStoppableSFX(audioDict[audio], sfxVolLv, expireTime, sfxMute, _playbackTime, _pitch);
                        break;
                    }

                    if (i == listTotal - 1)
                    {
                        // Create new prefab in case all the current players are using
                        SFXStoppablePlayer sfxPlayer = CreateNewStoppableSFXPlayer();
                        sfxPlayer.PlayStoppableSFX(audioDict[audio], sfxVolLv, expireTime, sfxMute, _playbackTime, _pitch);
                    }
                }
            }
            else
            {
                SFXStoppablePlayer sfxPlayer = CreateNewStoppableSFXPlayer();
                sfxPlayer.PlayStoppableSFX(audioDict[audio], sfxVolLv, expireTime, sfxMute, _playbackTime, _pitch);
            }
        }

        SFXStoppablePlayer CreateNewStoppableSFXPlayer()
        {
            SFXStoppablePlayer sfxPlayer = Instantiate(sfxStoppablePlayerPrefab, sfxStoppablePlayerParent);
            sfxStoppableList.Add(sfxPlayer);
            sfxPlayer.BasicSetup();

            return sfxPlayer;
        }

        void StopAllStoppableSFX()
        {
            for (int i = 0; i < sfxStoppableList.Count; i++)
            {
                sfxStoppableList[i].StopStoppableSFX();
            }
        }

        void PauseAllStoppableSFX(bool pause)
        {
            for (int i = 0; i < sfxStoppableList.Count; i++)
            {
                sfxStoppableList[i].PauseStoppableSFX(pause);
            }
        }

        void MuteAllStoppableSFX(bool mute)
        {
            for (int i = 0; i < sfxStoppableList.Count; i++)
            {
                sfxStoppableList[i].MuteStoppableSFX(mute);
            }
        }

        public void PlayBoostSFX()
        {
            boostSFXPlayer.time = 0.2f;
            boostSFXPlayer.pitch = Random.Range(0.8f, 1.2f);
            boostSFXPlayer.volume = 0.2f;

            boostSFXPlayer.Play();
        }

        public void StopBoostSFX()
        {
            boostSFXPlayer.Stop();
        }

        void FadeBGM(float duration, float targetVolume = 0f, bool stopPlayer = true, bool removeClip = false)
        {
            IEnumerator IE = _FadeBGM(duration, targetVolume, stopPlayer, removeClip);
            StartCoroutineStoppable(IE);
        }

        IEnumerator _FadeBGM(float _duration, float _targetVolume, bool _stopPlayer, bool _removeClip)
        {
            float startTime = Time.time;
            float start = bgmPlayer.volume;
            int counter = 0;
            //int limit = 1000;

            while (Time.time < startTime + _duration)
            {
                //if (counter <= limit)
                //{
                    bgmPlayer.volume = Mathf.Lerp(start, _targetVolume, (Time.time - startTime) / _duration);
                    counter++;
                    yield return null;
                //}
                //else
                //{
                //    Debug.LogError("_FadeBGM Counter > " + limit.ToString() + " and Break!");
                //    break;
                //}
            }

            if (_stopPlayer)
            {
                bgmPlayer.Stop();
            }

            if (_removeClip)
            {
                bgmPlayer.clip = null;
            }

            // Reset its original volume for revive etc. purpose, which will Play the BGMPlayer again directly
            bgmPlayer.volume = bgmFinalVol;
        }

        void IncomingLevelAlert()
        {
            IEnumerator IE = _IncomingLevelAlert();
            StartCoroutineStoppable(IE);
        }

        IEnumerator _IncomingLevelAlert()
        {
            FadeBGM(1.25f);
            yield return new WaitUntil(() => GameGM.instance.GetGameState() == IEnum.GameState.playing);
            yield return new WaitForSeconds(0.36f);

            /*for (int i = 0; i < 3; i++)
            {
                PlayStoppableSFX(IEnum.AudioList.alert, 5f);
                yield return new WaitUntil(() => GameGM.instance.GetGameState() == IEnum.GameState.playing);
                yield return new WaitForSeconds(0.75f);
            }*/
        }

        void SetupStoppableSFXList()
        {
            sfxStoppableList = new List<SFXStoppablePlayer>();

            for (int i = 0; i < sfxStoppablePlayerParent.childCount; i++)
            {
                SFXStoppablePlayer player = sfxStoppablePlayerParent.GetChild(i).GetComponent<SFXStoppablePlayer>();
                sfxStoppableList.Add(player);
                player.BasicSetup();
            }
        }

        void SetupAudioDict()
        {
            soundTracks.Add(new Sound(IEnum.AudioList.startPageTitle, clipList[0], 0.35f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gamePageNormal_1, clipList[1], 0.5f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gamePageNormal_2, clipList[2], 0.5f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gamePageDifficult_1, clipList[3], 0.35f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gamePageDifficult_2, clipList[4], 0.3f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gamePageDifficult_3, clipList[5], 0.25f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.buttonClick, clipList[6], 0.5f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.alert, clipList[7], 0.5f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.passSubLevel, clipList[8], 0.3f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.passLevel, clipList[9], 0.25f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gameOver1, clipList[10], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.gameOver2, clipList[11], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.dash1, clipList[12], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.dash2, clipList[13], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.dash3, clipList[14], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.dash4, clipList[15], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.eatPop, clipList[16], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.eatPowerup, clipList[17], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.dieFromTrap, clipList[18], 0.8f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.blockOnceBreak, clipList[19], 0.1f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.bounceBouncy, clipList[20], 0.1f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.bounceStatic, clipList[21], 0.1f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.revive, clipList[22], 1f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.buttonError, clipList[23], 0.1f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.boostFullNotification, clipList[24], 0.2f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.getCoin, clipList[25], 0.15f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.popupOpen, clipList[26], 0.4f, 1f));
            soundTracks.Add(new Sound(IEnum.AudioList.popupClose, clipList[27], 0.4f, 1f));

            for (int i = 0; i < soundTracks.Count; i++)
            {
                audioDict.Add(soundTracks[i].identifier, soundTracks[i]);
            }
        }
        #endregion

        #region Game Logic Handling
        void PauseGame(bool _pause)
        {
            PauseAllStoppableSFX(_pause);
        }

        void RecordControlPanelPrefs(bool _walkBtnOnRightSide)
        {
            walkBtnOnRightSide = _walkBtnOnRightSide;
            PlayerPrefs.SetString("walkBtnOnRightSide", _walkBtnOnRightSide == true ? "True" : "False");
        }

        void PassLevel(int passedLevel, int mul, bool firstTimePass)
        {
            AddCoin(CalculatePassLevelReward(passedLevel, mul, firstTimePass), false, firstTimePass? 5 * mul : 2 * mul);
            SaveGameData(saveToDB:true);

            IEnumerator IE = _PassLevel();
            StartCoroutineStoppable(IE);
        }

        IEnumerator _PassLevel()
        {
            FadeBGM(0.5f);
            PlayStoppableSFX(IEnum.AudioList.passLevel);
            yield return new WaitForSeconds(3f);
            PlayRandomGameBGM();
        }

        void StopAllEssentialRunningLogic(bool killTween)
        {
            // Stop all Coroutine in Dict that is still running which may affect following actions
            StopAllIEnumeratorRecord();
            //------------------------------------------

            // Stop all SFXStoppablePlayers
            StopAllStoppableSFX();
            //------------------------------------------------------

            // Run the following Reset Code to make sure that, even the Coroutines are forcibly stopped, it is resumed correctly
            ResetAffectedIEnumeratorsLogic();
            //------------------------------------------------------------------

            // Stop All Tweening
            if (killTween)
            {
                DG.Tweening.DOTween.KillAll();
            }
            //------------------------------------------------------------------
        }

        void GameOver()
        {
            StopAllEssentialRunningLogic(false);

            FadeBGM(0.4f);
            PlayRandomGameOverSFX();
        }

        void ResetAffectedIEnumeratorsLogic()
        {
            ResetBGM();
        }

        void GameRestart()
        {
            PlayRandomGameBGM();
        }

        void CharRevive()
        {
            PlayStoppableSFX(IEnum.AudioList.revive, 3f);
            PlayBGM();
        }

        void CharInvulnerable(bool invulnerable)
        {
            SetCharTrapCollisionLogic(invulnerable);
        }
        
        /// <summary>
        /// Turn On/Off 'Char' & 'Trap' Layer collision
        /// </summary>
        /// <param name="ignoreCollisionWithTrap"></param>
        void SetCharTrapCollisionLogic(bool ignoreCollisionWithTrap)
        {
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Character"), LayerMask.NameToLayer("Trap"), ignoreCollisionWithTrap);
        }

        [ContextMenu("SetCharTrapCollisionLogic")]
        void SetCharTrapCollisionLogicTest()
        {
            Debug.Log("SetCharTrapCollisionLogic");
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Character"), LayerMask.NameToLayer("Trap"), true);
        }
        #endregion

        #region Character Data
        void SetupCharData()
        {
            charData = new CharData(IEnum.CharacterType.rabbit, 5, 0);
        }
        #endregion

        #region Game Record Handling
        void LoadPrefs()
        {
            bool myBool = false;
            if (System.Boolean.TryParse(PlayerPrefs.GetString("bgmMute", "False"), out myBool))
            {
                bgmMute = myBool;
            }
            else
            {
                bgmMute = false;
            }
            if (System.Boolean.TryParse(PlayerPrefs.GetString("sfxMute", "False"), out myBool))
            {
                sfxMute = myBool;
            }
            else
            {
                sfxMute = false;
            }

            bgmVolLv = PlayerPrefs.GetFloat("bgmVolLv", 1f);
            sfxVolLv = PlayerPrefs.GetFloat("sfxVolLv", 1f);

            if (System.Boolean.TryParse(PlayerPrefs.GetString("walkBtnOnRightSide", "True"), out myBool))
            {
                walkBtnOnRightSide = myBool;
            }
            else
            {
                walkBtnOnRightSide = true;
            }
        }

        void PerformPrefsSettings()
        {
            bgmPlayer.mute = bgmMute;
            sfxPlayer.mute = sfxMute;
            boostSFXPlayer.mute = sfxMute;

            bgmPlayer.volume = bgmVolLv;
            sfxPlayer.volume = sfxVolLv;
            boostSFXPlayer.volume = sfxVolLv;
        }

        public void ClearPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("<color=magenta>-------------- ALL PLAYER PREFS DELETED! -------------</color>");
        }


        #endregion

        #region Ads Handling
        void PlayAd(int _reviveFee)
        {
            StartCoroutine(_PlayAd(_reviveFee));
        }

        IEnumerator _PlayAd(int _fee)
        {
            Debug.Log("Play Advertisement");
            yield return new WaitForSeconds(3f);
            Debug.Log("Finish Advertisement");
            AddCoin(_fee * -1);
            GameGM.instance.ReviveChar();
        }
        #endregion

        #region To Be Continued Handling
        void ToBeContinued()
        {
            FadeBGM(3.5f);
        }
        #endregion

        #region IEnumerator Handling
        public void StartCoroutineStoppable(IEnumerator IE)
        {
            coroutineManager.StartCoroutineStoppable(IE);
        }

        [ContextMenu("Force Stop IE")]
        void StopAllIEnumeratorRecord()
        {
            coroutineManager.StopAllCoroutineStoppable();
        }
        #endregion

        #region Game Resource Management
        public void AddCoin(int amount, bool skipAnim = true, int animCoinNum = 0)
        {
            Coin += amount;
            
            if (!string.IsNullOrEmpty(profileID))
            {
                if (amount > 0)
                {
                    CurrencyModule.AddCurrencyToProfile(profileID, currencyCode, amount, OnAddCurrency);
                }
                else
                {
                    CurrencyModule.SubtractCurrencyFromProfile(profileID, currencyCode, amount, OnSubtractCurrency);
                }
            }

            if (skipAnim)
            {
                ResourceUI.instance.AddCoin(amount, true);
                if (amount > 0)
                {
                    PlayStoppableSFX(IEnum.AudioList.getCoin, 2f, _pitch: 0.75f);
                }
                else
                {

                }
            }
            else
            {
                // Case for play Animation
                if (GameGM.instance != null)
                {
                    // ------------It is in Game Page------------
                    GameGM.instance.PlayGetCoinAnim(
                    () =>
                    {
                        ResourceUI.instance.AddCoin(amount, false);
                    },
                    () =>
                    {
                        PlayStoppableSFX(IEnum.AudioList.getCoin, 2f, _pitch: 0.75f);
                    }
                     , animCoinNum);
                }
                else
                {
                    // ------------It is NOT in Game Page------------

                }
            }
        }

        private void OnAddCurrency(CBSUpdateCurrencyResult result)
        {
            if (result.IsSuccess)
            {
                var balanceChange = result.BalanceChange;
                var updatedCurrency = result.UpdatedCurrency;
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }

        private void OnSubtractCurrency(CBSUpdateCurrencyResult result)
        {
            if (result.IsSuccess)
            {
                var balanceChange = result.BalanceChange;
                var updatedCurrency = result.UpdatedCurrency;
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }

        public int GetStandardEntryFee(int _indexLevel, int _mul, out bool isPositive)
        {
            int fee = CalculateStandardEntryFee(_indexLevel, _mul);
            isPositive = Coin >= fee ? true : false;
            return fee;
        }

        public int GetDiscountedEntryFee(int _indexLevel, int _mul, out bool isPositive)
        {
            int fee = CalculateStandardEntryFee(_indexLevel, _mul);

            float discounted = ((float)fee) * retryDiscount;
            // Magic calculation to rount number as nearest 10
            int discountFee = (Mathf.CeilToInt(discounted / 10f)) * 10;

            isPositive = Coin >= discountFee ? true : false;
            return discountFee;
        }

        public int GetReviveFee(int _indexLevel, int _mul, out bool isPositive)
        {
            int fee = CalculateStandardEntryFee(_indexLevel, _mul);

            float multiplied = ((float)fee) * reviveFeeMul;
            // Magic calculation to rount number as nearest 10
            int reviveFee = (Mathf.CeilToInt(multiplied / 10f)) * 10;

            isPositive = Coin >= reviveFee ? true : false;
            return reviveFee;
        }

        public int GetPassLevelReward(int _indexLevel, int _mul, bool _firstPass)
        {
            int reward = CalculatePassLevelReward(_indexLevel, _mul, _firstPass);
            return reward;
        }

        int CalculateStandardEntryFee(int _indexLV, int _multiplier)
        {
            return ((_indexLV + 1) * 100) * _multiplier;
        }

        int CalculatePassLevelReward(int _indexLV, int _multiplier, bool _first)
        {
            if (_first)
            {
                return ((_indexLV + 1) * 400) * _multiplier;
            }
            else
            {
                return ((_indexLV + 1) * 150) * _multiplier;
            }
        }

        public int GetPowerUpCost(IEnum.PowerUp _powerUp, int _indexLevel, out bool isPositive)
        {
            int fee = CalculatePowerUpCost(_powerUp, _indexLevel);
            isPositive = Coin >= fee ? true : false;
            return fee;
        }

        int CalculatePowerUpCost(IEnum.PowerUp power, int _indexLV)
        {
            switch (power)
            {
                case IEnum.PowerUp.none:
                    Debug.LogError("Cannot determine Power Up Type to calculate cost!");
                    break;
                case IEnum.PowerUp.carrotTrio:
                    return (_indexLV + 1) * 60;
                case IEnum.PowerUp.bubblyShield:
                    return (_indexLV + 1) * 120;
                case IEnum.PowerUp.extraHop:
                    return (_indexLV + 1) * 240;
                default:
                    Debug.LogError("Cannot determine Power Up Type to calculate cost!");
                    break;
            }
            return _indexLV * 10000;
        }

        public void PurchasePowerUp(IEnum.PowerUp _power, int _price)
        {
            AddCoin(_price * -1);
            AddPowerUpToInventory(_power, 1);
        }

        void AddPowerUpToInventory(IEnum.PowerUp _powerUpType, int amount)
        {
            if (gameDataHolder == null)
            {
                Debug.LogError("gameDataHolder is NULL which is not expected!");
                CreateGameDataHolder();
            }

            if (gameDataHolder.powerUpInventory == null)
            {
                // Create Dict
                Debug.LogError("Doesn't expect the gameDataHolder have not setup powerUpInventory!");
                gameDataHolder.powerUpInventory = CreatePowerUpInventory();
            }

            if (gameDataHolder.powerUpInventory.ContainsKey(_powerUpType.ToString()))
            {
                gameDataHolder.powerUpInventory.TryGetValue(_powerUpType.ToString(), out int prev);
                gameDataHolder.powerUpInventory[_powerUpType.ToString()] = prev + amount;
            }
            else
            {
                Debug.LogError("Cannot find the target power up key!");
            }
        }

        public int GetPowerUpInventory(IEnum.PowerUp _power)
        {
            if (gameDataHolder == null)
                return 0;
            if (gameDataHolder.powerUpInventory == null)
                return 0;
            if (!gameDataHolder.powerUpInventory.ContainsKey(_power.ToString()))
                return 0;

            return gameDataHolder.powerUpInventory[_power.ToString()];
        }

        void PassPreGameDataToGame()
        {
            if (selectedPowerUpList != null)
            {
                GameGM.instance.SetPowerUpList(selectedPowerUpList);

                // Remove the Power Up from Inventory
                for (int i = 0; i < selectedPowerUpList.Count; i++)
                {
                    if (gameDataHolder != null)
                    {
                        if (gameDataHolder.powerUpInventory != null)
                        {
                            if (gameDataHolder.powerUpInventory.ContainsKey(selectedPowerUpList[i].ToString()))
                            {
                                gameDataHolder.powerUpInventory[selectedPowerUpList[i].ToString()] -= 1;
                            }
                            else
                            {
                                Debug.LogError("gameDataHolder.powerUpInventory does not contain Key: " + selectedPowerUpList[i].ToString());
                            }
                        }
                        else
                        {
                            Debug.LogError("gameDataHolder.powerUpInventory is NULL");
                        }
                    }
                    else
                    {
                        Debug.LogError("gameDataHolder is NULL!");
                    }
                }
                //----------------------------------

                selectedPowerUpList.Clear();
            }

            GameGM.instance.SetGameBonusMultiplier(gameBonusMultiplier);
            gameBonusMultiplier = 0;

        }
        #endregion

        #region Local Save Load
        void WriteGameData(GameData data)
        {
            Debug.Log("<color=orange>---------Write Game Data---------</color>");
            BinaryFormatter bf = new BinaryFormatter();
            //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
            FileStream file = File.Create(Application.persistentDataPath + "/savedGD.gd");
            bf.Serialize(file, data);
            file.Close();
        }

        GameData ReadGameData()
        {
            Debug.Log("<color=orange>---------Read Game Data---------</color>");
            Debug.Log("<color=orange>Path:" + Application.persistentDataPath + "</color>");

            if (File.Exists(Application.persistentDataPath + "/savedGD.gd"))
            {
                Debug.Log("<color=gren>Get Game Data File Successfully!</color>");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/savedGD.gd", FileMode.Open);
                GameData data = (GameData)bf.Deserialize(file);
                Debug.Log(JsonConvert.SerializeObject(data));
                file.Close();

                return data;
            }

            Debug.Log("<color=red> NO GAME DATA FILE</color>");
            return null;
        }

        void LoadData()
        {
            Debug.Log("Load Data");
            gameDataHolder = new GameData();
            gameDataHolder = ReadGameData();

            if (gameDataHolder != null)
            {
                Coin = gameDataHolder.coin;

                if (gameDataHolder.highestLevel >= 0)
                {
                    startStage = gameDataHolder.highestLevel;
                    Debug.Log("Start Stage = " + startStage);
                }
            }
            else
            {
                // First Time enter game, no data found
                Coin = 5000;
            }
        }

        private void OnGetCurrencies(CBSGetCurrenciesResult result)
        {
            if (result.IsSuccess)
            {
                GetCurrencyInfo(result.Currencies);
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }

        private void GetCurrencyInfo(Dictionary<string, CBSCurrency> currencyDict)
        {
            foreach (var currencyInstance in currencyDict)
            {
                currencyCode = currencyInstance.Value.Code;
                Coin = currencyInstance.Value.Value;
                bool isRecharchable = currencyInstance.Value.Rechargeable;
                int maxRecharge = currencyInstance.Value.MaxRecharge;
                int secondsToNextRecharge = currencyInstance.Value.SecondsToRecharge;
            }
        }

        void SaveGameData(bool saveToDB)
        {
            Debug.Log("Save Game Data");

            if (gameDataHolder == null)
            {
                // First Time save
                CreateGameDataHolder();
            }

            if (GameGM.instance != null)
            {
                GameGM.instance.PrepareGameData(ref gameDataHolder);
            }

            gameDataHolder.coin = GetCoinAmount();

            string json = JsonConvert.SerializeObject(gameDataHolder);
            Debug.Log(json);
            WriteGameData(gameDataHolder);

            // Backup to Cloud if Internet access
            if (saveToDB && Application.internetReachability != NetworkReachability.NotReachable)
            {
                Dictionary<string, string> dataDict = new Dictionary<string, string>();
                dataDict.Add("gameDataHolder", json);
                ProfileModule.SaveProfileData(dataDict, OnNetWorkSave);
            }
        }

        private void OnNetWorkSave(CBSBaseResult result)
        {
            if (result.IsSuccess)
            {
                Debug.Log("Success!");
            }
            else
            {
                Debug.Log(result.Error.Message);
            }
        }

        /// <summary>
        /// Create and Setup gameDataHolder when no data is found
        /// </summary>
        void CreateGameDataHolder()
        {
            gameDataHolder = new GameData();
            gameDataHolder.clearStage = CreatePassedLevelDict();
            gameDataHolder.powerUpInventory = CreatePowerUpInventory();
        }

        Dictionary<string, bool> CreatePassedLevelDict()
        {
            Dictionary<string, bool> _dict = new Dictionary<string, bool>();

            for (int i = 0; i < GetMaxStage(); i++)
            {
                _dict.Add(i.ToString(), false);
            }

            return _dict;
        }

        Dictionary<string, int> CreatePowerUpInventory()
        {
            Dictionary<string, int> _dict = new Dictionary<string, int>();

            string[] _array = System.Enum.GetNames(typeof(IEnum.PowerUp));
            for (int i = 0; i < _array.Length; i++)
            {
                _dict.Add(_array[i], 0);
            }

            return _dict;
        }

        public void DeleteGameData()
        {
            File.Delete(Application.persistentDataPath + "/savedGD.gd");
            Debug.Log("<color=magenta>-------------- ALL GAME DATA DELETED! -------------</color>");
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                if (SceneManager.GetActiveScene().name == "Game")
                {
                    SaveGameData(saveToDB:false);
                }
            }
        }

        private void OnApplicationQuit()
        {
            SaveGameData(saveToDB:false);
        }

        private void OnApplicationFocus(bool focus)
        {
#if !UNITY_EDITOR
            if (!focus)
            {
                SaveGameData(saveToDB:true);
            }
#endif
        }
        #endregion

        #region Loading Screen
        void SetupLoadingScreen()
        {
            loadingScreenOn = false;
            loadingScreen.afterLoadingCloseAnimAct = (input) => { loadingScreenOn = false; AllowInput(input); };
            loadingScreen.afterLoadingStartAnimAct = () => { loadingScreenOn = true; };
        }
        #endregion

        #region Getter
        public bool GetIsPC()
        {
            return isPC;
        }

        public int GetCoinAmount()
        {
            return Coin;
        }

        public UIAudioDataClass GetAudioSettings()
        {
            UIAudioDataClass audio = new UIAudioDataClass();
            audio.bgmMute = bgmMute;
            audio.sfxMute = sfxMute;
            audio.bgmVol = bgmVolLv;
            audio.sfxVol = sfxVolLv;

            return audio;
        }

        public int GetMaxStage()
        {
            return stageLimit;
        }

        public int GetPlayerStageMaxProgress()
        {
            if (gameDataHolder == null)
                return 0;

            // By Default extract the highest possible stage from ChallengeBlockManager
            int maxPossibleLevel = ChallengeBlockManager.instance.GetMaxBlockLevel();
            //--------------------------------------------------------------------------

            return Mathf.Min(gameDataHolder.highestLevel, maxPossibleLevel);
        }

        public bool GetIfWalkBtnOnRight()
        {
            return walkBtnOnRightSide;
        }

        public bool GetIfAdsFree()
        {
            return AdsFree;
        }

        public Camera GetNormalUICamera()
        {
            if (GameGM.instance != null)
            {
                return GameGM.instance.GameUICamera;
            }
            else if (StartPageGM.instance != null)
            {
                return StartPageGM.instance.StartPageUICamera;
            }
            else if (AdventureLoginForm.instance != null)
            {
                return Camera.main;
            }
            else
            {
                Debug.LogError("Cannot Determine UI Camera! Will return Main Camera!!");
                return Camera.main;
            }
        }

        public bool CheckIfStageCleared(int indexLevel)
        {
            if (gameDataHolder != null)
            {
                if (gameDataHolder.clearStage != null)
                {
                    if (gameDataHolder.clearStage.ContainsKey(indexLevel.ToString()))
                    {
                        if (gameDataHolder.clearStage.TryGetValue(indexLevel.ToString(), out bool pass))
                        {
                            return pass;
                        }
                        else
                        {
                            Debug.LogError("Failed to get value from gameDataHolder.clearStage!");
                            return true;
                        }
                    }
                    else
                    {
                        Debug.LogError("gameDataHolder.clearStage does not cotains Key!");
                        return false;
                    }
                }
                else
                {
                    Debug.LogError("gameDataHolder.clearStage is NULL! Cannot check if level is passed");
                    return true;
                }
            }
            else
            {
                Debug.LogError("gameDataHolder is NULL! Cannot check if level is passed");
                return true;
            }
        }
        #endregion

        #region Testing
        [ContextMenu("PlaySFX")]
        void PlaySFXTest()
        {
            sfxPlayer.Play();
        }

        [ContextMenu("Add 1000 Coin")]
        void AddCoinTest()
        {
            AddCoin(1000);
        }

        void AddCoinTest10000()
        {
            AddCoin(10000);
            if (GameGM.instance)
            {
                GameGM.instance.RefreshGameOverUI();
            }
        }

        [ContextMenu("Remove All Coin")]
        void RemoveAllCoinTest()
        {
            AddCoin(-999999);
        }
        #endregion
    }
}