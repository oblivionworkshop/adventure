﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class GameGM : MonoBehaviour
        {
            public static GameGM instance;
            Transform _transform;

            [SerializeField]
            bool allowMoveBack;
            [SerializeField]
            bool unlimitedDash;

            [SerializeField]
            Camera gameUICamera;
            public Camera GameUICamera { get { return gameUICamera; } }
            [SerializeField]
            PracticeBlockManager practiceBlockManager;
            [SerializeField]
            ChallengeBlockManager challengeBlockManager;
            [SerializeField]
            BackgroundManager backgroundManager;
            [SerializeField]
            PoolManager poolManager;
            [SerializeField]
            CharController mainChar;
            [SerializeField]
            CameraController cameraController;

            public GameUI gameUI;

            IEnum.GameMode gameMode;

            [SerializeField]
            BlockBase startBlock;

            IEnum.GameState gameState = IEnum.GameState.none;

            [Space(10f)]
            public Color32 roughBlockColor;
            public Color32 fadeBlockColor;

            bool moveBtnPressed, boostBtnPressed;

            float minBoostEnergyRequire = 0.05f;
            float boostEnergyCost = 0.45f;

            int maxDashEnergy, startDashEnergy;
            int _currentDashEnergy;
            int currentDashEnergy
            {
                get { return _currentDashEnergy; }
                set
                {
                    int _prev = _currentDashEnergy;
                    _currentDashEnergy = Mathf.Clamp(value, 0, maxDashEnergy);
                    gameUI.UpdateDashEnergyPanel(_prev, _currentDashEnergy);
                }
            }

            [Header("Coin Generation")]
            [SerializeField]
            float coinBarSpeedMul;
            [SerializeField]
            [Tooltip("An one-off bonus to Coin Bar when dash")]
            float coinBarDashBonus = 0.45f;
            [SerializeField]
            float coinBarFadeSpeed;
            bool allowMoveGenCoin;
            float _coinBarVal;
            float coinBarVal
            {
                get { return _coinBarVal; }
                set
                {
                    _coinBarVal = Mathf.Clamp(value, 0f, 1f);
                    gameUI.UpdateCoinBar(_coinBarVal);
                }
            }

            // Pre-Game Data
            List<IEnum.PowerUp> powerUpList;
            int gameBonusMultiplier;
            public int GameBonusMultiplier { get { return gameBonusMultiplier; } }
            //---------------------------------

            // Used to determine if col is inside Camera range
            Camera cam;
            Plane[] planes;
            //-------------------------------------------------

            // Record Handling
            Vector2Int failLevel;
            int failCounter = 0;
            Dictionary<string, bool> clearStageBonusDict;       // used to check if the player has received the Clear Stage Bonus of that stage
            //----------------------------------------------------

            [Header("Library")]
            [SerializeField]
            List<Food> foodPrefabList;
            [SerializeField]
            GameObject stepOnStarExplodePS;
            IEnum.CharacterType charType;

            [Header("Parents")]
            [SerializeField]
            Transform stepOnStarEffectsParent;

            public UnityAction<bool> allowInputAct;
            public UnityAction<bool> bgmMuteAct, sfxMuteAct, determineControlWalkBtnRightAct, pauseGameAct;
            public UnityAction<bool> charInvulnerableAct, forceSaveGameDataAct;
            public UnityAction<float> bgmVolChangeAct, sfxVolChangeAct;
            public UnityAction gameOverAct, gameRestartAct, charReviveAct, selectStageAct, quitGameSceneAct, finishSetupAct, incomingLevelAlertAct;
            public UnityAction firstTimeEnterGame_TutorialMenuOKBtnAct, gameEndAct;
            public UnityAction<int> enterLastSubLevelAct, playAdAct;
            public UnityAction<int, int, bool> passLevelAct;

            Dictionary<int, Rigidbody2D> rdTrapDict = new Dictionary<int, Rigidbody2D>();
            int rdTrapDictIndexer;

            Dictionary<int, Animator> pauseAnimDict = new Dictionary<int, Animator>();
            int pauseAnimDictIndexer;

            private void Awake()
            {
                if (instance == null)
                {
                    instance = this;
                }
                else if (instance != this)
                {
                    Destroy(gameObject);
                }
                _transform = gameObject.transform;
                allowMoveGenCoin = false;
            }

            private void Start()
            {
                cam = Camera.main;
            }

            void InitGameGMVar()
            {
                failLevel = new Vector2Int(-1, -1);
                failCounter = 0;

                coinBarVal = 0;
            }

            public void SetupGameGM(CharData charData, IEnum.GameMode mode, bool loadCustomLevel = false, int startLevel = 0, bool firstTimePlay = false)
            {
                gameState = IEnum.GameState.playing;
                charType = charData.charType;

                InitGameGMVar();
                cameraController.Init();

                // UI related setup
                SetupGameUIActions();
                gameUI.GameUISetup(charData, firstTimePlay);
                //---------------------------

                backgroundManager.BGSetup();

                ChooseBlockManager(mode, loadCustomLevel, startLevel);

                SetupChar(charData);

                ApplyPowerUps();

                if (firstTimePlay)
                {
                    gameUI.PauseGame();
                }

                finishSetupAct();
            }

            void SetupGameUIActions()
            {
                gameUI.allowInputAct = allowInputAct;

                gameUI.selectStageAct = selectStageAct;
                gameUI.retryAct = GameRestart;
                gameUI.reviveAct = ReviveProcedure;
                gameUI.skipStageAct = SkipStage;

                gameUI.moveBtnDownAct = MoveBtnPressed;
                gameUI.moveBtnUpAct = MoveBtnReleased;
                gameUI.boostBtnDownAct = BoostBtnPressed;
                gameUI.boostBtnUpAct = BoostBtnReleased;
                gameUI.dashBtnDownAct = DashBtnPressed;

                gameUI.pauseMenuOnAct = PauseGame;
                gameUI.pauseMenuOffAct = ResumeGame;
                gameUI.quitBtnClickAct = quitGameSceneAct;

                gameUI.bgmBtnAfterPressAct = bgmMuteAct;
                gameUI.sfxBtnAfterPressAct = sfxMuteAct;
                gameUI.bgmVolChangeAct = bgmVolChangeAct;
                gameUI.sfxVolChangeAct = sfxVolChangeAct;

                gameUI.tutorialMenuOKBtnAct = () =>
                {
                    firstTimeEnterGame_TutorialMenuOKBtnAct();
                    gameBonusMultiplier = 1;
                    ResumeGame();
                };

                gameUI.determineControlWalkBtnRightAct = determineControlWalkBtnRightAct;
            }

            void ChooseBlockManager(IEnum.GameMode mode, bool loadCustomLevel, int startLevel)
            {
                switch (mode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        practiceBlockManager.GameStart();
                        break;
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        challengeBlockManager = ChallengeBlockManager.instance;
                        gameUI.ShowChallengeGameModeUI(true);
                        AllowMoveGenerateCoin();
                        challengeBlockManager.incomingLevelAlertAct = IncomingLevelAlert;
                        challengeBlockManager.passLevelAct = PassLevel;
                        challengeBlockManager.enterLastSubLevelAct = enterLastSubLevelAct;
                        challengeBlockManager.SetCustomStartLevel(loadCustomLevel, startLevel);
                        challengeBlockManager.GameStart();
                        break;
                    default:
                        break;
                }
                gameMode = mode;
            }

            void GameRestart(int _fee)
            {
                AppGM.instance.AddCoin(_fee * -1);
                StartCoroutine("_GameRestart");
            }

            IEnumerator _GameRestart()
            {
                gameState = IEnum.GameState.playing;
                gameRestartAct();
                gameUI.ShowGameOverMenu(false);

                mainChar.CharRestart();
                currentDashEnergy = startDashEnergy;

                cameraController.InstantFocus(mainChar.GetPos());
                cameraController.AllowCamFollow(true);

                backgroundManager.ActivateBGParticle(true);

                coinBarVal = 0;
                powerUpList = new List<IEnum.PowerUp>();

                ResourceUI.instance.DisableOpenCoinShopBtn();
                ResourceUI.instance.ShowOnTop(false);

                rdTrapDictIndexer = 0;
                rdTrapDict.Clear();

                PauseAllTrapsPowerupsAnim(false);
                pauseAnimDictIndexer = 0;
                pauseAnimDict.Clear();

                poolManager.ReturnAllBullets();

                // After Confirming the ball pos, spawn first block in block manager
                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.practice:
                        practiceBlockManager.ClearAllBlocks();
                        yield return new WaitForEndOfFrame();
                        practiceBlockManager.GameRestart(true);
                        break;
                    case IEnum.GameMode.traditional:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.challenge:
                        yield return new WaitForEndOfFrame();
                        challengeBlockManager.SetCustomStartLevel(true, challengeBlockManager.GetCurrentLevel().x);
                        challengeBlockManager.GameRestart(true);
                        break;
                    default:
                        yield return new WaitForEndOfFrame();
                        break;
                }

                allowInputAct(true);
            }

            void SkipStage()
            {
                StartCoroutine(_SkipStage());
            }

            IEnumerator _SkipStage()
            {
                gameState = IEnum.GameState.playing;
                gameRestartAct();
                gameUI.ShowGameOverMenu(false);
                mainChar.CharRestart();

                cameraController.InstantFocus(mainChar.GetPos());
                cameraController.AllowCamFollow(true);

                backgroundManager.ActivateBGParticle(true);

                coinBarVal = 0;
                powerUpList = new List<IEnum.PowerUp>();

                ResourceUI.instance.DisableOpenCoinShopBtn();
                ResourceUI.instance.ShowOnTop(false);

                rdTrapDictIndexer = 0;
                rdTrapDict.Clear();

                PauseAllTrapsPowerupsAnim(false);
                pauseAnimDictIndexer = 0;
                pauseAnimDict.Clear();

                poolManager.ReturnAllBullets();

                // After Confirming the ball pos, spawn first block in block manager
                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.practice:
                        practiceBlockManager.ClearAllBlocks();
                        yield return new WaitForEndOfFrame();
                        practiceBlockManager.GameRestart(true);
                        break;
                    case IEnum.GameMode.traditional:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.challenge:
                        yield return new WaitForEndOfFrame();
                        challengeBlockManager.SkipStage(1);
                        challengeBlockManager.SetCustomStartLevel(true, challengeBlockManager.GetCurrentLevel().x);
                        challengeBlockManager.GameRestart(true);
                        break;
                    default:
                        yield return new WaitForEndOfFrame();
                        break;
                }

                forceSaveGameDataAct(true);
                allowInputAct(true);
            }

            public void GameOver()
            {
                gameState = IEnum.GameState.gameOver;
                gameOverAct();
                cameraController.AllowCamFollow(false);
                mainChar.CharDestroy();

                // UI related
                StopIncomingLevelAlert();
                DetermineGameOverMenu(UpdateFailCounter());
                gameUI.TurnOnBoostAnim(false);

                ResourceUI.instance.SetupOpenCoinShopBtn();
                ResourceUI.instance.ShowOnTop(true);
                //--------------------------------------

                backgroundManager.ActivateBGParticle(false);

                PauseAllTrapsPowerupsAnim(true);
            }

            /// <summary>
            /// Used to refersh the GameOver Scene, especially when Coin changed, the UI that requires coin needs to be updated
            /// </summary>
            public void RefreshGameOverUI()
            {
                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        break;
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        gameUI.SetupRetryFeeAndBtn(challengeBlockManager.GetCurrentLevel().x);
                        gameUI.SetupReviveFeeAndBtn(challengeBlockManager.GetCurrentLevel().x);
                        break;
                    default:
                        break;
                }
            }

            public void GameEnd()
            {
                StartCoroutine(_GameEnd());
            }

            IEnumerator _GameEnd()
            {
                gameUI.PauseGame();
                gameUI.TriggerToBeContinuedAnim();
                gameEndAct();

                yield return new WaitForSeconds(5f);

                quitGameSceneAct();
            }

            int UpdateFailCounter()
            {
                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        break;
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        Vector2Int currentFailLevel = challengeBlockManager.GetCurrentLevel();
                        if (failLevel != currentFailLevel)
                        {
                            failLevel = currentFailLevel;
                        }
                        else
                        {
                            failCounter++;
                        }
                        break;
                    default:
                        break;
                }

                return failCounter;
            }

            void DetermineGameOverMenu(int counter)
            {
                gameUI.ShowGameOverMenu(true, true, false, challengeBlockManager.GetCurrentLevel().x);

                // For skip stage, need to set mechanics for game highest level -5 does not allow skip stage so to avoid player in max level & skip stage

                /*if (counter >= 10 && IsHisHighestLevel)
                {
                    gameUI.ShowGameOverMenu(true, true, true);
                }
                else if (counter >= 10)
                {
                    gameUI.ShowGameOverMenu(true, true);
                }
                else if (counter == 2 || counter % 5 == 0)
                {
                    gameUI.ShowGameOverMenu(true, true);
                }
                else
                {
                    gameUI.ShowGameOverMenu(true);
                }*/
            }

            void SetupChar(CharData data)
            {
                mainChar.charAfterDeathAct = GameOver;
                mainChar.charStartDashAct = CharStartDash;
                mainChar.charEndDashAct = CharEndDash;
                mainChar.charStartBoostAct = CharStartBoost;
                mainChar.charStopBoostAct = CharStopBoost;
                mainChar.allowInputAct = allowInputAct;
                mainChar.invulnerableAct = CharInvulnerable;
                mainChar.eatFoodAct = CharEatFood;
                mainChar.stopCamFollowAct = StopCameraFollow;

                mainChar.CharSetup();

                maxDashEnergy = data.maxEnergy;
                currentDashEnergy = data.startEnergy;
                startDashEnergy = data.startEnergy;
            }

            void CharStartDash()
            {
                currentDashEnergy--;

                // Play Sound
                int ran = Random.Range(1, 5);
                switch (ran)
                {
                    case 1:
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dash1, 4f);
                        break;
                    case 2:
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dash2, 4f);
                        break;
                    case 3:
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dash3, 4f);
                        break;
                    case 4:
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dash4, 4f);
                        break;
                    default:
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dash1, 4f);
                        break;
                }
                //-----------------------------------
            }

            void CharEndDash()
            {

            }

            void CharStartBoost()
            {
                AppGM.instance.PlayBoostSFX();
            }

            void CharStopBoost()
            {
                AppGM.instance.StopBoostSFX();
            }

            void CharEatFood()
            {
                AppGM.instance.PlayStoppableSFX(IEnum.AudioList.eatPop, 3f);
                AppGM.instance.PlayStoppableSFX(IEnum.AudioList.eatPowerup, 3f);
                if (currentDashEnergy < maxDashEnergy)
                {
                    currentDashEnergy++;
                }
            }

            void StopCameraFollow()
            {
                cameraController.AllowCamFollow(false);
            }

            void IncomingLevelAlert()
            {
                incomingLevelAlertAct();
                //gameUI.PlayAlertScreenAnim();
            }

            void StopIncomingLevelAlert()
            {
                gameUI.StopAlertSceenAnim();
            }

            void PassLevel(int level)
            {
                currentDashEnergy++;
                backgroundManager.StageLevelUp();
                gameUI.PlayPassLevelConfetti();

                bool _firstTimePass = false;

                if (clearStageBonusDict == null)
                {
                    Debug.LogError("clearStageBonusDict is NULL!!");
                    return;
                }
                else
                {
                    if (!clearStageBonusDict.ContainsKey(level.ToString()))
                    {
                        // Case of cannot find the level key of whatever reason
                        Debug.LogError("clearStageBonusDict does not contain Key for " + level.ToString() + " !!");
                        clearStageBonusDict.Add(level.ToString(), true);
                        _firstTimePass = true;
                    }
                    else
                    {
                        if (clearStageBonusDict.TryGetValue(level.ToString(), out bool _passed))
                        {
                            if (!_passed)
                            {
                                // Case of First Time Passing Level
                                clearStageBonusDict[level.ToString()] = true;
                                _firstTimePass = true;
                            }
                            else
                            {
                                // Case of Passing Level more than 1 time
                                _firstTimePass = false;
                            }
                        }
                    }
                }
                    

                passLevelAct(level, gameBonusMultiplier, _firstTimePass);
            }

            void MoveBtnPressed(UnityEngine.EventSystems.PointerEventData data)
            {
                moveBtnPressed = true;
#if _DEBUG
                    Debug.Log("<color=green>MoveBtnPressed = true</color>");
#endif
            }

            void MoveBtnReleased(UnityEngine.EventSystems.PointerEventData data)
            {
                moveBtnPressed = false;
#if _DEBUG
                    Debug.Log("<color=green>MoveBtnPressed = false</color>");
#endif
            }

            void BoostBtnPressed(UnityEngine.EventSystems.PointerEventData data)
            {
                boostBtnPressed = true;
#if _DEBUG
                    Debug.Log("<color=green>BoostBtnPressed = true</color>");
#endif
            }

            void BoostBtnReleased(UnityEngine.EventSystems.PointerEventData data)
            {
                boostBtnPressed = false;
#if _DEBUG
                    Debug.Log("<color=green>BoostBtnPressed = false</color>");
#endif
            }

            void DashBtnPressed(UnityEngine.EventSystems.PointerEventData data)
            {
                if (!AppGM.instance.GetIsPC())
                {
                    mainChar.DashCommand();
                }
#if _DEBUG
                    Debug.Log("<color=green>BoostBtnPressed = true</color>");
#endif
            }

            #region Power Up Before Game
            public void SetPowerUpList(List<IEnum.PowerUp> _powerUpList)
            {
                powerUpList = new List<IEnum.PowerUp>(_powerUpList);
            }

            public void SetGameBonusMultiplier(int _mul)
            {
                gameBonusMultiplier = _mul;
            }
            
            void ApplyPowerUps()
            {
                if (powerUpList != null)
                {
                    if (powerUpList.Count > 0)
                    {
                        for (int i = 0; i < powerUpList.Count; i++)
                        {
                            switch (powerUpList[i])
                            {
                                case IEnum.PowerUp.none:
                                    break;
                                case IEnum.PowerUp.carrotTrio:
                                    currentDashEnergy += 3;
                                    break;
                                case IEnum.PowerUp.bubblyShield:
                                    break;
                                case IEnum.PowerUp.extraHop:
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            #endregion

            #region Pause Related

            void PauseGame()
            {
                gameState = IEnum.GameState.pause;
                allowInputAct(false);
                pauseGameAct(true);
                backgroundManager.PauseBGParticle(true);
                mainChar.PauseGame(true);
                PauseAllTrapsRD(true);
                PauseAllTrapsPowerupsAnim(true);
                gameUI.PauseAllTween();
            }

            void ResumeGame()
            {
                gameState = IEnum.GameState.playing;
                allowInputAct(true);
                pauseGameAct(false);
                backgroundManager.PauseBGParticle(false);
                mainChar.PauseGame(false);
                PauseAllTrapsRD(false);
                PauseAllTrapsPowerupsAnim(false);
                gameUI.ResumeAllTween();
            }

            void PauseAllTrapsRD(bool pause)
            {
                foreach (int key in rdTrapDict.Keys)
                {
                    rdTrapDict[key].simulated = !pause;
                }
            }

            void PauseAllTrapsPowerupsAnim(bool pause)
            {
                foreach (int key in pauseAnimDict.Keys)
                {
                    pauseAnimDict[key].speed = pause ? 0f : 1f;
                }
            }

            public void RegRDIntoDict(Rigidbody2D rd, UnityAction<int> setAct)
            {
                if (rd != null)
                {
                    rdTrapDict.Add(rdTrapDictIndexer, rd);
                }
                else
                {
                    Debug.LogError("rd is null & cannot reg into RD Dict!");
                }
                
                if (setAct != null)
                {
                    setAct(rdTrapDictIndexer);
                }
                else
                {
                    Debug.LogError("setAct is null & cannot reg into RD Dict!");
                }

                rdTrapDictIndexer++;
            }

            public void RemoveRDFromDict(int num)
            {
                rdTrapDict.Remove(num);
#if _DEBUG_Trap
                Debug.Log("Removed " + num + ". Remaining are = ");

                foreach (int item in rdTrapDict.Keys)
                {
                    Debug.Log("RemoveRDFromDict :: RD Trap Dict key = " + item + " , RD gameObject Name = " + rdTrapDict[item].name);
                }
#endif
            }

            public void RegAnimIntoPauseList(Animator anim, UnityAction<int> setAct)
            {
                if (anim)
                {
                    pauseAnimDict.Add(pauseAnimDictIndexer, anim);
                }
                else
                {
                    Debug.LogError("Anim is null & cannot reg into pauseAnim Dict!");
                }

                if (setAct != null)
                {
                    setAct(pauseAnimDictIndexer);
                }
                else
                {
                    Debug.LogError("setAct is null & cannot reg into pauseAnim Dict!");
                }

                pauseAnimDictIndexer++;
            }

            public void RemoveAnimFromPauseList(int _key)
            {
                pauseAnimDict.Remove(_key);
            }

            #endregion

            #region Revive Related
            void ReviveProcedure(int _fee)
            {
                if (!AppGM.instance.GetIfAdsFree())
                {
                    // Need to watch ads before Revive
                    playAdAct(_fee);
                }
                else
                {
                    ReviveChar();
                }
            }

            public void ReviveChar()
            {
                StartCoroutine(_ReviveChar());
            }

            public IEnumerator _ReviveChar()
            {
                gameState = IEnum.GameState.playing;
                charReviveAct();

                gameUI.ShowGameOverMenu(false);
                mainChar.GetLastBouncedBlock().Revive();
                mainChar.CharRevive();

                currentDashEnergy = maxDashEnergy;

                cameraController.InstantFocus(mainChar.GetPos());
                cameraController.AllowCamFollow(true);

                backgroundManager.ActivateBGParticle(true);

                //rdTrapDictIndexer = 0;
                //rdTrapDict.Clear();

                //powerUpList = new List<IEnum.PowerUp>();

                ResourceUI.instance.DisableOpenCoinShopBtn();
                ResourceUI.instance.ShowOnTop(false);

                PauseAllTrapsPowerupsAnim(false);
                //pauseAnimDictIndexer = 0;
                //pauseAnimDict.Clear();

                //poolManager.ReturnAllBullets();

                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.practice:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.traditional:
                        yield return new WaitForEndOfFrame();
                        break;
                    case IEnum.GameMode.challenge:
                        yield return new WaitForEndOfFrame();
                        challengeBlockManager.CharRevive();
                        break;
                    default:
                        yield return new WaitForEndOfFrame();
                        break;
                }

                allowInputAct(true);
            }

            void CharInvulnerable(bool invulnerable)
            {
                charInvulnerableAct(invulnerable);

                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        break;
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        challengeBlockManager.ActivateAllBlocksPlatformerEffect(invulnerable);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region Prepare Game Data
            public void PrepareGameData(ref GameData _data)
            {
                if (clearStageBonusDict != null)
                {
                    _data.clearStage = clearStageBonusDict;
                }

                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        break;
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        if (_data.highestLevel < challengeBlockManager.GetCurrentLevel().x)
                        {
                            _data.highestLevel = challengeBlockManager.GetCurrentLevel().x;
                            _data.highestSubLevel = challengeBlockManager.GetCurrentLevel().y;
                        }

                        // No need the below code as data saved level can be greater than the actual possible level, when the 'To Be Continued' is triggered
                        /*if (data.highestLevel > challengeBlockManager.GetMaxBlockLevel())
                        {
                            data.highestLevel = challengeBlockManager.GetMaxBlockLevel();
                            data.highestSubLevel = 0;
                        }*/
                        break;
                    default:
                        break;
                }
            }

            public void WriteLevelDict(Dictionary<string, bool> dict)
            {
                clearStageBonusDict = dict;
            }
            #endregion

            #region Effect related
            public void CreateStepOnStarExplodeEffect(Vector3 pos)
            {
                
                if (stepOnStarEffectsParent.childCount == 0)
                {
                    ParticleSystem ps = Instantiate(stepOnStarExplodePS, pos, _transform.rotation, stepOnStarEffectsParent).GetComponent<ParticleSystem>();
                }
                else
                {
                    GameObject readyObj = null;

                    for (int i = 0; i < stepOnStarEffectsParent.childCount; i++)
                    {
                        if (!stepOnStarEffectsParent.GetChild(i).gameObject.activeInHierarchy)
                        {
                            readyObj = stepOnStarEffectsParent.GetChild(i).gameObject;
                            break;
                        }
                    }

                    if (readyObj == null)
                    {
                        ParticleSystem ps = Instantiate(stepOnStarExplodePS, pos, _transform.rotation, stepOnStarEffectsParent).GetComponent<ParticleSystem>();
                    }
                    else
                    {
                        readyObj.transform.position = pos;
                        readyObj.SetActive(true);
                    }
                }
            }
            #endregion

            #region Game UI Tips
            public void BlinkBoostBar()
            {
                AppGM.instance.PlaySFX(IEnum.AudioList.buttonError);
                gameUI.BlinkBoostBar();
            }

            public void ShowBoostTips(bool _show)
            {
                gameUI.ShowBoostTips(_show);
            }
            #endregion

            #region Coin Generation by Moving
            void AllowMoveGenerateCoin()
            {
                allowMoveGenCoin = true;
                gameUI.ShowCoinBar(true);
            }

            public void UpdateCoinBar(bool _dash = false, float _speed = 0.1f)
            {
                if (allowMoveGenCoin && gameState == IEnum.GameState.playing)
                {
                    if (!_dash)
                    {
                        coinBarVal = coinBarVal + (coinBarSpeedMul * _speed * Time.fixedDeltaTime);
                    }
                    else
                    {
                        coinBarVal = coinBarVal + coinBarDashBonus;
                    }
                    

                    if (coinBarVal >= 1f)
                    {
                        GetCoinBarCoin();
                        coinBarVal = 0f;
                    }
                }
            }

            public void FadeCoinBar()
            {
                if (allowMoveGenCoin && gameState == IEnum.GameState.playing)
                {
                    coinBarVal = coinBarVal - (coinBarFadeSpeed * Time.fixedDeltaTime);
                }
            }

            void GetCoinBarCoin()
            {
                int amount = challengeBlockManager.GetCurrentLevel().x + 1;
                AppGM.instance.AddCoin(amount, false, 1);
            }

            public void PlayGetCoinAnim(UnityAction resourceUIAct, UnityAction soundAct, int numOfCoins)
            {
                gameUI.RewardCoinAnim(resourceUIAct, soundAct, numOfCoins);
            }

            #endregion

            #region Getter
            public bool GetAllowMoveBack()
            {
#if UNITY_EDITOR
                return allowMoveBack;
#else
                return false;
#endif
            }

            public IEnum.GameState GetGameState()
            {
                return gameState;
            }

            public bool GetMoveBtnPressed()
            {
                return moveBtnPressed;
            }

            public bool GetBoostBtnPressed()
            {
                return boostBtnPressed;
            }

            public CharController GetChar()
            {
                return mainChar;
            }

            public Transform GetTransform()
            {
                return _transform;
            }

            public PoolManager GetPoolManager()
            {
                return poolManager;
            }

            public Food GetFoodPrefab()
            {
                switch (charType)
                {
                    case IEnum.CharacterType.none:
                        Debug.LogError("Char Type is null! Cannot determine Food Prefab!");
                        return null;
                    case IEnum.CharacterType.rabbit:
                        return foodPrefabList[0];
                    default:
                        Debug.LogError("Cannot determine Food Prefab!");
                        return null;
                }
            }


            public float? GetCurrentLevelLowestBlockY()
            {
                float startBlockWidth = startBlock.GetCol().bounds.size.x;

                switch (gameMode)
                {
                    case IEnum.GameMode.none:
                        break;
                    case IEnum.GameMode.practice:
                        // Determine if need to consider Start Block Y
                        if (GetChar().GetPos().x > (startBlock.GetMinX() + startBlockWidth))
                        {
                            // No need to consider start Block Y
                            return practiceBlockManager.GetCurrentLevelBlockLowestY();
                        }
                        else
                        {
                            // Consider Start Block Y
                            return Mathf.Min(practiceBlockManager.GetCurrentLevelBlockLowestY(), GetStartBlockLowestY());
                        }
                    case IEnum.GameMode.traditional:
                        break;
                    case IEnum.GameMode.challenge:
                        // Determine if need to consider Start Block Y
                        if (GetChar().GetPos().x > (startBlock.GetMinX() + startBlockWidth))
                        {
                            // No need to consider start Block Y
                            return challengeBlockManager.GetCurrentLevelBlockLowestY();
                        }
                        else
                        {
                            // Consider Start Block Y
                            return Mathf.Min(challengeBlockManager.GetCurrentLevelBlockLowestY(), GetStartBlockLowestY());
                        }
                    default:
                        break;
                }

                return null;
            }

            [ContextMenu("LowestBlockY")]
            public void TestLowestBlockY()
            {
                Debug.Log("Lowest Y = " + GetCurrentLevelLowestBlockY());
            }

            public bool DetermineIfColInsideCamera(Collider2D col)
            {
                planes = GeometryUtility.CalculateFrustumPlanes(cam);

                if (GeometryUtility.TestPlanesAABB(planes, col.bounds))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public float GetStartBlockLowestY()
            {
                return startBlock.GetLowestY();
            }

            public float GetMinBoostEnergyRequire()
            {
                return minBoostEnergyRequire;
            }

            public float GetBoostEnergyCost()
            {
                return boostEnergyCost;
            }

            public bool DetermineIfCanDash()
            {
#if UNITY_EDITOR
                if ( ((!unlimitedDash && currentDashEnergy >= 1)|| unlimitedDash) && gameState == IEnum.GameState.playing)
#else
                if ( currentDashEnergy >= 1 && gameState == IEnum.GameState.playing)
#endif
                {
                    return true;
                }
                else
                {
                    AppGM.instance.PlaySFX(IEnum.AudioList.buttonError);
                    gameUI.BlinkFoodIcons();
                    return false;
                }
            }

            public bool IsBoostTipsShowing()
            {
                return gameUI.IsBoostTipsShowing();
            }

            public bool GetIsCamFollow()
            {
                return cameraController.GetIsCamFollow();
            }

            public bool CheckIfPowerUpActive(IEnum.PowerUp _power)
            {
                if (powerUpList == null)
                {
                    return false;
                }
                else
                {
                    return powerUpList.Contains(_power) ? true : false;
                }
            }
            #endregion

            #region Testing
            [ContextMenu("LogRDDict")]
            void LogRDDict()
            {
                if (rdTrapDict.Keys.Count == 0)
                {
                    Debug.Log("LogRDDict is empty!");
                }
                else
                {
                    foreach (int item in rdTrapDict.Keys)
                    {
                        Debug.Log("LogRDDict /n RD Trap Dict key = " + item + " , RD gameObject Name = " + rdTrapDict[item].name);
                    }
                }
            }
#endregion

        }
    }
}