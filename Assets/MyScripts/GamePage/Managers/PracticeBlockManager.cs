﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class PracticeBlockManager : MonoBehaviour
        {

            [SerializeField]
            BlockBase blockPrefab;

            [SerializeField]
            float spawnDistanceInterval;

            float lastSpawnPosX, nextSpawnPosX;
            float remainDistanceToSpawn;

            float lastSpawnPosY;

            Transform _transform;
            Transform creatorTransform;

            bool isActive;

            List<BlockBase> tempBlockList;
            float low;

            private void Awake()
            {
                if (spawnDistanceInterval <= 1f)
                {
                    spawnDistanceInterval = 1f;
                }

                _transform = gameObject.transform;
                isActive = false;

                InitVar();
            }

            private void Update()
            {
                if (isActive && GameGM.instance.GetGameState() == IEnum.GameState.playing)
                {
                    PracticeBlockUpdateMode();
                }
            }

            void InitVar()
            {
                tempBlockList = new List<BlockBase>();
                tempBlockList = new List<BlockBase>();
            }

            public void GameStart()
            {
                isActive = true;
                Debug.Log(this.name + " Activated");

                creatorTransform = GameGM.instance.GetChar().GetBlockCreatorTransform();
                ResetBlockManager(true);
            }

            public void GameRestart(bool needSpawnBlock)
            {
                InitVar();
                ResetBlockManager(needSpawnBlock);
            }

            void ResetBlockManager(bool spawnBlock = false)
            {
                if (spawnBlock)
                {
                    SpawnPracticeBlock(true);
                }
            }

            void PracticeBlockUpdateMode()
            {
                remainDistanceToSpawn = nextSpawnPosX - creatorTransform.position.x;
                if (remainDistanceToSpawn <= 0f && GameGM.instance.GetChar().GetPos().y > GetCurrentLevelBlockLowestY())
                {
                    SpawnPracticeBlock();
                    KillBehindBlock();
                }
            }

            void SpawnPracticeBlock(bool isStartBlock = false)
            {
                NextPracticeSpawnCal();

                if (isStartBlock)
                {
                    // as the creator is too far away from Start Block, spawn extra block before it
                    BlockBase extraBlock = Instantiate(blockPrefab, new Vector3(lastSpawnPosX - 7f, 0f, 0f), creatorTransform.rotation, _transform);
                    tempBlockList.Add(extraBlock);
                }

                float randomY = Random.Range(creatorTransform.position.y, creatorTransform.position.y - 1f);
                randomY = Mathf.Clamp(randomY, Mathf.NegativeInfinity, tempBlockList[tempBlockList.Count - 1].GetY() + 3.85f);

                BlockBase spawnedBlock = Instantiate(blockPrefab, new Vector3(lastSpawnPosX, randomY, 0f), creatorTransform.rotation, _transform);
                tempBlockList.Add(spawnedBlock);
            }

            void KillBehindBlock()
            {
                int counter = tempBlockList.Count;
                for (int i = 0; i < counter; i++)
                {
                    float diff = Mathf.Abs(tempBlockList[i].GetMinX() - GameGM.instance.GetChar().GetPos().x);
                    if (diff >= 15f)
                    {
                        BlockBase selectedBlock = tempBlockList[0];
                        selectedBlock.gameObject.SetActive(false);
                        tempBlockList.RemoveAt(0);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            void NextPracticeSpawnCal()
            {
                lastSpawnPosX = creatorTransform.position.x;
                nextSpawnPosX = lastSpawnPosX + spawnDistanceInterval;
            }

            public void ClearAllBlocks()
            {
                for (int i = _transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(_transform.GetChild(i).gameObject);
                }
            }

            public float GetCurrentLevelBlockLowestY()
            {
                float lowestY = tempBlockList[0].GetY();

                for (int i = 0; i < tempBlockList.Count; i++)
                {
                    if (tempBlockList[i].GetY() < lowestY)
                    {
                        lowestY = tempBlockList[i].GetY();
                    }
                }

                low = lowestY;
                return lowestY;
            }
        }
    }
}