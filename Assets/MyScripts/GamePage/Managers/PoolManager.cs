using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class PoolManager : MonoBehaviour
        {
            [SerializeField]
            StingBullet stingPrefab;
            [SerializeField]
            Transform stingParent;

            List<BulletBase> activeBulletList = new List<BulletBase>();

            List<StingBullet> stingTotalList = new List<StingBullet>();
            List<StingBullet> stingReadyList = new List<StingBullet>();
            List<StingBullet> stingUsingList = new List<StingBullet>();
            int bulletIndex;

            private void Start()
            {
                StartCoroutine(GenerateStingPool(1));
            }

            #region Sting Bullet Specific

            IEnumerator GenerateStingPool(int num)
            {
                if (!stingPrefab || !stingParent)
                {
                    Debug.LogError("Cannot Create Sting Pool! Please check the stingPrefab or stingParent existence!");
                }
                else
                {
                    for (int i = 0; i < num; i++)
                    {
                        if (i % 10 == 0)
                        {
                            // for buffering purpose
                            yield return null;
                        }

                        GenerateSting(bulletIndex);
                    }
                }
            }

            StingBullet GenerateSting(int _index)
            {
                StingBullet sting = Instantiate(stingPrefab, stingParent);
                sting.SetID(_index);
                stingTotalList.Add(sting);
                stingReadyList.Add(sting);
                bulletIndex++;
                return sting;
            }

            StingBullet GetStingBullet()
            {
                StingBullet sting;

                if (stingReadyList.Count > 0)
                {
                    sting = stingReadyList[0];
                }
                else
                {
                    // Pool is empty, need to create 1 for emergency
                    sting = GenerateSting(bulletIndex);
                }

                stingReadyList.RemoveAt(0);
                stingUsingList.Add(sting);
                activeBulletList.Add(sting);

                return sting;
            }

            /// <summary>
            /// Return the Sting bullet to the pool after expire
            /// </summary>
            /// <param name="_id">The id of the bullet</param>
            /// <param name="willDestroy">Determine if this is prepared for Destroy. If true, the bullet will not be added back to the readyList so it will never be re-used again</param>
            public void ReturnStingBullet(int _id, bool willDestroy = false)
            {
                bool found = false;

                RemoveFromActiveList(_id);
                
                for (int i = 0; i < stingUsingList.Count; i++)
                {
                    if (stingUsingList[i].GetID() == _id)
                    {
                        found = true;
                        StingBullet bullet = stingUsingList[i];
                        if (!willDestroy)
                        {
                            stingReadyList.Add(bullet);
                        }
                        else
                        {
                            // If the bullet will not be re-used again, remove it from totalList as well
                            for (int j = 0; j < stingTotalList.Count; j++)
                            {
                                if (stingTotalList[j].GetID() == _id)
                                {
                                    stingTotalList.RemoveAt(j);
                                    break;
                                }
                            }
                        }
                        stingUsingList.RemoveAt(i);
                        bullet.gameObject.SetActive(false);
                        break;
                    }
                }
                
                if (!found)
                {
                    Debug.LogError("Cannot locate id:" + _id + " in stingUsingList!");
                }
            }
            #endregion

            #region General
            void RemoveFromActiveList(int _index)
            {
                bool found = false;
                for (int i = 0; i < activeBulletList.Count; i++)
                {
                    if (stingUsingList[i].GetID() == _index)
                    {
                        found = true;
                        activeBulletList.RemoveAt(i);
                        break;
                    }
                }

                if (!found)
                {
                    Debug.LogError("Cannot locate id:" + _index + " in activeBulletList!");
                }
            }
            

            public void ReturnAllBullets()
            {
                BulletBase bullet;
                int bulletTotal = activeBulletList.Count;

                for (int i = 0; i < bulletTotal; i++)
                {
                    bullet = activeBulletList[0];
                    bullet.BulletExpire(false);
                }

            }

            public BulletBase GetBullet(IEnum.BulletType _type)
            {
                switch (_type)
                {
                    case IEnum.BulletType.sting:
                        return GetStingBullet();
                    case IEnum.BulletType.bomb:
                        return null;
                    default:
                        return null;
                }
            }
            #endregion
        }
    }
}
