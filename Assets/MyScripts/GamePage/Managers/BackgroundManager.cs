﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class BackgroundManager : MonoBehaviour
        {
            [SerializeField]
            SpriteRenderer bgTop, bgBot, bgMid;
            [SerializeField]
            ParticleSystem bgParticle;
            [SerializeField]
            List<Sprite> particleList;

            string[] top = new string[8] { "#cceef0", "#f0eccc" , "#fff6bd", "#c5f0cb", "#e1ffb7", "#e2cfff", "#ccdfff", "#ffffcc"};
            string[] mid = new string[8] { "#bfdee0", "#e0ddbf" , "#f7e6a1", "#afe0b4", "#d1f2a7", "#d0bff2", "#b4cef7", "#f6f7b4"};
            string[] bot = new string[8] { "#abcdcf", "#cfcbab" , "#f0d890", "#97cf9c", "#bfe898", "#bda9e8", "#a6bef0", "#eded9d"};

            Color32[] bgColor = new Color32[3];
            int curBGColorIndex = -1;

            public void BGSetup()
            {
                bgColor = GetRandomBGColor();
                SetRandomParticle();

                bgTop.color = bgColor[0];
                bgMid.color = bgColor[1];
                bgBot.color = bgColor[2];
            }

            void LerpBGColor()
            {
                Color32[] newBGColor = new Color32[3];

                newBGColor = GetRandomBGColor(curBGColorIndex);

                StartCoroutine(_LerpColor(newBGColor, 1f));
            }

            IEnumerator _LerpColor(Color32[] newColor, float lerpTime)
            {
                for (float t = 0.01f; t < lerpTime; t+=0.1f)
                {
                    bgTop.color = Color.Lerp(bgColor[0], newColor[0], t / lerpTime);
                    bgMid.color = Color.Lerp(bgColor[1], newColor[1], t / lerpTime);
                    bgBot.color = Color.Lerp(bgColor[1], newColor[2], t / lerpTime);
                    yield return new WaitForSeconds(0.1f);
                }

                bgColor = newColor;
            }

            public void StageLevelUp()
            {
                LerpBGColor();
                SetRandomParticle();
            }

            public void ActivateBGParticle(bool flag)
            {
                if (flag)
                {
                    var main = bgParticle.main;

                    main.prewarm = false;
                    StartCoroutine(DelayActivateBGParticle());
                }
                else
                {
                    bgParticle.Stop();
                    bgParticle.Clear();
                }
            }

            IEnumerator DelayActivateBGParticle()
            {
                var main = bgParticle.main;
                main.prewarm = true;

                yield return null;
                bgParticle.Play();
                yield return null;
                bgParticle.Simulate(main.startLifetime.constant, true, true, false);
                yield return null;
                bgParticle.Play();

#if _DEBUG
        Debug.Log("Delay Activate Particle");
#endif
            }

            public void PauseBGParticle(bool pause)
            {
                var main = bgParticle.main;

                if (pause)
                {
                    main.simulationSpeed = 0f;
                }
                else
                {
                    main.simulationSpeed = 1f;
                }
            }

            Color32[] GetRandomBGColor(int excludeIndex = -1)
            {
                int randomNum = -1;
                randomNum = Random.Range(0, top.Length);
                if (excludeIndex >= 0)
                {
                    int counter = 0;

                    while (randomNum == excludeIndex)
                    {
                        if (counter < 50)
                        {
                            randomNum = Random.Range(0, top.Length);
                            counter++;
                        }
                        else
                        {
                            Debug.LogError("The looping time of BG Color is at max!");
                            break;
                        }
                    }
                }

                Color32[] colorSet = new Color32[3];
                Color tempColor;

                ColorUtility.TryParseHtmlString(top[randomNum], out tempColor);
                colorSet[0] = tempColor;
                ColorUtility.TryParseHtmlString(mid[randomNum], out tempColor);
                colorSet[1] = tempColor;
                ColorUtility.TryParseHtmlString(bot[randomNum], out tempColor);
                colorSet[2] = tempColor;

                curBGColorIndex = randomNum;

                return colorSet;
            }

            void SetRandomParticle()
            {
                int randomNum = Random.Range(0, particleList.Count);
                int counter = 0;

                while (counter < 100)
                {
                    if (particleList[randomNum] != null)
                    {
                        bgParticle.textureSheetAnimation.SetSprite(0, particleList[randomNum]);
                        break;
                    }
                    else
                    {
                        counter++;
                        if (counter >= 100)
                        {
                            Debug.LogError("No Available Sprite can be found!");
                            bgParticle.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }
    }
}