﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class ChallengeBlockManager : MonoBehaviour
        {
            public static ChallengeBlockManager instance;
            Transform _transform;

            BlockCreator blockCreator;

            bool isActive;

            // This checks if the char Y is already lower than the lowest block Y. Originally designed for checking 'should the passLevelAct be triggered'
            // If this is true, does not count pass level, and no more higher level blocks created even the player keep dashing under the lowest Block
            bool charIsUnderLowestBlockOfNextLevel;

            int currentBlockLevel, currentBlockSubLevel;    // currentBlockLevel is 0 when it is Level 1 as displayed in UI, so 0,0 combination means 1-1 in UI
            
            int maxBlockLevel;
           
            Vector2 startPos = new Vector2(20f, 0f);

            [SerializeField]
            List<BlockLevel> allStages;
            public List<BlockLevel> createdStages;
            int currentCreatedLevelIndex, currentCreatedSubLevelIndex;          // this is the index for getting the current level from 'createdStages' List

            public UnityAction incomingLevelAlertAct;
            public UnityAction<int> enterLastSubLevelAct, passLevelAct;

            bool toBeContinueFlag = false;

            [Header("Test Function")]
            [SerializeField]
            bool customStartLevelFlag;
            [SerializeField]
            Vector2Int customStartLevel;

            private void Awake()
            {
                if (instance == null)
                {
                    instance = this;
                }
                else if (instance != this)
                {
                    Destroy(gameObject);
                }
                DontDestroyOnLoad(gameObject);

                _transform = gameObject.transform;
                isActive = false;

                currentCreatedLevelIndex = -1;
                currentCreatedSubLevelIndex = -1;

                SetDefaultHighestPossibleLevel();
            }

            #region During Game
            public void GameStart()
            {
                isActive = true;
                Debug.Log(this.name + " Activated");

                ClearAllCreatedStages();

                charIsUnderLowestBlockOfNextLevel = false;
                toBeContinueFlag = false;

                blockCreator = GameGM.instance.GetChar().GetBlockCreator();
                blockCreator.createAction = BlockLevelGeneration;

                GameGM.instance.GetChar().charPassLevelAct = BlockLevelUp;

                GenerateBlockLevelAndUpdateUI(true);
            }

            void BlockLevelGeneration(Vector2 pos, EndPoint endPoint)
            {
                if (GameGM.instance.GetGameState() == IEnum.GameState.playing && isActive && !charIsUnderLowestBlockOfNextLevel)
                {
                    if (GameGM.instance.GetChar().IsFalling())
                    {
                        Debug.Log("Char is falling! Will not generate Block Level!");
                        return;
                    }

                    LevelStage levelStage;
                    GameObject spawnedLevelStageObj;

                    if (endPoint != null)
                    {
                        if (!endPoint.GetIsBlockCreated() && endPoint.gameObject.activeInHierarchy)
                        {
                            endPoint.SetBlockCreated(true);

                            int subLevelLimit = currentBlockSubLevel+1;
                            int levelLimit = 0;

                            if (DetermineIfSubLevelIsMaxed(subLevelLimit))
                            {
                                levelLimit = Mathf.Max(0, currentBlockLevel + 1);
                                subLevelLimit = 0;
                            }
                            else
                            {
                                levelLimit = Mathf.Clamp(currentBlockLevel, 0, maxBlockLevel);
                            }

                            if (levelLimit <= maxBlockLevel)
                            {
                                levelStage = allStages[levelLimit].level[subLevelLimit].subLevel;
                            }
                            else
                            {
                                levelStage = null;
                            }

                            if (levelStage != null)
                            {
                                spawnedLevelStageObj = Instantiate(levelStage.gameObject, pos, Quaternion.Euler(Vector3.zero), _transform);
#if _DEBUG_Level
                                Debug.Log("Gen Next Level. Next Level is " + levelLimit + "-" + subLevelLimit);
#endif

                                if (DetermineIfSubLevelIsLast(subLevelLimit))
                                {
                                    incomingLevelAlertAct();
                                }
                            }
                            else
                            {
                                spawnedLevelStageObj = null;
                            }

                            if (spawnedLevelStageObj != null)
                            {
                                SetupSpawnedLevelStage(spawnedLevelStageObj.GetComponent<LevelStage>(), levelLimit, subLevelLimit);
                            }
                            else
                            {
                                Debug.Log("Reaches MAX Level! No Blocks spawned");
                                toBeContinueFlag = true;
                            }
                        }
                    }
                    else
                    {
                        currentCreatedLevelIndex = 0;
                        currentCreatedSubLevelIndex = 0;

                        if (customStartLevelFlag)
                        {
                            int subLevelLimit = customStartLevel.y;
                            int levelLimit = Mathf.Clamp(customStartLevel.x, 0, maxBlockLevel);

                            levelStage = allStages[levelLimit].level[subLevelLimit].subLevel;
                            if (levelStage != null)
                            {
                                spawnedLevelStageObj = Instantiate(levelStage.gameObject, pos, Quaternion.Euler(Vector3.zero), _transform);
                                Debug.Log("Gen Custom Level. Custom Level is " + levelLimit + "-" + subLevelLimit);

                                // Update the current Block level Var so the game can gen future Levels correctly
                                currentBlockLevel = levelLimit;
                                currentBlockSubLevel = subLevelLimit;
                                //----------------------------------------------
                            }
                            else
                            {
                                Debug.LogError("The block is NULL! Generate End Block");

                                levelStage = allStages[currentBlockLevel].level[currentBlockSubLevel].subLevel;
                                spawnedLevelStageObj = Instantiate(levelStage.gameObject, pos, Quaternion.Euler(Vector3.zero), _transform);
                                
                            }

                            SetupSpawnedLevelStage(spawnedLevelStageObj.GetComponent<LevelStage>(), levelLimit, subLevelLimit);
                            customStartLevelFlag = false;
                        }
                        else
                        {
                            Debug.Log("Gen Default Level");
                            levelStage = allStages[0].level[0].subLevel;
                            spawnedLevelStageObj = Instantiate(levelStage.gameObject, pos, Quaternion.Euler(Vector3.zero), _transform);

                            SetupSpawnedLevelStage(spawnedLevelStageObj.GetComponent<LevelStage>(), 0, 0);
                        }
                    }
                }
            }


            void BlockLevelUp(EndPoint endPoint)
            {
                if (GameGM.instance.GetGameState() == IEnum.GameState.playing && isActive)
                {
                    if (!endPoint.GetIsCharPassed() && !charIsUnderLowestBlockOfNextLevel)
                    {
                        if (GameGM.instance.GetChar().IsFalling())
                        {
                            Debug.Log("Char is falling! Will not classify as Block Level Up!");
                            return;
                        }

                        if (!toBeContinueFlag)
                        {
                            int a = createdStages.Count - 1;
                            int b = createdStages[createdStages.Count - 1].level.Count - 1;

                            //Debug.Log("Pos Y = " + GameGM.instance.GetChar().GetPos().y + "  , Lowest Block Y in next Level = " + createdStages[a].level[b].subLevel.GetLowestBlockY());

                            // Check if the Char Pos Y is lower than the lowest block Y in the newest LevelStage
                            if (GameGM.instance.GetChar().GetPos().y >= createdStages[a].level[b].subLevel.GetLowestBlockY())
                            {
                                endPoint.SetCharPassed(true);

                                // handling of 'createdStages' relaed var
                                currentCreatedLevelIndex = createdStages.Count - 1;
                                currentCreatedSubLevelIndex = createdStages[currentCreatedLevelIndex].level.Count - 1;
                                //--------------------------------------------

                                currentBlockSubLevel++;
                                if (DetermineIfSubLevelIsMaxed(currentBlockSubLevel))
                                {
                                    currentBlockLevel++;
                                    currentBlockSubLevel = 0;

                                    passLevelAct(currentBlockLevel - 1);
                                }
                                else if (DetermineIfSubLevelIsLast(currentBlockSubLevel))
                                {
                                    enterLastSubLevelAct(currentBlockLevel);
                                }
                                else
                                {
                                    AppGM.instance.PlaySFX(IEnum.AudioList.passSubLevel);
                                }

                                GameGM.instance.gameUI.BlockLevelUpdate(currentBlockLevel, currentBlockSubLevel);
                            }
                            else
                            {
                                charIsUnderLowestBlockOfNextLevel = true;
                                Debug.Log("Char Pos Y is lower than lowest Y block in next level, so the pass Level action will not be triggered");
                            }
                        }
                        else
                        {
                            int a = createdStages.Count - 1;
                            int b = createdStages[createdStages.Count - 1].level.Count - 1;

                            // Check if the Char Pos Y is lower than the lowest block Y in the latest LevelStage
                            if (GameGM.instance.GetChar().GetPos().y >= createdStages[a].level[b].subLevel.GetLowestBlockY())
                            {
                                currentBlockLevel++;
                                currentBlockSubLevel = 0;

                                passLevelAct(currentBlockLevel - 1);

                                // Trigger 'To Be Continued' action
                                GameGM.instance.GameEnd();
                            }
                            else
                            {
                                charIsUnderLowestBlockOfNextLevel = true;
                                Debug.Log("Char Pos Y is lower than lowest Y block in next level, so the pass Level action will not be triggered");
                            }
                        }
                    }
                }
            }

            bool DetermineIfSubLevelIsMaxed(int subLevel)
            {
                if (subLevel > allStages[currentBlockLevel].level.Count - 1)
                {
                    //Debug.Log("Sub Level is Maxed!");
                    return true;
                }
                else
                {
                    //Debug.Log("Sub Level is NOT Maxed!");
                    return false;
                }
            }

            bool DetermineIfSubLevelIsLast(int subLevel)
            {
                if (subLevel == allStages[currentBlockLevel].level.Count - 1)
                {
                    //Debug.Log("Sub Level is Last SubLevel!");
                    return true;
                }
                else
                {
                    //Debug.Log("Sub Level is NOT Last SubLevel!");
                    return false;
                }
            }

            void SetupSpawnedLevelStage(LevelStage _stage, int _lv, int _subLv)
            {
                _stage.Init(_lv, _subLv);
                AddCreatedLevelStageToList(_stage, _lv, _subLv);

                // Apply PowerUp
                if (GameGM.instance.CheckIfPowerUpActive(IEnum.PowerUp.extraHop))
                {
                    _stage.IncreaseAllBlockHP(1);
                }
            }

            void AddCreatedLevelStageToList(LevelStage stage, int _level, int _subLevel)
            {
                if (_subLevel == 0)
                {
                    // The case of entering a new level

                    BlockSubLevel bsl = new BlockSubLevel();
                    bsl.subLevel = stage;

                    BlockLevel bl = new BlockLevel();
                    bl.level = new List<BlockSubLevel>();
                    bl.level.Add(bsl);

                    createdStages.Add(bl);
                }
                else
                {
                    BlockSubLevel bsl = new BlockSubLevel();
                    bsl.subLevel = stage;

                    createdStages[createdStages.Count - 1].level.Add(bsl);
                }

                if (createdStages.Count >= 3)
                {
                    RemoveOldestBlockFromCreatedList();
                }
            }

            void RemoveOldestBlockFromCreatedList()
            {
                int count = createdStages[0].level.Count;
                for (int i = 0; i < count; i++)
                {
                    LevelStage picked = createdStages[0].level[0].subLevel;
                    createdStages[0].level.RemoveAt(0);

                    picked.gameObject.SetActive(false);
                    Destroy(picked);

                    if (i == count - 1)
                    {
                        createdStages.RemoveAt(0);
                    }
                }

                currentCreatedLevelIndex = 0;
            }

            void RemoveAllBlocksFromCreatedList()
            {
                for (int i = createdStages.Count - 1; i >= 0 ; i--)
                {
                    int count = createdStages[i].level.Count;
                    for (int j = count - 1; j >= 0; j--)
                    {
                        LevelStage picked = createdStages[i].level[j].subLevel;
                        createdStages[i].level.RemoveAt(j);

                        picked.gameObject.SetActive(false);
                        Destroy(picked);

                        if (j == 0)
                        {
                            createdStages.RemoveAt(i);
                        }
                    }
                }

                currentCreatedLevelIndex = -1;
                currentCreatedSubLevelIndex = -1;
            }
#endregion

#region After Game
            void ClearAllCreatedStages()
            {
                foreach (Transform child in _transform)
                {
                    Destroy(child.gameObject);
                }
                createdStages.Clear();
                /*for (int i = _transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(_transform.GetChild(i).gameObject);
                }
                createdStages = new List<BlockLevel>();*/
            }

            public void GameRestart(bool needSpawnBlock)
            {
                charIsUnderLowestBlockOfNextLevel = false;
                
                currentBlockSubLevel = 0;
                ClearAllCreatedStages();

                GenerateBlockLevelAndUpdateUI(needSpawnBlock);
            }

            public void CharRevive()
            {
                charIsUnderLowestBlockOfNextLevel = false;
                GenerateBlockLevelAndUpdateUI();
            }

            public void ActivateAllBlocksPlatformerEffect(bool flag)
            {
                for (int i = 0; i < createdStages.Count; i++)
                {
                    for (int j = 0; j < createdStages[i].level.Count; j++)
                    {
                        createdStages[i].level[j].subLevel.ActivateAllBlocksPlatformerEffect(flag);
                    }
                }
            }

            void GenerateBlockLevelAndUpdateUI(bool spawnBlock = false)
            {
                if (spawnBlock)
                {
                    BlockLevelGeneration(startPos, null);
                }

                GameGM.instance.gameUI.BlockLevelUpdate(currentBlockLevel, currentBlockSubLevel);
            }

            public void SetCustomStartLevel(bool flag, int level)
            {
                customStartLevelFlag = flag;
                customStartLevel = new Vector2Int(level, 0);
            }

            public void SkipStage(int num)
            {
                currentBlockLevel += num;
            }

            public void ResetChallengeBlockManager()
            {
                ClearAllCreatedStages();

                charIsUnderLowestBlockOfNextLevel = false;
                toBeContinueFlag = false;

                currentBlockLevel = -1;
                currentBlockSubLevel = -1;
                currentCreatedLevelIndex = -1;
                currentCreatedSubLevelIndex = -1;

                isActive = false;
            }
            #endregion

            #region Getter
            /// <summary>
            /// This function will get the current Level Info. X equals to Level & Y equals to Sub-level. 0-0 equals to 1-1 in UI.
            /// </summary>
            /// <returns>Returns the current Level as X & current Sub-level as Y.</returns>
            public Vector2Int GetCurrentLevel()
            {
                return new Vector2Int(currentBlockLevel, currentBlockSubLevel);
            }

            /// <summary>
            /// This is the actual highest level that the blocks prefab could have
            /// </summary>
            /// <returns></returns>
            public int GetMaxBlockLevel()
            {
                return maxBlockLevel;
            }

            public float GetCurrentLevelBlockLowestY()
            {
                return createdStages[currentCreatedLevelIndex].level[currentCreatedSubLevelIndex].subLevel.GetLowestBlockY();
            }
            #endregion

            #region Setter
            private void SetDefaultHighestPossibleLevel()
            {
                for (int i = 0; i < allStages.Count; i++)
                {
                    if (allStages[i].level.Count == 0)
                    {
                        maxBlockLevel = i - 1;
                        break;
                    }
                }
            }
#endregion
        }
    }
}