﻿using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class CameraController : MonoBehaviour
        {

            Transform _transform;
            CharController target;
            Vector3 velocity = Vector3.zero;
            [SerializeField]
            float smoothSpeed = 10f;
            [SerializeField]
            Vector3 offset = new Vector3(-1.5f, 0f, -10f);
            [SerializeField]
            [Range(0f, 1f)]
            float upperBoundaryY = 0.75f, lowerBoundaryY = 0.45f;

            bool allowCamFollow = false;

            float prevHighestViewportY, prevLowestViewportY;

            enum CameraMovingState
            {
                up,
                none,
                down,
            }

            public void Init()
            {
                _transform = transform;
                target = GameGM.instance.GetChar();


                allowCamFollow = true;
            }

            public void AllowCamFollow(bool flag)
            {
                allowCamFollow = flag;
            }

            public void InstantFocus(Vector3 pos)
            {
                _transform.position = pos + offset;
            }

            private void FixedUpdate()
            {
                if (allowCamFollow)
                {
                    Vector3 charPos = target.GetPos();
                    float viewportY = Camera.main.WorldToViewportPoint(charPos).y;

                    Vector3 newPos;

                    CameraMovingState movingState;


                    if (viewportY > upperBoundaryY)
                    {
                        movingState = CameraMovingState.up;

                        float worldViewportY = Camera.main.ViewportToWorldPoint(new Vector3(0f, viewportY, 0f)).y;
                        float worldLowerBoundY = Camera.main.ViewportToWorldPoint(new Vector3(0f, upperBoundaryY, 0f)).y;

                        if (worldViewportY > prevHighestViewportY)
                        {
                            prevHighestViewportY = worldViewportY;
                        }
                        float worldYDiff = prevHighestViewportY - worldLowerBoundY;

                        newPos = new Vector3(charPos.x, _transform.position.y + worldYDiff, charPos.z);
                    }
                    else if (viewportY < lowerBoundaryY)
                    {
                        movingState = CameraMovingState.down;

                        float worldViewportY = Camera.main.ViewportToWorldPoint(new Vector3(0f, viewportY, 0f)).y;
                        float worldLowerBoundY = Camera.main.ViewportToWorldPoint(new Vector3(0f, lowerBoundaryY, 0f)).y;
                        float worldPrevLowest = Camera.main.ViewportToWorldPoint(new Vector3(0f, prevLowestViewportY, 0f)).y;
                        if (worldViewportY < worldPrevLowest)
                        {
                            prevLowestViewportY = viewportY;
                        }
                        float worldYDiff = Camera.main.ViewportToWorldPoint(new Vector3(0f, prevLowestViewportY, 0f)).y - worldLowerBoundY;

                        newPos = new Vector3(charPos.x, _transform.position.y + worldYDiff, charPos.z);

                        //Debug.Log("a=" + a + ", b=" + b + ", worldPrevLow = " + worldPrevLowest + ", prevLow=" + prevLowestViewportY + ", worldDiff=" + worldYDiff + ", newPos=" + newPos);
                    }
                    else
                    {
                        movingState = CameraMovingState.none;

                        prevHighestViewportY = viewportY;
                        prevLowestViewportY = viewportY;
                        newPos = new Vector3(charPos.x, _transform.position.y, charPos.z);
                    }


                    Vector3 desiredPos = newPos + offset;
                    Vector3 smoothedPos;
                    if (movingState != CameraMovingState.down)
                    {
                        smoothedPos = Vector3.SmoothDamp(_transform.position, desiredPos, ref velocity, smoothSpeed * Time.fixedDeltaTime);

                        _transform.position = smoothedPos;
                    }
                    else
                    {
                        float xPos = Mathf.SmoothDamp(_transform.position.x, desiredPos.x, ref velocity.x, smoothSpeed * Time.fixedDeltaTime);
                        float zPos = Mathf.SmoothDamp(_transform.position.z, desiredPos.z, ref velocity.z, smoothSpeed * Time.fixedDeltaTime);

                        _transform.position = new Vector3(xPos, desiredPos.y, zPos);
                    }
                }
            }

            public bool GetIsCamFollow()
            {
                return allowCamFollow;
            }
        }
    }
}