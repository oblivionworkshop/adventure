﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        [System.Serializable]
        public class LevelStage : MonoBehaviour
        {
            [HideInInspector]
            public Transform _transform;

            [SerializeField]
            Transform blocksGroup;
            [SerializeField]
            Transform trapsGroup;

            [Header("Food related")]
            [SerializeField]
            Transform foodGroup;
            [SerializeField]
            int foodNum = 0;
            List<Transform> foodSpawnPoints;

            List<BlockBase> blocksList;

            float lowestBlockY = -999f;

            int level = -1, subLevel = -1;

            public void Init(int lv, int subLv)
            {
                _transform = transform;

                blocksList = new List<BlockBase>();
                foreach (BlockBase item in blocksGroup.GetComponentsInChildren<BlockBase>())
                {
                    blocksList.Add(item);
                }

                SetLevelInfo(lv, subLv);
                SetFoodLocation(foodNum);
                lowestBlockY = DetermineLowestBlockY();
            }

            public void ActivateAllBlocksPlatformerEffect(bool activate)
            {
                int counter = blocksList.Count;
                for (int i = 0; i < counter; i++)
                {
                    blocksList[i].ActivateColliderEffector(activate);
                }
            }

            void SetLevelInfo(int _level, int _subLevel)
            {
                level = _level;
                subLevel = _subLevel;

                SetAllChildBlockLevelInfo(_level, _subLevel);
            }

            void SetFoodLocation(int num)
            {
                if (foodGroup)
                {
                    foodSpawnPoints = new List<Transform>();
                    for (int i = 0; i < foodGroup.childCount; i++)
                    {
                        foodSpawnPoints.Add(foodGroup.GetChild(i));
                    }

                    List<Transform> locationList = new List<Transform>(foodSpawnPoints);
                    for (int i = 0; i < num; i++)
                    {
                        if (locationList.Count > 0)
                        {
                            int ranNum = Random.Range(0, locationList.Count);
                            Food food = Instantiate(GameGM.instance.GetFoodPrefab(), locationList[ranNum].position, Quaternion.Euler(Vector3.zero), foodGroup);
                            food.Init();
                            locationList.RemoveAt(ranNum);
                        }
                        else
                        {
                            Debug.LogWarning("locationList is empty already!");
                            break;
                        }
                    }
                }
            }

            void SetAllChildBlockLevelInfo(int lv, int subLv)
            {
                foreach (BlockBase item in blocksList)
                {
                    item.SetLevelInfo(lv, subLv);
                }
            }

            float DetermineLowestBlockY()
            {
                int counter = blocksList.Count;
                float lowest = 0f;

                for (int i = 0; i < counter; i++)
                {
                    float blockY = blocksList[i].GetLowestY();
                    if (blockY < lowest)
                    {
                        lowest = blockY;
                    }
                }

                return lowest;
            }

            public void IncreaseAllBlockHP(int _val)
            {
                foreach(BlockBase item in blocksList)
                {
                    if (item.FadeAfterHit && item.GetComponent<BlockOnce>() == null)
                    {
                        item.IncreaseHP(_val);
                    }
                }
            }

            public float GetLowestBlockY()
            {
                return lowestBlockY - 2f;   // Add -2 as a buffer
            }

            public Transform GetTrapsGroup()
            {
                return trapsGroup;
            }
        }
    }
}