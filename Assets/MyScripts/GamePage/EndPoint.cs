﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class EndPoint : MonoBehaviour
        {

            bool isBlockCreated;
            bool isCharPassed;

            public bool GetIsBlockCreated()
            {
                return isBlockCreated;
            }

            public void SetBlockCreated(bool flag)
            {
                isBlockCreated = flag;
            }

            public bool GetIsCharPassed()
            {
                return isCharPassed;
            }

            public void SetCharPassed(bool flag)
            {
                isCharPassed = flag;
            }
        }
    }
}