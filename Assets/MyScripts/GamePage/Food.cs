using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class Food : MonoBehaviour
        {
            [SerializeField]
            Animator anim;

            [HideInInspector]
            public Transform _transform;
            [SerializeField]
            Collider2D col;
            int animID;

            UnityAction<int> regAnimIndexAct;

            private void Awake()
            {
                _transform = transform;
                regAnimIndexAct = SetAnimID;
                gameObject.layer = LayerMask.NameToLayer("PowerUp");
            }

            public void Init()
            {
                GameGM.instance.RegAnimIntoPauseList(anim, regAnimIndexAct);
            }

            public void Eatten()
            {
                col.enabled = false;
                anim.SetTrigger("eat");
                StartCoroutine("_Eatten");
            }

            IEnumerator _Eatten()
            {
                yield return new WaitForSeconds(7f / 24f);
                gameObject.SetActive(false);
            }

            void SetAnimID(int _id)
            {
                animID = _id;
            }
        }
    }
}
