﻿using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class MovingBlock : BlockBase, IHitable
        {
            [Header("Moving Block")]
            [SerializeField]
            Transform targetTransform;
            [SerializeField]
            float speed;
            [SerializeField]
            bool willMoveBack;

            [Space(10f)]
            [SerializeField]
            SpriteRenderer directionPicLeft;
            [SerializeField]
            SpriteRenderer directionPicRight;
            float horizontalShift = 0.44f;

            [Space(10f)]
            [SerializeField]
            bool sleeping;

            Vector3 startPos, endPos;
            bool isActive, movingBackward;

            float lowestY;

            protected override void Awake()
            {
                base.Awake();
                lowestY = CalculateLowestY();
            }

            private new void Start()
            {
                base.Start();
                SetDirectionPos();
                FillIndicatorColor();

                startPos = _transform.position;
                endPos = targetTransform.position;
                if (!sleeping)
                {
                    isActive = true;
                }
                else
                {
                    isActive = false;
                }
                movingBackward = false;
            }

            void SetDirectionPos()
            {
                directionPicRight.transform.position = new Vector2(col.bounds.max.x - horizontalShift, directionPicRight.transform.position.y);
            }

            private void FillIndicatorColor()
            {
                if (fadeAfterHit)
                {
                    directionPicLeft.color = directionPicRight.color = GameGM.instance.roughBlockColor;
                }
                else
                {
                    directionPicLeft.color = directionPicRight.color = GameGM.instance.fadeBlockColor;
                }
            }

            private void FixedUpdate()
            {
                if (isActive && GameGM.instance.GetGameState()==IEnum.GameState.playing)
                {
                    if (!movingBackward)
                    {
                        if (Vector2.Distance((Vector2)_transform.position, (Vector2)endPos) <= 0.1f)
                        {
                            if (willMoveBack)
                            {
                                movingBackward = !movingBackward;
                            }
                            else
                            {
                                isActive = false;
                            }
                        }
                        else
                        {
                            //_transform.position = Vector2.MoveTowards(_transform.position, endPos, 0.3f);
                            _transform.position = Vector2.MoveTowards(_transform.position, endPos, speed * Time.fixedDeltaTime);
                        }
                    }
                    else
                    {
                        if (Vector2.Distance((Vector2)_transform.position, (Vector2)startPos) <= 0.1f)
                        {
                            if (willMoveBack)
                            {
                                movingBackward = !movingBackward;
                            }
                            else
                            {
                                isActive = false;
                            }
                        }
                        else
                        {
                            _transform.position = Vector2.MoveTowards(_transform.position, startPos, speed * Time.fixedDeltaTime);
                        }
                    }
                }
            }

            public new void HitEffect()
            {
                base.HitEffect();

                if (fadeAfterHit)
                {
                    if (currentHitPoint >= 0)
                    {
                        Color32 _color = directionPicLeft.color;
                        _color.a = (byte)(255f * (float)currentHitPoint / (float)startingHitPoint);
                        directionPicLeft.color = _color;

                        _color = directionPicRight.color;
                        _color.a = (byte)(255f * (float)currentHitPoint / (float)startingHitPoint);
                        directionPicRight.color = _color;
                    }

                    if (currentHitPoint <= 0)
                    {
                        //directionPicLeft.enabled = false;
                        //directionPicRight.enabled = false;

                        // Detach the child traps that are holding over this block
                        TrapTopParent[] trapTops = gameObject.GetComponentsInChildren<TrapTopParent>();
                        LevelStage lvStage = gameObject.GetComponentInParent<LevelStage>();
                        Transform trapParent = lvStage.GetTrapsGroup();

                        for (int i = 0; i < trapTops.Length; i++)
                        {
                            if (trapParent != null)
                            {
                                trapTops[i].transform.SetParent(trapParent, true);
                            }
                            else
                            {
                                trapTops[i].transform.SetParent(lvStage._transform, true);
                            }
                        }
                        //---------------------------
                    }
                }

                if (sleeping)
                {
                    isActive = true;
                    sleeping = false;
                }
            }
            public override void Revive()
            {
                if (col.isTrigger)
                {
                    col.isTrigger = false;
                }

                if (fadeAfterHit)
                {
                    alive = true;
                    currentHitPoint = startingHitPoint + 1;

                    spRendererOutter.enabled = true;
                    spRendererInner.enabled = true;
                    col.enabled = true;

                    Color32 _color = spRendererOutter.color;
                    _color.a = (byte)(255f);
                    spRendererOutter.color = _color;

                    _color = spRendererInner.color;
                    _color.a = (byte)(255f);
                    spRendererInner.color = _color;

                    _color = directionPicLeft.color;
                    _color.a = (byte)(255f);
                    directionPicLeft.color = _color;

                    _color = directionPicRight.color;
                    _color.a = (byte)(255f);
                    directionPicRight.color = _color;

                    gameObject.SetActive(true);
                }
            }

            float CalculateLowestY()
            {
                if (targetTransform.position.y < _transform.position.y)
                {
                    return targetTransform.position.y;
                }
                else
                {
                    return _transform.position.y;
                }
            }

            public override float GetLowestY()
            {
                return lowestY;
            }
        }
    }
}