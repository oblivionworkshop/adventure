using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class BlockOnce : BlockBase, IHitable
        {
            [Header("Block Once")]
            [SerializeField]
            protected List<SpriteRenderer> spRendererOutterList;
            [SerializeField]
            protected List<SpriteRenderer> spRendererInnerList;
            [SerializeField]
            List<Transform> blockFragmentList;
            [SerializeField]
            List<Rigidbody2D> rdList;
            List<Vector3> fragmentPosList;

            float diedPosY;
            float deathYRange = 60f;
            bool died = false;
            
            Vector3 deathRotateVector = new Vector3(0f, 0f, 150f);

            protected override void Start()
            {
                currentHitPoint = startingHitPoint;
                fragmentPosList = new List<Vector3>();
                int counter = blockFragmentList.Count;
                for (int i = 0; i < counter; i++)
                {
                    fragmentPosList.Add(blockFragmentList[i].position);
                }
            }

            protected void Update()
            {
                if (died)
                {
                    for (int i = 0; i < blockFragmentList.Count; i++)
                    {
                        blockFragmentList[i].Rotate(deathRotateVector * Time.deltaTime);
                    }

                    CheckShouldBlockDestroy();
                }
            }

            public override void HitEffect()
            {
                if (currentHitPoint >= 0 && !died)
                {
                    currentHitPoint--;
                    AppGM.instance.PlayStoppableSFX(IEnum.AudioList.blockOnceBreak, 2f, 0f, Random.Range(0.75f, 1.25f));

                    if (currentHitPoint <= 0)
                    {
                        diedPosY = blockFragmentList[0].position.y;
                        died = true;
                        col.enabled = false;
                        Fall();

                        if (fadeAfterHit)
                        {
                            spRendererOutter.enabled = false;
                            spRendererInner.enabled = false;
                        }
                    }
                }
            }

            void Fall()
            {
                for (int i = 0; i < rdList.Count; i++)
                {
                    rdList[i].bodyType = RigidbodyType2D.Dynamic;
                    rdList[i].gravityScale = Random.Range(0.8f, 2f);
                }
            }

            [ContextMenu("Revive")]
            public override void Revive()
            {
                alive = true;
                died = false;
                diedPosY = default(float);
                currentHitPoint = startingHitPoint + 1;
                
                col.enabled = true;

                for (int i = 0; i < rdList.Count; i++)
                {
                    rdList[i].bodyType = RigidbodyType2D.Static;
                }
                for (int i = 0; i < blockFragmentList.Count; i++)
                {
                    blockFragmentList[i].position = fragmentPosList[i];
                    blockFragmentList[i].rotation = Quaternion.Euler(Vector3.zero);
                }

                gameObject.SetActive(true);
            }

            void CheckShouldBlockDestroy()
            {
                float rangeDiff = Mathf.Abs(blockFragmentList[0].position.y - diedPosY);
                if (rangeDiff >= deathYRange)
                {
                    DisableThis();
                }
            }

            void DisableThis()
            {
                gameObject.SetActive(false);
            }

        }
    }
}
