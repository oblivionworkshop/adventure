﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class BlockBase : MonoBehaviour, IHitable
        {
            [SerializeField]
            protected SpriteRenderer spRendererOutter;
            [SerializeField]
            protected SpriteRenderer spRendererInner;
            Vector2 innerShift = new Vector2(0f, 0.03f);        // The reposition required after align with Collider due to Sprite picture not fit in border
            Vector2 outterShift = new Vector2(0.01f, 0f);       // The reposition required after align with Collider due to Sprite picture not fit in border
            [SerializeField]
            protected Transform sizeController;
            [SerializeField]
            protected BoxCollider2D col;
            [SerializeField]
            Animator anim;
            [SerializeField]
            protected bool fadeAfterHit = true;
            public bool FadeAfterHit { get { return fadeAfterHit; } }
            [SerializeField]
            protected int startingHitPoint = 3;
            protected bool alive;
            int _currentHitPoint;
            protected int currentHitPoint
            {
                get { return _currentHitPoint; }
                set
                {
                    _currentHitPoint = value;
                    if (_currentHitPoint <= 0)
                    {
                        _currentHitPoint = 0;
                        alive = false;
                    }
                }
            }
            

            int levelBelong = -1, subLevelBelong = -1;        // Records the level info that this block belongs to

            public Transform _transform { get; private set; }

            protected virtual void Awake()
            {
                _transform = gameObject.transform;
            }

            protected virtual void Start()
            {
                alive = true;
                currentHitPoint = startingHitPoint;
                if (!fadeAfterHit)
                {
                    spRendererInner.color = GameGM.instance.roughBlockColor;
                }
                else
                {
                    spRendererInner.color = GameGM.instance.fadeBlockColor;
                }
                
                ConvertBlockScale();
            }

            void ConvertBlockScale()
            {
                Vector2 inOutSpriteWidthGap = spRendererInner.size - spRendererOutter.size;

                Vector2 newOutterSize = new Vector2(spRendererOutter.size.x * sizeController.localScale.x, spRendererOutter.size.y * sizeController.localScale.y);
                spRendererOutter.size = newOutterSize;

                Vector2 newInnerSize = newOutterSize + inOutSpriteWidthGap;
                spRendererInner.size = newInnerSize;
                

                Transform colTransform = col.transform;
                colTransform.localScale = new Vector2(colTransform.localScale.x * sizeController.localScale.x, colTransform.localScale.y * sizeController.localScale.y);

                // Detach all child under Anim as I need to re-position the animator controller
                if (anim != null)
                {
                    Transform upperParent = anim.transform.parent;
                    List<Transform> childList = new List<Transform>();
                    foreach (Transform child in anim.transform)
                    {
                        childList.Add(child);
                        child.SetParent(upperParent);
                    }
                    anim.transform.position = col.bounds.center;
                    foreach (Transform child in childList)
                    {
                        child.SetParent(anim.transform);
                    }

                    Vector2 colLeftMostPos = new Vector2(col.bounds.min.x, col.bounds.max.y);
                    spRendererInner.transform.position = colLeftMostPos + innerShift;
                    spRendererOutter.transform.position = colLeftMostPos + outterShift;
                }
                //------------------------------------------------------------

                // Reset the sizeController to 1
                sizeController.localScale = Vector2.one;
            }

            public virtual void HitEffect()
            {
                //Debug.Log("Parent HitEffect");
                if (fadeAfterHit)
                {
                    if (currentHitPoint > 0)
                    {
                        currentHitPoint--;
                        AppGM.instance.PlayStoppableSFX(IEnum.AudioList.bounceBouncy, 1f, 0f, Random.Range(0.75f, 1.25f));

                        if (anim)
                        {
                            anim.SetTrigger("hit");
                        }

                        Color32 _color = spRendererOutter.color;
                        _color.a = (byte)(255f * (float)currentHitPoint / (float)startingHitPoint);
                        spRendererOutter.color = _color;

                        _color = spRendererInner.color;
                        _color.a = (byte)(255f * (float)currentHitPoint / (float)startingHitPoint);
                        spRendererInner.color = _color;

                        if (currentHitPoint <= 0)
                        {
                            spRendererOutter.enabled = false;
                            spRendererInner.enabled = false;
                            col.isTrigger = true;
                            //col.enabled = false;
                            //gameObject.SetActive(false);  
                        }
                    }
                }
            }

            public virtual void Revive()
            {
                if (col.isTrigger)
                {
                    col.isTrigger = false;
                }

                if (fadeAfterHit)
                {
                    alive = true;
                    currentHitPoint = startingHitPoint + 1;

                    spRendererOutter.enabled = true;
                    spRendererInner.enabled = true;
                    col.enabled = true;

                    Color32 _color = spRendererOutter.color;
                    _color.a = (byte)(255f);
                    spRendererOutter.color = _color;

                    _color = spRendererInner.color;
                    _color.a = (byte)(255f);
                    spRendererInner.color = _color;

                    gameObject.SetActive(true);
                }
            }

            public void SetLevelInfo(int _level, int _subLevel)
            {
                levelBelong = _level;
                subLevelBelong = _subLevel;
            }

            public void ActivateColliderEffector(bool flag)
            {
                col.usedByEffector = flag;
            }

            protected void DestroyThis()
            {
                Destroy(this.gameObject);
            }

            public void IncreaseHP(int _value)
            {
                startingHitPoint = startingHitPoint + _value;
            }

            #region Getter
            public bool GetIfAlive()
            {
                return alive;
            }

            public float GetMinX()
            {
                return col.bounds.min.x;
            }

            public float GetY()
            {
                return _transform.position.y;
            }

            public virtual float GetLowestY()
            {
                return _transform.position.y;
            }

            public int GetLevel()
            {
                return levelBelong;
            }

            public int GetSubLevel()
            {
                return subLevelBelong;
            }
            public BoxCollider2D GetCol()
            {
                return col;
            }
            #endregion

            #region Debug
            [ContextMenu("ShowMinX")]
            public void ShowMinX()
            {
                Debug.Log("Min X = " + col.bounds.min.x);
            }

            [ContextMenu("ShowWorldPosX")]
            public void ShowWorldPosX()
            {
                Debug.Log("World X = " + _transform.position.x);
            }
            #endregion
        }
    }
}