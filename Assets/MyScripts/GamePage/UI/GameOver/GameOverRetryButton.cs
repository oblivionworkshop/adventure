using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class GameOverRetryButton : ButtonBase
        {
            public UnityEngine.UI.Image btnImg;
            public TMPro.TMP_Text retryEntryFeeTxt;

        }
    }
}
