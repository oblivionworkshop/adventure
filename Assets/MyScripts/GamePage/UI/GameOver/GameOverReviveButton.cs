using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class GameOverReviveButton : ButtonBase
        {
            public UnityEngine.UI.Image btnImg;
            public TMPro.TMP_Text reviveFeeTxt;
        }
    }
}
