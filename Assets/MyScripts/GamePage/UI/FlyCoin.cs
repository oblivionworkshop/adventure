using Com.OblivionWorkshop.GamePage;
using Lean.Pool;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public class FlyCoin : MonoBehaviour, IPoolable
    {
        RectTransform rt;
        Transform _transform;
        Canvas _canvas;

        void Awake()
        {
            rt = gameObject.GetComponent<RectTransform>();
            _transform = gameObject.transform;
        }

        void IPoolable.OnDespawn() 
        {

        }

        void IPoolable.OnSpawn() 
        {
            if (_canvas == null)
            {
                _canvas = gameObject.AddComponent<Canvas>();
                _canvas.renderMode = RenderMode.ScreenSpaceCamera;
                _canvas.worldCamera = AppGM.instance.GetNormalUICamera();
                _canvas.overrideSorting = true;
                _canvas.sortingLayerName = "UI";
                _canvas.sortingOrder = 97;
            }

            rt.anchoredPosition = Vector2.zero;
            _transform.localScale = Vector3.zero;
        }
    }
}
