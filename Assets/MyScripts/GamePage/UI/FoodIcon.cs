using UnityEngine;
using UnityEngine.UI;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class FoodIcon : MonoBehaviour
        {
            Image icon;
            Animator anim;
            Sprite enableIcon, disableIcon;

            int animID;

            UnityEngine.Events.UnityAction<int> regAnimIndexAct;

            private void Awake()
            {
                regAnimIndexAct = SetAnimID;
            }

            public void BasicSetup(Sprite _enable, Sprite _disable)
            {
                icon = gameObject.GetComponent<Image>();
                anim = gameObject.GetComponent<Animator>();
                enableIcon = _enable;
                disableIcon = _disable;

                GameGM.instance.RegAnimIntoPauseList(anim, regAnimIndexAct);
            }

            public void Activate(bool _activate, bool _playGetAnim = false)
            {
                if (_activate)
                {
                    icon.sprite = enableIcon;
                    icon.color = new Color32(255, 255, 255, 255);
                    if (_playGetAnim)
                    {
                        anim.SetTrigger("get");
                    }
                }
                else
                {
                    icon.sprite = disableIcon;
                    icon.color = new Color32(255, 255, 255, 127);
                }
            }

            public void Blink()
            {
                anim.ResetTrigger("blink");
                anim.SetTrigger("blink");
            }

            void SetAnimID(int _id)
            {
                animID = _id;
            }
        }
    }
}
