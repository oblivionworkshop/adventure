﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using I2.Loc;
using TMPro;
using DG.Tweening;
using Lean.Pool;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class GameUI : MonoBehaviour
        {
            [Header("Game Over Menu")]
            [SerializeField]
            GameObject gameOverMenu;
            [SerializeField]
            RectTransform gameOverMenuHeader;
            [SerializeField]
            GameOverSelectStageButton selectStageBtn;
            [SerializeField]
            GameOverRetryButton retryBtn;
            [SerializeField]
            GameOverReviveButton reviveBtn;
            [SerializeField]
            GameOverSkipStageButton skipStageBtn;
            [SerializeField]
            GameOverQuitButton gameOverQuitBtn;
            [SerializeField]
            HorizontalLayoutGroup gameOverMenuLayoutFirstRow, gameOverMenuLayoutSecondRow;

            [Header("Challenge Mode")]
            [SerializeField]
            GameObject challengeMode;
            [SerializeField]
            Text blockLevelTxt;

            [Header("Control Panel")]
            //[SerializeField]
            //Button moveBtn;
            [SerializeField]
            EventTrigger moveBtnTrigger;
            //[SerializeField]
            //Button boostBtn;
            [SerializeField]
            EventTrigger boostBtnTrigger;
            //[SerializeField]
            //Button dashBtn;
            [SerializeField]
            EventTrigger dashBtnTrigger;

            [Header("Char Panel")]
            [SerializeField]
            Animator boostBarAnim;
            [SerializeField]
            Image boostBarMaskController;
            [SerializeField]
            Image boostBar;
            [SerializeField]
            Image boostBarIcon;
            [SerializeField]
            Transform boostBarPatternGrouper;
            [SerializeField]
            Animator boostBarPatternsAnim;
            [SerializeField]
            Color32 boostBarAllowColor, boostBarAllowPatternColor, boostBarDisallowColor, boostBarDisallowPatternColor;

            [Space(8f)]
            [SerializeField]
            Transform foodIconPlace;
            [SerializeField]
            FoodIcon iconPrefab;
            List<FoodIcon> foodIconList;

            [Space(8f)]
            [SerializeField]
            Animator boostTipsAnim;
            bool showingBoostTips;

            [Header("Pause Related")]
            [SerializeField]
            GameObject greyGamePanelBG;
            [Space(8f)]
            [SerializeField]
            PauseButton pauseBtn;
            [SerializeField]
            GameObject pauseMenuSet;
            [SerializeField]
            AnimBase pauseMenuAnimBase, decideControlMenuAnimBase;
            [SerializeField]
            PauseMenuControlButton controlBtn;
            [SerializeField]
            PauseMenuTutorialButton tutorialBtn;
            [SerializeField]
            PauseMenuContinueButton continueBtn;
            [SerializeField]
            PauseMenuQuitButton pauseMenuQuitBtn;

            [SerializeField]
            SliderBase bgmSlider;
            [SerializeField]
            SliderBase sfxSlider;
            [SerializeField]
            PauseMenuBGMButton bgmBtn;
            [SerializeField]
            PauseMenuSFXButton sfxBtn;

            [Space(10f)]
            [SerializeField]
            DecideControlCloseButton decideControlCloseBtn;
            [SerializeField]
            Toggle decideControlMoveBtnOnRightTog, decideControlMoveBtnOnLeftTog;

            Vector3 leftDashBtnXY = new Vector3(185f, 226f), rightDashBtnXY = new Vector3(-185f, 226f);
            Vector3 leftBoostBtnXY = new Vector3(400f, 35f), rightBoostBtnXY = new Vector3(-400f, 35f);
            Vector3 leftMoveBtnXY = new Vector3(360f, 35f), rightMoveBtnXY = new Vector3(-360f, 35f);

            [Header("Tutorial Related")]
            [SerializeField]
            GameObject tutorialPanel;
            [SerializeField]
            ButtonBase tutorialOKBtn;
            [SerializeField]
            AnimBase tutorialMenuAnimBase;

            public UnityAction tutorialMenuOKBtnAct;


            [Header("Level Related")]
            [SerializeField]
            Animator alertAnim;
            int alertAnimID;
            [SerializeField]
            ParticleSystem passLevelConfetti;
            
            [SerializeField]
            LeanGameObjectPool coinPool;
            [SerializeField]
            Vector2 randomOffestX, randomOffestY, randomRotation;

            [Header("Game End")]
            [SerializeField]
            GameObject toBeContinuePanel;

            [Header("Coin Bar")]
            [SerializeField]
            Slider coinBar;

            [Header("Resources and Prefabs")]
            [SerializeField]
            Sprite retryBtnDefaultImg;
            [SerializeField]
            Sprite reviveBtnDefaultImg;
            [SerializeField]
            Sprite graySqaureBtnImg;

            public UnityAction selectStageAct, skipStageAct;
            public UnityAction<int> retryAct, reviveAct;
            public UnityAction<PointerEventData> moveBtnDownAct, boostBtnDownAct, dashBtnDownAct;
            public UnityAction<PointerEventData> moveBtnUpAct, boostBtnUpAct;
            public UnityAction pauseMenuOnAct, pauseMenuOffAct;
            public UnityAction<bool> bgmBtnAfterPressAct, sfxBtnAfterPressAct, determineControlWalkBtnRightAct;
            public UnityAction<float> bgmVolChangeAct, sfxVolChangeAct;

            public UnityAction<bool> allowInputAct;
            public UnityAction quitBtnClickAct;

            public void GameUISetup(CharData charData, bool firstTime)
            {
                EventTrigger.Entry entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerDown;
                entry.callback.AddListener((data) => { moveBtnDownAct((PointerEventData)data); });
                moveBtnTrigger.triggers.Add(entry);

                entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerDown;
                entry.callback.AddListener((data) => { boostBtnDownAct((PointerEventData)data); });
                boostBtnTrigger.triggers.Add(entry);

                entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerDown;
                entry.callback.AddListener((data) => { dashBtnDownAct((PointerEventData)data); });
                dashBtnTrigger.triggers.Add(entry);

                entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerUp;
                entry.callback.AddListener((data) => { moveBtnUpAct((PointerEventData)data); });
                moveBtnTrigger.triggers.Add(entry);

                entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerUp;
                entry.callback.AddListener((data) => { boostBtnUpAct((PointerEventData)data); });
                boostBtnTrigger.triggers.Add(entry);

                /*pauseBtn.onClick.RemoveAllListeners();
                pauseBtn.onClick.AddListener(ShowPauseMenuFromPauseBtn);*/

                pauseBtn.clickAct += DefaultBtnClick;
                pauseBtn.clickAct += ShowPauseMenuFromPauseBtn;
                pauseBtn.ButtonSetup();

                decideControlCloseBtn.clickAct = DefaultBtnClick;
                decideControlCloseBtn.afterClickAct = PlayCloseDecideControlMenuAnim;
                decideControlCloseBtn.ButtonSetup();

                controlBtn.clickAct = DefaultBtnClick;
                controlBtn.afterClickAct = ControlBtnFinishClick;
                controlBtn.ButtonSetup();

                tutorialBtn.clickAct = DefaultBtnClick;
                tutorialBtn.afterClickAct = TutorialBtnFinishClick;
                tutorialBtn.ButtonSetup();

                continueBtn.clickAct = DefaultBtnClick;
                continueBtn.afterClickAct = ContinueBtnFinishClick;
                continueBtn.ButtonSetup();

                pauseMenuQuitBtn.clickAct = DefaultBtnClick;
                pauseMenuQuitBtn.afterClickAct = quitBtnClickAct;
                pauseMenuQuitBtn.ButtonSetup();

                // Need setup for Tutorial Menu Show Button

                //-----------------------------------------

                bgmSlider.slideAct = bgmVolChangeAct;
                bgmSlider.SliderSetup();

                sfxSlider.slideAct = sfxVolChangeAct;
                sfxSlider.SliderSetup();

                bgmBtn.clickAct = DefaultBtnClick;
                bgmBtn.afterClickAct = DefaultBtnFinishClick;
                bgmBtn.afterPressAct = bgmBtnAfterPressAct;
                bgmBtn.ButtonSetup();

                sfxBtn.clickAct = DefaultBtnClick;
                sfxBtn.afterClickAct = DefaultBtnFinishClick;
                sfxBtn.afterPressAct = sfxBtnAfterPressAct;
                sfxBtn.ButtonSetup();

                decideControlMoveBtnOnRightTog.onValueChanged.AddListener(ControlMoveBtnOnRightTog);
                decideControlMoveBtnOnLeftTog.onValueChanged.AddListener(ControlMoveBtnOnLeftTog);

                selectStageBtn.clickAct = DefaultBtnClick;
                selectStageBtn.afterClickAct = selectStageAct;
                selectStageBtn.ButtonSetup();

                gameOverQuitBtn.clickAct = DefaultBtnClick;
                gameOverQuitBtn.afterClickAct = quitBtnClickAct;
                gameOverQuitBtn.ButtonSetup();

                skipStageBtn.clickAct = DefaultBtnClick;
                skipStageBtn.afterClickAct = skipStageAct;
                skipStageBtn.ButtonSetup();

                // Setup Control Panel
                SwapControlBtnPos(AppGM.instance.GetIfWalkBtnOnRight());
                //----------------------------------

                // Reg Anim to GameGM Pausable Dict
                GameGM.instance.RegAnimIntoPauseList(alertAnim, SetAlertAnimID);
                //--------------------------------------------

                // Setup Energy Panel
                SetupDashEnergyPanel(charData.charType, charData.maxEnergy);
                //------------------------------------------------

                showingBoostTips = false;
                greyGamePanelBG.SetActive(firstTime);
                pauseMenuSet.transform.localScale = Vector3.zero;
                pauseMenuAnimBase.CloseInstant();
                decideControlMenuAnimBase.CloseInstant();
                ShowGameOverMenu(false);

                if (firstTime)
                {
                    tutorialPanel.SetActive(true);
                    tutorialMenuAnimBase.OpenInstant();

                    tutorialOKBtn.clickAct = DefaultBtnClick;
                    tutorialOKBtn.afterClickAct = FirstTimeTutorialOKBtnFinishClick;
                    tutorialOKBtn.ButtonSetup();
                }
            }

            #region Game Over Menu Related
            public void ShowGameOverMenu(bool show, bool showRevive = false, bool showSkipStage = false, int _indexLv = -1)
            {
                RectTransform rect1 = gameOverMenuLayoutFirstRow.GetComponent<RectTransform>();
                RectTransform rect2 = gameOverMenuLayoutFirstRow.GetComponent<RectTransform>();

                if (show)
                {
                    // Setup Retry Entry Fee & Retry Btn
                    SetupRetryFeeAndBtn(_indexLv);
                    //---------------------------------------------------

                    // Setup position for Header, First Row
                    gameOverMenuHeader.anchorMin = new Vector2(0f, 0.69f);
                    gameOverMenuHeader.anchorMax = new Vector2(1f, 0.79f);
                    gameOverMenuHeader.anchoredPosition = Vector2.zero;
                    gameOverMenuHeader.sizeDelta = Vector2.zero;

                    rect1.anchorMin = new Vector2(0f, 0.3f);
                    rect1.anchorMax = new Vector2(1f, 0.6f);
                    rect1.anchoredPosition = Vector2.zero;
                    rect1.sizeDelta = Vector2.zero;

                    if (!showRevive)
                    {
                        gameOverMenuLayoutFirstRow.spacing = 80f;
                        gameOverMenuLayoutSecondRow.spacing = 80f;
                        reviveBtn.gameObject.SetActive(false);
                        skipStageBtn.gameObject.SetActive(false);
                    }
                    else
                    {
                        gameOverMenuLayoutFirstRow.spacing = 40f;
                        gameOverMenuLayoutSecondRow.spacing = 40f;
                        reviveBtn.gameObject.SetActive(true);

                        // Setup Revive Fee & Revive Btn
                        SetupReviveFeeAndBtn(_indexLv);
                        //------------------------------------------

                        if (showSkipStage)
                        {
                            // Setup position for Header, First Row and Second Row
                            gameOverMenuHeader.anchorMin = new Vector2(0f, 0.78f);
                            gameOverMenuHeader.anchorMax = new Vector2(1f, 0.88f);
                            gameOverMenuHeader.anchoredPosition = Vector2.zero;
                            gameOverMenuHeader.sizeDelta = Vector2.zero;

                            rect1.anchorMin = new Vector2(0f, 0.42f);
                            rect1.anchorMax = new Vector2(1f, 0.72f);
                            rect1.anchoredPosition = Vector2.zero;
                            rect1.sizeDelta = Vector2.zero;

                            rect2.anchorMin = new Vector2(0f, 0.06f);
                            rect2.anchorMax = new Vector2(1f, 0.36f);
                            rect2.anchoredPosition = Vector2.zero;
                            rect2.sizeDelta = Vector2.zero;

                            skipStageBtn.gameObject.SetActive(true);
                        }
                    }
                }

                gameOverMenu.SetActive(show);
            }

            public void SetupRetryFeeAndBtn(int indexLv)
            {
                if (indexLv < 0)
                {
                    Debug.LogError("indexLv does not pass into function correctly!");
                    indexLv = 99;
                }

                int fee = AppGM.instance.GetDiscountedEntryFee(indexLv, GameGM.instance.GameBonusMultiplier, out bool positive);
                retryBtn.retryEntryFeeTxt.text = fee.ToString();

                if (positive)
                {
                    retryBtn.retryEntryFeeTxt.color = ResourceUI.instance.FeeTxtPositiveWhiteColor;
                    retryBtn.btnImg.sprite = retryBtnDefaultImg;

                    retryBtn.clickAct = DefaultBtnClick;
                    retryBtn.afterClickAct = () => { retryAct(fee); };
                    retryBtn.ButtonSetup();
                }
                else
                {
                    retryBtn.retryEntryFeeTxt.color = ResourceUI.instance.FeeTxtNegativeRedColor;
                    retryBtn.btnImg.sprite = graySqaureBtnImg;

                    retryBtn.clickAct = DefaultBtnClick;
                    retryBtn.afterClickAct = AllowInput;
                    retryBtn.ButtonSetup();
                }
            }

            public void SetupReviveFeeAndBtn(int indexLv)
            {
                if (indexLv < 0)
                {
                    Debug.LogError("_indexLv does not pass into function correctly!");
                    indexLv = 99;
                }

                int reviveFee = AppGM.instance.GetReviveFee(indexLv, GameGM.instance.GameBonusMultiplier, out bool revivePositive);
                reviveBtn.reviveFeeTxt.text = reviveFee.ToString();

                if (revivePositive)
                {
                    reviveBtn.reviveFeeTxt.color = ResourceUI.instance.FeeTxtPositiveWhiteColor;
                    reviveBtn.btnImg.sprite = reviveBtnDefaultImg;

                    reviveBtn.clickAct = DefaultBtnClick;
                    reviveBtn.afterClickAct = () => { reviveAct(reviveFee); };
                    reviveBtn.ButtonSetup();
                }
                else
                {
                    reviveBtn.reviveFeeTxt.color = ResourceUI.instance.FeeTxtNegativeRedColor;
                    reviveBtn.btnImg.sprite = graySqaureBtnImg;

                    reviveBtn.clickAct = DefaultBtnClick;
                    reviveBtn.afterClickAct = AllowInput;
                    reviveBtn.ButtonSetup();
                }
            }
            #endregion

            #region Pause Menu Related

            void ShowPauseMenuFromPauseBtn()
            {
                AppGM.instance.PlaySFX(IEnum.AudioList.buttonClick);

                PauseGame();
                pauseMenuSet.transform.localScale = Vector3.one;
                OpenPauseMenuAnim();

                UIAudioDataClass audio = AppGM.instance.GetAudioSettings();

                if (audio.bgmMute)
                {
                    bgmBtn.PressInstant();
                }
                else
                {
                    bgmBtn.ReleaseInstant();
                }

                if (audio.sfxMute)
                {
                    sfxBtn.PressInstant();
                }
                else
                {
                    sfxBtn.ReleaseInstant();
                }

                bgmSlider.SetSliderValue(audio.bgmVol);
                sfxSlider.SetSliderValue(audio.sfxVol);
            }

            public void PauseGame()
            {
                pauseMenuOnAct();
                TurnOnBoostAnim(false);
            }

            void OpenPauseMenuAnim()
            {
                greyGamePanelBG.SetActive(true);

                pauseMenuAnimBase.afterOpenAnim = null;
                pauseMenuAnimBase.afterOpenAnim = AllowInput;
                pauseMenuAnimBase.Open();
            }

            void PlayClosePauseMenuAnim()
            {
                pauseMenuAnimBase.afterCloseAnim = null;
                pauseMenuAnimBase.afterCloseAnim = ClosePauseMenu;
                pauseMenuAnimBase.Close();
            }

            void ClosePauseMenu()
            {
                greyGamePanelBG.SetActive(false);

                pauseMenuSet.transform.localScale = Vector3.zero;
                pauseMenuOffAct();
                TurnOnBoostAnim(true);
            }

            void PlayFirstTimeCloseTutorialMenuAnim()
            {
                tutorialMenuAnimBase.afterCloseAnim = null;
                tutorialMenuAnimBase.afterCloseAnim = FirstTimeCloseTutorialMenu;
                tutorialMenuAnimBase.Close();
            }

            void FirstTimeCloseTutorialMenu()
            {
                tutorialPanel.SetActive(false);
                greyGamePanelBG.SetActive(false);

                tutorialMenuOKBtnAct();
                TurnOnBoostAnim(true);
            }
            
            public void TriggerToBeContinuedAnim()
            {
                toBeContinuePanel.SetActive(true);
            }

            void SetupDecideControlMenu()
            {
                if (AppGM.instance.GetIfWalkBtnOnRight())
                {
                    decideControlMoveBtnOnRightTog.isOn = true;
                }
                else
                {
                    decideControlMoveBtnOnLeftTog.isOn = true;
                }
            }

            void ShowDecideControlMenu()
            {
                SetupDecideControlMenu();

                decideControlMenuAnimBase.afterOpenAnim = null;
                decideControlMenuAnimBase.afterOpenAnim = AllowInput;
                decideControlMenuAnimBase.Open();
            }

            void PlayCloseDecideControlMenuAnim()
            {
                decideControlMenuAnimBase.afterCloseAnim = null;
                decideControlMenuAnimBase.afterCloseAnim = OpenPauseMenuAnim;
                decideControlMenuAnimBase.Close();
            }

            void SetupTutorialPanelMenu()
            {
                tutorialOKBtn.clickAct = DefaultBtnClick;
                tutorialOKBtn.afterClickAct = PlayCloseTutorialPanelMenuAnim;
                tutorialOKBtn.ButtonSetup();
            }

            void ShowTutorialPanelMenu()
            {
                tutorialMenuAnimBase.afterOpenAnim = null;
                tutorialMenuAnimBase.afterOpenAnim = AllowInput;
                
                tutorialPanel.SetActive(true);
                SetupTutorialPanelMenu();
                tutorialMenuAnimBase.Open();
            }

            void PlayCloseTutorialPanelMenuAnim()
            {
                tutorialMenuAnimBase.afterCloseAnim = null;
                tutorialMenuAnimBase.afterCloseAnim = TransitionFromTutorialPanelToPauseMenu;
                tutorialMenuAnimBase.Close();
            }

            void TransitionFromTutorialPanelToPauseMenu()
            {
                tutorialPanel.SetActive(false);
                OpenPauseMenuAnim();
            }

            void ContinueBtnFinishClick()
            {
                PlayClosePauseMenuAnim();
            }

            void ControlBtnFinishClick()
            {
                pauseMenuAnimBase.afterCloseAnim = null;
                pauseMenuAnimBase.afterCloseAnim = ShowDecideControlMenu;
                pauseMenuAnimBase.Close();
            }

            void TutorialBtnFinishClick()
            {
                pauseMenuAnimBase.afterCloseAnim = null;
                pauseMenuAnimBase.afterCloseAnim = ShowTutorialPanelMenu;
                pauseMenuAnimBase.Close();
            }
            void FirstTimeTutorialOKBtnFinishClick()
            {
                PlayFirstTimeCloseTutorialMenuAnim();
            }

            public void PauseAllTween()
            {
                DOTween.PauseAll();
            }

            public void ResumeAllTween()
            {
                DOTween.PlayAll();
            }
            #endregion

            #region Level UI Related
            public void ShowChallengeGameModeUI(bool show)
            {
                challengeMode.SetActive(show);
            }

            public void BlockLevelUpdate(int level, int subLevel)
            {
                LocalizedString locString = "GamePage/Level";
                blockLevelTxt.text = locString + " " + (level + 1).ToString() + "-" + (subLevel+1).ToString();
            }

            public void PlayPassLevelConfetti()
            {
                passLevelConfetti.Clear();
                passLevelConfetti.Play();
            }
            #endregion

            #region Coin Related
            public void ShowCoinBar(bool show)
            {
                coinBar.gameObject.SetActive(show);
            }

            public void UpdateCoinBar(float val)
            {
                coinBar.value = val;
            }

            [ContextMenu("Test Fly Coin")]
            void TestFlyCoin()
            {
                RewardCoinAnim(()=>ResourceUI.instance.AddCoin(0, false), ()=>AppGM.instance.PlayStoppableSFX(IEnum.AudioList.getCoin, 2f, _pitch: 0.75f), 20);
            }

            public void RewardCoinAnim(UnityAction _resouceUIAct, UnityAction _soundAct, int _num)
            {
                float delay = 0f;

                Vector2 coinImgWorldPos = ResourceUI.instance.GetCoinImgWorldPos();

                Transform coinTransform = ResourceUI.instance.CoinImg.transform;

                Sequence mainSeq = DOTween.Sequence();

                for (int i = 0; i < _num; i++)
                {
                    GameObject coin = coinPool.Spawn(coinPool.transform);
                    Transform child = coin.transform;
                    RectTransform rt = coin.GetComponent<RectTransform>();

                    float ranX = Random.Range(randomOffestX.x, randomOffestX.y);
                    float ranY = Random.Range(randomOffestY.x, randomOffestY.y);
                    
                    if (i != 0)
                    {
                        Vector2 newPos = rt.anchoredPosition + new Vector2(ranX, ranY);
                        rt.anchoredPosition = newPos;
                        child.Rotate(0f, 0f, Random.Range(randomRotation.x, randomRotation.y));
                    }

                    Sequence coinSeq = DOTween.Sequence();
                    coinSeq.Append(child.DOScale(1f, 0.45f).SetEase(Ease.OutBack));
                    coinSeq.AppendInterval(0.25f);
                    coinSeq.Append(child.DOMove(coinImgWorldPos, 1.2f).SetEase(Ease.InBack).OnComplete(() =>
                    {
                        coinTransform.DOScale(1.3f, 0.05f);
                        coinTransform.DOScale(1f, 0.2f).SetDelay(0.05f);

                        _soundAct();
                    }));
                    coinSeq.Join(child.DORotate(Vector3.zero, 0.75f).SetEase(Ease.Flash));
                    coinSeq.Join(child.DOScale(1.5f, 1.2f).SetEase(Ease.OutBack));
                    coinSeq.AppendCallback(() => coinPool.Despawn(coin));
                    //coinSeq.Append(child.DOScale(0f, 0.45f).SetEase(Ease.OutBack));

                    mainSeq.Insert(delay, coinSeq);
                    delay += 0.15f;
                }

                mainSeq.AppendCallback(() =>
                {
                    _resouceUIAct();
                    //Destroy(coinGroup.gameObject);
                });
                mainSeq.Play();
            }
            #endregion

            #region Char Panel Related
            public void UpdateBoostMeter(float finalValue)
            {
                //if (finalValue >= 1f || GameGM.instance.GetChar().IsCharBoosting())
                if (finalValue >= GameGM.instance.GetMinBoostEnergyRequire() || GameGM.instance.GetChar().IsCharBoosting())
                {
                    boostBarPatternsAnim.SetBool("allowBoost", true);
                    TurnOnBoostAnim(true);
                    ChangeBoostBarColor(true);
                }
                else
                {
                    boostBarPatternsAnim.SetBool("allowBoost", true);
                    TurnOnBoostAnim(false);
                    ChangeBoostBarColor(false);
                }
                float val = 556f * finalValue;
                RectTransform rt = boostBarMaskController.GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(556f - val, 45f);
            }

            public void TurnOnBoostAnim(bool flag)
            {
                if (flag)
                {
                    boostBarPatternsAnim.speed = 1f;
                }
                else
                {
                    boostBarPatternsAnim.speed = 0f;
                }
            }

            void ChangeBoostBarColor(bool allowBoost)
            {
                if (allowBoost)
                {
                    boostBarIcon.color = boostBarAllowColor;
                    boostBar.color = boostBarAllowColor;
                    for (int i = 0; i < boostBarPatternGrouper.childCount; i++)
                    {
                        Image childImg = boostBarPatternGrouper.GetChild(i).GetComponent<Image>();
                        childImg.color = boostBarAllowPatternColor;
                    }
                }
                else
                {
                    boostBarIcon.color = boostBarDisallowColor;
                    boostBar.color = boostBarDisallowColor;
                    for (int i = 0; i < boostBarPatternGrouper.childCount; i++)
                    {
                        Image childImg = boostBarPatternGrouper.GetChild(i).GetComponent<Image>();
                        childImg.color = boostBarDisallowPatternColor;
                    }
                }
            }

            public void BlinkBoostBar()
            {
                boostBarAnim.ResetTrigger("blink");
                boostBarAnim.SetTrigger("blink");
            }

            public void ShowBoostTips(bool show)
            {
                showingBoostTips = show;
                boostTipsAnim.SetBool("showTips", show);
            }

            void SetupDashEnergyPanel(IEnum.CharacterType type, int max)
            {
                Sprite enableIcon = GetFoodEnableIconByChar(type);
                Sprite disableIcon = GetFoodDisableIconByChar(type);
                foodIconList = new List<FoodIcon>();

                for (int i = 0; i < max; i++)
                {
                    FoodIcon icon = Instantiate(iconPrefab);
                    icon.gameObject.transform.SetParent(foodIconPlace, false);
                    icon.gameObject.transform.localScale = Vector3.one;
                    icon.BasicSetup(enableIcon, disableIcon);

                    foodIconList.Add(icon);
                }
            }

            public void BlinkFoodIcons()
            {
                for (int i = 0; i < foodIconList.Count; i++)
                {
                    foodIconList[i].Blink();
                }
            }

            public void UpdateDashEnergyPanel(int prevEnergy, int currentEnergy)
            {
                int counter = foodIconList.Count;
                for (int i = 0; i < counter; i++)
                {
                    if (i < currentEnergy)
                    {
                        foodIconList[i].Activate(true, prevEnergy < currentEnergy && i == currentEnergy - 1? true : false);
                    }
                    else
                    {
                        foodIconList[i].Activate(false);
                    }
                    
                }
            }

            Sprite GetFoodEnableIconByChar(IEnum.CharacterType _type)
            {
                string enableTxt = "_Enable";

                switch (_type)
                {
                    case IEnum.CharacterType.none:
                        Debug.LogError("Character Type is NONE!! No Food Icon matches!!");
                        return null;
                    case IEnum.CharacterType.rabbit:
                        Sprite pic = Resources.Load<Sprite>(AppGM.instance.foodUIPath + "Carrot" + enableTxt);
                        return pic;
                    default:
                        Debug.LogError("Undefined Character Type!! No Food Icon matches!!");
                        return null;
                }
            }

            Sprite GetFoodDisableIconByChar(IEnum.CharacterType _type)
            {
                string disableTxt = "_Disable";

                switch (_type)
                {
                    case IEnum.CharacterType.none:
                        Debug.LogError("Character Type is NONE!! No Food Icon matches!!");
                        return null;
                    case IEnum.CharacterType.rabbit:
                        Sprite pic = Resources.Load<Sprite>(AppGM.instance.foodUIPath + "Carrot" + disableTxt);
                        return pic;
                    default:
                        Debug.LogError("Undefined Character Type!! No Food Icon matches!!");
                        return null;
                }
            }

            public bool IsBoostTipsShowing()
            {
                return showingBoostTips;
            }
            #endregion

            #region Alert Related
            public void PlayAlertScreenAnim()
            {
                alertAnim.SetTrigger("alert");
            }

            [ContextMenu("Test Instant Finish Alert")]
            public void StopAlertSceenAnim()
            {
                alertAnim.Play("Default", 0, 0f);
            }

            void SetAlertAnimID(int _index)
            {
                alertAnimID = _index;
            }
            #endregion

            #region Control Panel Related
            void ControlMoveBtnOnRightTog(bool tog)
            {
                if (decideControlMoveBtnOnRightTog.isOn)
                {
                    SwapControlBtnPos(true);
                    determineControlWalkBtnRightAct(true);          // Record PlayerPrefs
                }
            }

            void ControlMoveBtnOnLeftTog(bool tog)
            {
                if (decideControlMoveBtnOnLeftTog.isOn)
                {
                    SwapControlBtnPos(false);
                    determineControlWalkBtnRightAct(false);          // Record PlayerPrefs
                }
            }

            void SwapControlBtnPos(bool moveBtnWillOnRight)
            {
                RectTransform moveRect = moveBtnTrigger.GetComponent<RectTransform>();
                RectTransform boostRect = boostBtnTrigger.GetComponent<RectTransform>();
                RectTransform dashRect = dashBtnTrigger.GetComponent<RectTransform>();

                if (moveBtnWillOnRight)
                {
                    moveRect.anchorMin = new Vector2(1f, 0f);
                    moveRect.anchorMax = new Vector2(1f, 0f);
                    moveRect.pivot = new Vector2(1f, 0f);

                    boostRect.anchorMin = Vector2.zero;
                    boostRect.anchorMax = Vector2.zero;
                    boostRect.pivot = Vector2.zero;

                    dashRect.anchorMin = Vector2.zero;
                    dashRect.anchorMax = Vector2.zero;
                    dashRect.pivot = Vector2.zero;

                    moveRect.anchoredPosition = rightMoveBtnXY;
                    boostRect.anchoredPosition = leftBoostBtnXY;
                    dashRect.anchoredPosition = leftDashBtnXY;
                }
                else
                {
                    moveRect.anchorMin = Vector2.zero;
                    moveRect.anchorMax = Vector2.zero;
                    moveRect.pivot = Vector2.zero;

                    boostRect.anchorMin = new Vector2(1f, 0f);
                    boostRect.anchorMax = new Vector2(1f, 0f);
                    boostRect.pivot = new Vector2(1f, 0f);

                    dashRect.anchorMin = new Vector2(1f, 0f);
                    dashRect.anchorMax = new Vector2(1f, 0f);
                    dashRect.pivot = new Vector2(1f, 0f);

                    moveRect.anchoredPosition = leftMoveBtnXY;
                    boostRect.anchoredPosition = rightBoostBtnXY;
                    dashRect.anchoredPosition = rightDashBtnXY;
                }
            }
            #endregion

            #region Input Related
            void BlockInput()
            {
                allowInputAct(false);
            }

            void AllowInput()
            {
                allowInputAct(true);
            }

            void DefaultBtnClick()
            {
                BlockInput();
                AppGM.instance.PlaySFX(IEnum.AudioList.buttonClick);
            }

            void DefaultBtnFinishClick()
            {
                AllowInput();
            }
            #endregion
        }
    }
}