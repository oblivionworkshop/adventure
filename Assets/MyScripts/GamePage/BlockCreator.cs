﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class BlockCreator : MonoBehaviour
        {

            public UnityEngine.Events.UnityAction<Vector2, EndPoint> createAction;

            Transform _transform;

            public void Awake()
            {
                _transform = gameObject.transform;
            }

            private void OnTriggerEnter2D(Collider2D col)
            {
                if (col.gameObject.CompareTag("EndPoint"))
                {
                    EndPoint endPoint = col.gameObject.GetComponent<EndPoint>();
                    if (createAction != null)
                    {
                        Vector2 createPos = new Vector2(_transform.position.x, 0f);
                        createAction.Invoke(createPos, endPoint);
                    }
                    else
                    {
                        Debug.LogError("No Create Action!");
                    }

                }
            }
        }
    }
}