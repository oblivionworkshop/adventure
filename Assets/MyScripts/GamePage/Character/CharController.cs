﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using UnityEditor;

namespace Com.OblivionWorkshop
{
    namespace GamePage
    {
        public class CharController : MonoBehaviour
        {

            [SerializeField]
            float speed;
            [SerializeField]
            GameObject charSpriteObj;
            [SerializeField]
            Transform charOriginPoint;
            [SerializeField]
            BlockCreator blockCreator;

            Transform _transform;
            Rigidbody2D rd;
            float charGravity = 1.5f;
            
            [SerializeField]
            Collider2D col;
            [SerializeField]
            Transform psParent;
            List<ParticleSystem> psList;            // Useful when need to pause all the particle system when game paused

            [Header("Power Ups")]
            [SerializeField]
            GameObject bubblyShield;

            [Header("Body Parts")]
            [SerializeField]
            Transform eyebrowsHandsome;
            [SerializeField]
            Transform eyebrowsRun;
            [SerializeField]
            Transform eyeNormal, eyeCloseStrong, eyeSmile, eyeDead;
            [SerializeField]
            SpriteRenderer mouthNormal, mouthSad, mouthOpenSmall, mouthOpenBig;
            [SerializeField]
            GameObject runEffectGO;
            [SerializeField]
            ParticleSystem dashPSEffect, eatPSEffect, diePSEffect;

            Animator anim;
            bool _falling;
            bool falling
            {
                get { return _falling; }
                set
                {
                    _falling = value;
                    anim.SetBool("fall", _falling);
                }
            }

            float charHighestY = 0f;

            Vector3 charStartPos;
            BlockBase currentBlock;     // The block that is currently processing in OnCollisionEnter and it will become null in OnCollisionExit, or cleared after OnCollisionEnter if the block is not alive anymore
            BlockBase lastBouncedBlock;        // The latest block that the char Bounce on
            float? lastBouncedBlockY;
            BlockBase lastHitBlock;        // The latest block that comes into collision with the ball
            float? lastHitBlockY;
            bool alive = true;
            bool charSetupDone = false;
            ContactPoint2D contact;     // The last contact info between on the char
            bool NoBounceOnLastCollision = false;
            float basicBounceForce = 9.8f;
            float smoothingForce = 1.15f;
            float deathYRange = 90f;           // determine how far the Y difference should be for the char pos and the last block Y pos

            float forceBounceCounter;           // Force bounce the char if detected the RD is (0,0) for over a period of time

            // Ball Control related var
            float inputValue, inputBoostValue;
            float simHValue, simVValue;         // Value in mobile input to simulate the Input.GetAxis
            bool horizontalPressed, verticalPressed;
            bool horizontalFirstPressed, verticalFirstPressed;      // Flags to determine if the coming h/v-pressed is the first time press or not. Will become true once the player release the axis button

            // Boost related var
            float boostMeter;
            float boostMeterStackRate = 0.1f;
            bool _isBoosting;
            bool isBoosting
            {
                get { return _isBoosting; }
                set
                {
                    _isBoosting = value;
                    if (_isBoosting == false)
                    {
                        BoostAnim(false);
                        // The purpose of the following code is to ensure that the boost bar will turn grey
                        AccumulateBoostMeter(0f);
                    }
                }
            }

            // Dash related var
            bool isDashing;
            float dashAccTime, dashTotalTime = 0.2f;

            public UnityAction charAfterDeathAct, charStartDashAct, charEndDashAct, eatFoodAct, charStartBoostAct, charStopBoostAct, stopCamFollowAct;
            public UnityAction<EndPoint> charPassLevelAct;
            public UnityAction<bool> allowInputAct;

            // Invulnerable related var
            bool invulnerable = false;
            float invulnerableAccTime, invulnerableTotalTime;
            public UnityAction<bool> invulnerableAct; 

            private void Awake()
            {
                _transform = gameObject.transform;
                charStartPos = _transform.position;

                rd = GetComponent<Rigidbody2D>();
                if (!col)
                {
                    col = GetComponent<Collider2D>();
                }
                anim = gameObject.GetComponent<Animator>();
            }

            public void CharSetup()
            {
                alive = true;
                falling = false;
                SetupParticleSystemList();
                BoostReset();
                ApplyStartGamePowerUp();
                ActivateChar(true);
                charSetupDone = true;
            }

            private void Update()
            {
                if (!charSetupDone) return;

                if (Input.GetKeyDown(KeyCode.Space) && AppGM.instance.GetIsPC())
                {
                    DashCommand();
                }

                CheckShouldCharDestroy();

                if (alive && GameGM.instance.GetGameState() == IEnum.GameState.playing)
                {
                    ForceBounceIfNotBouncingByCheckingTime();
                }
            }

            private void FixedUpdate()
            {
                if (alive && GameGM.instance.GetGameState() == IEnum.GameState.playing)
                {
                    CharControl();
                    ForceBounceIfNotBouncingByCheckingTouch();
                    CheckDashing();
                }
            }

            void CharControl()
            {
                if (GameGM.instance.GetGameState() == IEnum.GameState.playing)
                {
                    if (AppGM.instance.GetIsPC())
                    {
                        //--------------------This Section handles PC Control--------------------------
                        //-----------------------------------------------------------------------------

                        // Determine the input state at the beginning
                        horizontalPressed = (Input.GetAxis("Horizontal") > 0f && Input.GetAxis("Horizontal") >= inputValue) ? true : false;
                        verticalPressed = (Input.GetAxis("Vertical") > 0f && Input.GetAxis("Vertical") >= inputBoostValue) ? true : false;

                        if (!horizontalPressed && !horizontalFirstPressed)
                        {
                            // Run only once for state change
                            horizontalFirstPressed = true;
                        }
                        if (!verticalPressed && !verticalFirstPressed)
                        {
                            // Run only once for state change
                            verticalFirstPressed = true;
                        }
                        //---------------------------------------------------------------------------

                        if ((GameGM.instance.GetAllowMoveBack() && Input.GetAxis("Horizontal") != 0f) ||
                            (!GameGM.instance.GetAllowMoveBack() && Input.GetAxis("Horizontal") > 0f))
                        {
                            float finalSpeed = Input.GetAxis("Horizontal") * speed;
                            _transform.Translate(Vector3.right * finalSpeed, Space.World);
                            if (!falling)
                            {
                                GameGM.instance.UpdateCoinBar(_speed:finalSpeed);
                            }

                            if (!isBoosting)
                            {
                                AccumulateBoostMeter(Input.GetAxis("Horizontal"));
                            }
                        }
                        
                        if (horizontalPressed && verticalPressed)
                        {
                            //if ((!isBoosting && boostMeter == 1f) || (isBoosting && boostMeter > 0f))
                            if ((!isBoosting && boostMeter >= GameGM.instance.GetMinBoostEnergyRequire()) || (isBoosting && boostMeter > 0f))
                            {
                                float finalAccSpeed = Input.GetAxis("Vertical") * speed * 1.25f;
                                Boost(finalAccSpeed);
                            }
                            else if (isBoosting && boostMeter == 0f)
                            {
                                // Only run once when state transition
                                isBoosting = false;
                            }/*else if (!isBoosting && boostMeter < 1f)
                            {
                                // Prompt error blink to show that not enough energy is filled, prompt only once
                                if (horizontalFirstPressed || verticalFirstPressed)
                                {
                                    GameGM.instance.BlinkBoostBar();
                                }
                            }*/
                        } else if (!verticalPressed)
                        {
                            // Case that horizontal is not pressed & vertical also not pressed
                            if (isBoosting)
                            {
                                isBoosting = false;
                            }
                            if (!falling)
                            {
                                GameGM.instance.FadeCoinBar();
                            }
                            
                        } else if (verticalPressed)
                        {
                            // Case that horizontal is not pressed but vertical is pressed
                            if (isBoosting)
                            {
                                isBoosting = false;
                            }
                            if (!falling)
                            {
                                GameGM.instance.FadeCoinBar();
                            }

                            // Prompt tips that if the player want to boost, need to press both horizontal & vertical
                            if (verticalFirstPressed)
                            {
                                GameGM.instance.ShowBoostTips(true);
                                
                            }
                        }

                        if ((!verticalPressed || (verticalPressed && horizontalPressed)) && GameGM.instance.IsBoostTipsShowing())
                        {
                            GameGM.instance.ShowBoostTips(false);
                        }

                        // Reset the first press var after this frame has been done, only if the corresponding axis button is pressed
                        if (horizontalPressed && horizontalFirstPressed)
                        {
                            // Run only once for state change
                            horizontalFirstPressed = false;
                        }
                        if (verticalPressed && verticalFirstPressed)
                        {
                            // Run only once for state change
                            verticalFirstPressed = false;
                        }
                        //----------------------------------------------------------

                        // Act as a temp record for Input.H & Input.V in this frame. In next frame, if Input.H / Input.V is smaller than these two value,
                        // means the player once released their hand on the referring button
                        inputValue = Input.GetAxis("Horizontal");
                        inputBoostValue = Input.GetAxis("Vertical");
                    }
                    else
                    {
                        //----------------This Section handles Mobile Control--------------------------
                        //-----------------------------------------------------------------------------

                        // Determine the input state at the beginning
                        horizontalPressed = GameGM.instance.GetMoveBtnPressed() ? true : false;
                        verticalPressed = GameGM.instance.GetBoostBtnPressed() ? true : false;

                        if (!horizontalPressed && !horizontalFirstPressed)
                        {
                            // Only Run once for state Change
                            horizontalFirstPressed = true;
                        }
                        if (!verticalPressed && !verticalFirstPressed)
                        {
                            // Only Run once for state Change
                            verticalFirstPressed = true;
                        }
                        //---------------------------------------------------------------------------


                        bool noHInput = false;
                        
                        int dir = 0;
                        if (horizontalPressed)
                            dir = 1;
                        else if (!GameGM.instance.GetMoveBtnPressed())
                        {
                            // no input at all. force must lerp into zero
                            noHInput = true;
                        }

                        // Formulation of input value & input boost value
                        if (horizontalPressed)
                        {
                            // increase force towards desired direction & simulate Horizontal Axis
                            simHValue += Time.deltaTime * dir * 3f;
                            simHValue = Mathf.Clamp(simHValue, -1, 1);
                        }
                        else
                        {
                            // lerping force into zero if the force is greater than a threshold (0.01)
                            if (Mathf.Abs(simHValue) >= 0.01f)
                            {
                                int opositeDir = (simHValue > 0) ? -1 : 1;
                                simHValue += Time.deltaTime * 3f * opositeDir;
                            }
                            else
                            {
                                simHValue = 0;
                            }
                        }

                        if (verticalPressed)
                        {
                            // increase force towards desired direction & simulate Vertical Axis
                            simVValue += Time.deltaTime * 1f * 3f;
                            simVValue = Mathf.Clamp(simVValue, -1, 1);
                        }
                        else
                        {
                            // lerping boost force into zero if the boost force is greater than a threshold (0.01)
                            if (Mathf.Abs(simVValue) >= 0.01f)
                            {
                                int opositeDir = (simVValue > 0) ? -1 : 1;
                                simVValue += Time.deltaTime * 3f * opositeDir;
                            }
                            else
                            {
                                simVValue = 0;
                            }
                        }
                        //----------------------------------------------------------------------


                        if (horizontalPressed)
                        {
                            float finalSpeed = simHValue * speed;
                            _transform.Translate(Vector3.right * finalSpeed, Space.World);
                            if (!falling)
                            {
                                GameGM.instance.UpdateCoinBar(_speed: finalSpeed);
                            }

                            if (!isBoosting)
                            {
                                AccumulateBoostMeter(simHValue);
                            }
                        }

                        if (horizontalPressed && verticalPressed)
                        {
                            //if ((!isBoosting && boostMeter == 1f) || (isBoosting && boostMeter > 0f))
                            if ((!isBoosting && boostMeter >= GameGM.instance.GetMinBoostEnergyRequire()) || (isBoosting && boostMeter > 0f))
                            {
                                float finalAccSpeed = simVValue * speed * 1.25f;
                                Boost(finalAccSpeed);
                            }
                            else if (isBoosting && boostMeter == 0f)
                            {
                                // Only run once when state transition
                                isBoosting = false;
                            }
                            /*else if (!isBoosting && boostMeter < 1f)
                            {
                                // Prompt error blink to show that not enough energy is filled, prompt only once
                                if (horizontalFirstPressed || verticalFirstPressed)
                                {
                                    GameGM.instance.BlinkBoostBar();
                                }
                            }*/
                        }
                        else if (!verticalPressed)
                        {
                            // Case that horizontal is not pressed & vertical also not pressed
                            if (isBoosting)
                            {
                                isBoosting = false;
                            }
                            if (!falling)
                            {
                                GameGM.instance.FadeCoinBar();
                            }
                            
                        }
                        else if (verticalPressed)
                        {
                            // Case that horizontal is not pressed but vertical is pressed
                            if (isBoosting)
                            {
                                isBoosting = false;
                            }
                            if (!falling)
                            {
                                GameGM.instance.FadeCoinBar();
                            }

                            // Prompt tips that if the player want to boost, need to press both horizontal & vertical
                            if (verticalFirstPressed)
                            {
                                GameGM.instance.ShowBoostTips(true);

                            }
                        }

                        if ((!verticalPressed || (verticalPressed && horizontalPressed)) && GameGM.instance.IsBoostTipsShowing())
                        {
                            GameGM.instance.ShowBoostTips(false);
                        }

                        // Reset the first press var after this frame has been done, only if the corresponding axis button is pressed
                        if (horizontalPressed && horizontalFirstPressed)
                        {
                            // Run only once for state change
                            horizontalFirstPressed = false;
                        }
                        if (verticalPressed && verticalFirstPressed)
                        {
                            // Run only once for state change
                            verticalFirstPressed = false;
                        }
                        //----------------------------------------------------------
                    }
                }
            }

            void ForceBounceIfNotBouncingByCheckingTouch()
            {
                float charMaxX = col.bounds.max.x;
                float blockMinX;
                if (currentBlock != null)
                {
                    blockMinX = currentBlock.GetMinX();
                }
                else
                {
                    blockMinX = Mathf.Infinity;
                }

                if (NoBounceOnLastCollision && charOriginPoint.position.y >= lastHitBlock._transform.position.y && charMaxX >= blockMinX)
                {
                    IHitable hitable = contact.collider.transform.parent.parent.GetInterface<IHitable>();
                    if (hitable != null)
                    {
#if _DEBUG_Char
                        Debug.Log("<color=orange>Forced Bounce!</color>");
#endif
                        Bounce(new Vector2(0f, 1f), hitable, contact);
                    }
                    else
                    {
                        Debug.LogError("Cannot Get Hitable from last contact info!");
                    }

                }
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                contact = collision.GetContact(0);

#if _DEBUG_Char
                Debug.Log("<color=green>Contact.normal = " + contact.normal.x.ToString("F5") + "  ,  " + contact.normal.y.ToString("F5") + " , Timestamp = " + Time.time + "</color>");
#endif

                if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                {
                    currentBlock = contact.collider.gameObject.GetComponent<BlockBase>();
                }
                else
                {
                    if (contact.collider.transform.parent.parent.gameObject.GetComponent<BlockBase>())
                    {
                        currentBlock = contact.collider.transform.GetComponentInParent<BlockBase>();
                    }
                }

                TrapBase trap = null;
                if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                {
                    trap = contact.collider.gameObject.GetComponent<TrapBase>();
                }
                else
                {
                    if (contact.collider.transform.parent.parent.gameObject.GetComponent<TrapBase>())
                    {
                        trap = contact.collider.transform.GetComponentInParent<TrapBase>();
                    }
                }
                    

                IHitable hitable = null;

                if (currentBlock != null)
                {
#if LAST_HIT_DEBUG
                    Debug.Log("<color=darkblue>Last hit is a block. Name = " + currentBlock.gameObject.name + "</color>");
#endif

                    if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                    {
                        hitable = contact.collider.transform.GetInterface<IHitable>();
                    }
                    else
                    {
                        hitable = contact.collider.transform.parent.parent.GetInterface<IHitable>();
                    }

                    if (hitable != null)
                    {
                        lastHitBlock = currentBlock;
                        lastHitBlockY = lastHitBlock.GetY();

                        if (contact.normal.x >= -0.1f && contact.normal.x <= 0.1f)
                        {
                            if (contact.normal.y >= 0.9f)
                            {
#if _DEBUG_Char
                                Debug.Log("<color=orange>Normal Full Bounce!</color>");
#endif
                                lastBouncedBlock = currentBlock;
                                lastBouncedBlockY = lastBouncedBlock.GetY();

                                Bounce(contact.normal, hitable, contact);
                            }
                        }
                        else
                        {
                            NoBounceOnLastCollision = true;
                        }

                        // When block hp = 0, the collider will be disabled and Char OnCollisionExit2D cannot be triggered, hence current Block will not be cleared even Char is not touching the block.
                        // The lines below is used to deal with the issue as mentioned above
                        if (!currentBlock.GetIfAlive())
                        {
                            currentBlock = null;
                        }
                        //--------------------------------------------------------------------------------------------------
                    }
                    else
                    {
                        Debug.LogError("Cannot Get IHitable on last hit object!");
                    }
                }
                else if (trap != null)
                {
                    Debug.Log("<color=darkblue>Last hit is a trap. Name = " + trap.gameObject.name + "</color>");

                    BulletBase bullet = null;

                    if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                    {
                        bullet = contact.collider.gameObject.GetComponent<BulletBase>();
                    }
                    else
                    {
                        if (contact.collider.transform.parent.parent.gameObject.GetComponent<BulletBase>())
                        {
                            bullet = contact.collider.transform.parent.parent.gameObject.GetComponent<BulletBase>();
                        }
                    }

                    if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                    {
                        hitable = contact.collider.transform.GetInterface<IHitable>();
                    }
                    else
                    {
                        hitable = contact.collider.transform.parent.parent.GetInterface<IHitable>();
                    }

                    if (hitable != null)
                    {
                        if (bullet == null)
                        {
                            // Case for simple trap
                            if (trap.GetKillChar())
                            {
                                if (alive && !invulnerable)
                                {
#if _DEBUG_Char
                                Debug.Log("Kill Char");
#endif
                                    if (bubblyShield.activeInHierarchy)
                                    {
                                        BubblyShieldBurst();
                                        if (contact.normal.x >= -0.1f && contact.normal.x <= 0.1f)
                                        {
                                            if (contact.normal.y >= 0.9f)
                                            {
#if _DEBUG_Char
                                        Debug.Log("<color=orange>Bounce from stepping trap!</color>");
#endif
                                                Bounce(contact.normal, hitable, contact);
                                            }

                                        }
                                        else
                                        {
                                            NoBounceOnLastCollision = true;
                                        }
                                    }
                                    else
                                    {
                                        CharDie();
                                    }
                                }
                            }
                            else
                            {
                                if (contact.normal.x >= -0.1f && contact.normal.x <= 0.1f)
                                {
                                    if (contact.normal.y >= 0.9f)
                                    {
#if _DEBUG_Char
                                        Debug.Log("<color=orange>Bounce from stepping trap!</color>");
#endif
                                        Bounce(contact.normal, hitable, contact);
                                    }

                                }
                                else
                                {
                                    NoBounceOnLastCollision = true;
                                }
                            }
                        }
                        else
                        {
                            // Case for bullet

                            BounceDataClass data = bullet.DetermineIfCanStepOn(contact.normal);

                            if (data.bounce)
                            {
#if _DEBUG_Char
                                Debug.Log("<color=orange>Bounce from bullet!</color>");
#endif
                                Bounce(new Vector2(0f, 1f), hitable, contact);
                            }
                            else if (data.neutral)
                            {
                                Debug.Log("Neutral");
                                NoBounceOnLastCollision = true;
                            }
                            else if (data.die)
                            {
                                if (alive)
                                {
#if _DEBUG_Char
                                Debug.Log("Kill Char");
#endif
                                    if (bubblyShield.activeInHierarchy)
                                    {
                                        BubblyShieldBurst();
                                    }
                                    else
                                    {
                                        CharDie();
                                    }
                                    
                                }
                            }

                            /*if (contact.normal.x >= -0.1f && contact.normal.x <= 0.1f && contact.normal.y >= 0.9f)
                            {
                                Bounce(contact.normal, hitable);
                            }
                            else if (bullet.GetKillChar())
                            {
                                if (alive)
                                {
#if _DEBUG_Char
                                Debug.Log("Kill Char");
#endif
                                    CharDie();
                                }
                            }
                            else
                            {
                                NoBounceOnLastCollision = true;
                            }*/
                        }
                    }
                    else
                    {
                        Debug.LogError("Cannot Get IHitable on last hit object!");
                    }
                }
                else
                {
                    Debug.LogError("Last hit is NOT a block nor a trap! What was that???");
                }
            }

            private void OnCollisionExit2D(Collision2D collision)
            {
                IHitable hitable = null;
                if (contact.collider.gameObject.CompareTag("GetHitableFromThis"))
                {
                    hitable = contact.collider.transform.GetInterface<IHitable>();
                }
                else
                {
                    hitable = contact.collider.transform.parent.parent.GetInterface<IHitable>();
                }

                if (hitable != null)
                {
                    currentBlock = null;
                }
            }

            private void OnTriggerEnter2D(Collider2D col)
            {
                if (col.gameObject.CompareTag("EndPoint"))
                {
                    if (charPassLevelAct != null)
                    {
                        EndPoint endPoint = col.gameObject.GetComponent<EndPoint>();
                        charPassLevelAct(endPoint);
                    }
                }

                Food food = col.gameObject.GetComponentInParent<Food>();
                if (food)
                {
                    food.Eatten();
                    eatFoodAct();
                    EatFoodAnim();
                }
            }

            void EatFoodAnim()
            {
                anim.ResetTrigger("eat");
                anim.SetTrigger("eat");

                if (!isBoosting && !isDashing)
                {
                    eatPSEffect.Stop(true);
                    eatPSEffect.Play(true);
                }
            }

            void Bounce(Vector2 bounceVector, IHitable hitableObj, ContactPoint2D? _contact)
            {
                rd.velocity = Vector2.zero;
                rd.simulated = false;
                rd.simulated = true;
                rd.velocity = Vector2.zero;

                rd.AddForce(bounceVector * basicBounceForce * smoothingForce, ForceMode2D.Impulse);

                hitableObj.HitEffect();
                NoBounceOnLastCollision = false;

                anim.SetTrigger("bounce");
                if (_contact.HasValue)
                {
                    GameGM.instance.CreateStepOnStarExplodeEffect((Vector3)_contact.Value.point);
                }
                
            }

            void SetupParticleSystemList()
            {
                psList = new List<ParticleSystem>();

                ParticleSystem[] psArray = psParent.GetComponentsInChildren<ParticleSystem>(true);

                for (int i = 0; i < psArray.Length; i++)
                {
                    psList.Add(psArray[i]);
                }
            }

            public void CharRestart()
            {
                _transform.position = charStartPos;
                ResetRigidbody();
                ResetAllPowerUps();
                BoostReset();
                ResetVar();
                ActivateChar(true);
            }

            public void CharRevive()
            {
                // Generate the pos right above the last Bounced Block
                float spawnY = lastBouncedBlock.GetY() + (Mathf.Abs(_transform.position.y - charOriginPoint.position.y));
                float spawnX = lastBouncedBlock.GetMinX() + (col.bounds.size.x / 2f);

                _transform.position = new Vector2(spawnX, spawnY);
                ResetRigidbody();
                //ResetAllPowerUps();
                ResetVar();
                ActivateChar(true);
                InvulnerableStart(3f);
            }

            void ActivateChar(bool activate)
            {
                isBoosting = false;
                isDashing = false;

                charSpriteObj.SetActive(activate);
                rd.gravityScale = charGravity;
                rd.simulated = activate;
                alive = activate;

                // Reset the body and emoji animation
                if (activate)
                {
                    anim.Play("Action Layer.Char_BounceNormal", 2, 1f);
                    anim.Play("Emoji Layer.CharEmoji_Normal", 1, 1f);
                }
            }

            void ResetEffects()
            {
                if (runEffectGO.activeInHierarchy)
                {
                    runEffectGO.gameObject.SetActive(false);
                }

                if (dashPSEffect.isPlaying || dashPSEffect.isEmitting)
                {
                    dashPSEffect.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                }

                if (eatPSEffect.isPlaying || eatPSEffect.isEmitting)
                {
                    eatPSEffect.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                }
            }

            void ResetRigidbody()
            {
                rd.velocity = Vector2.zero;
                rd.angularVelocity = 0f;
                _transform.rotation = Quaternion.Euler(Vector3.zero);
            }

            void ResetVar()
            {
                currentBlock = null;
                lastHitBlock = null;
                lastHitBlockY = null;
                lastBouncedBlock = null;
                lastBouncedBlockY = null;
                contact = new ContactPoint2D();
                NoBounceOnLastCollision = false;
                invulnerableAccTime = 0f;
                invulnerableTotalTime = 0f;
            }

            void CheckShouldCharDestroy()
            {
                if (alive && GameGM.instance.GetGameState() == IEnum.GameState.playing)
                {
                    float? lowestBlockY = GameGM.instance.GetCurrentLevelLowestBlockY();
                    float rangeDiff;

                    if (lowestBlockY.HasValue)
                    {
                        rangeDiff = Mathf.Abs(_transform.position.y - lowestBlockY.Value);

                        if (!falling && _transform.position.y <= lowestBlockY.Value)
                        {
                            falling = true;
                        }

                        if (GameGM.instance.GetIsCamFollow() && rangeDiff >= (deathYRange - 20f) && _transform.position.y <= lowestBlockY.Value)
                        {
                            stopCamFollowAct();
                        }

                        if (rangeDiff >= deathYRange && _transform.position.y <= lowestBlockY.Value)
                        {
                            alive = false;
                            falling = false;
                            charAfterDeathAct();
                        }
                    }
                    else if (lastHitBlockY != null)
                    {
                        rangeDiff = Mathf.Abs(_transform.position.y - lastHitBlockY.Value);

                        if (rangeDiff >= deathYRange)
                        {
                            alive = false;
                            falling = false;
                            charAfterDeathAct();
                        }
                    }
                    else
                    {
                        Debug.LogError("Cannot get lowestBlockY nor lastHitBlockY!!");
                        rangeDiff = Mathf.Abs(_transform.position.y);

                        if (rangeDiff >= deathYRange && _transform.position.y <= 0f)
                        {
                            alive = false;
                            falling = false;
                            charAfterDeathAct();
                        }
                    }
                }
            }
            
            /// <summary>
            /// Force Bounce if the Char is not Bounced after 0.5s & standing on top of last bounced block
            /// (deal with the issue when a up-down moving block pressed the Char down on a forward moving block, then the latest collision is on the head but no further collision on the foot)
            /// </summary>
            void ForceBounceIfNotBouncingByCheckingTime()
            {
                if (alive && GameGM.instance.GetGameState() == IEnum.GameState.playing && rd.velocity.x == 0f && rd.velocity.y == 0f)
                {
                    if (isDashing)
                    {
                        // No need to count if the Char is dashing
                        forceBounceCounter = 0f;
                    }
                    else
                    {
                        forceBounceCounter += Time.deltaTime;
                    }

                    if (forceBounceCounter >= 0.5f)
                    {
                        if(lastBouncedBlock != null)
                        {
                            BoxCollider2D lastBouncedBoxCol = lastBouncedBlock.GetCol();

                            if (lastBouncedBoxCol != null)
                            {
                                if (lastBouncedBlockY < _transform.position.y && col.IsTouching(lastBouncedBoxCol))
                                {
                                    IHitable lastBouncedBlockHitable = lastBouncedBoxCol.transform.parent.parent.GetInterface<IHitable>();
                                    if (lastBouncedBlockHitable != null)
                                    {
#if _DEBUG_Char
                                        Debug.Log("<color=orange>Forced Bounce!</color>");
#endif
                                        Bounce(new Vector2(0f, 1f), lastBouncedBlockHitable, null);
                                    }
                                    else
                                    {
                                        Debug.LogError("Cannot Get Hitable from Last Bounced Block!");
                                    }
                                }
                                else
                                {
                                    Debug.LogError("Char is not touching last bounced block. Cannot Force Bounce!");
                                }
                            }
                            else
                            {
                                Debug.LogError("Cannot Get Last Bounce Block Col. Cannot Force Bounce!");
                            }
                        }
                        else
                        {
                            Debug.LogError("No Last Bounce Block is found. Cannot Force Bounce!");
                        }

                        forceBounceCounter = 0f;
                    }
                }
            }

            public void DashCommand()
            {
                if (!isDashing && !isBoosting && alive && GameGM.instance.DetermineIfCanDash())
                {
                    StartDashing();
                }
            }

            void StartDashing()
            {
                ResetEffects();

                isDashing = true;
                rd.velocity = Vector2.zero;
                rd.gravityScale = 0f;
                charStartDashAct();

                dashAccTime = 0f;
                StartCoroutine("DashTimer");
                anim.SetTrigger("dash");

                dashPSEffect.Stop(true);
                dashPSEffect.Play(true);
                
                if (!falling)
                {
                    GameGM.instance.UpdateCoinBar(_dash: true);
                }
                
            }

            void EndDash()
            {
#if _DEBUG_Char
                Debug.Log("End Dash");
#endif
                rd.gravityScale = charGravity;
                charEndDashAct();

                anim.ResetTrigger("dash");

                // This avoid the 'eat' trigger to be on-hold and pending for consumption during 'eatting food while running'
                anim.ResetTrigger("eat");
                //-----------------------------------

                isDashing = false;
            }

            IEnumerator DashTimer()
            {
                float startTime, endTime;

                yield return null;
                anim.ResetTrigger("dash");

                while (dashAccTime < dashTotalTime && GameGM.instance.GetGameState() != IEnum.GameState.none)
                {
                    if (GameGM.instance.GetGameState() == IEnum.GameState.playing)
                    {
                        startTime = Time.time;
                        yield return null;
                        endTime = Time.time;

                        dashAccTime = dashAccTime + (endTime - startTime);

                        if (dashAccTime >= dashTotalTime)
                        {
                            EndDash();
                        }
                    }
                    else if (GameGM.instance.GetGameState() == IEnum.GameState.pause)
                    {
                        yield return new WaitForSecondsRealtime(0.02f);
                    }
                    else if (GameGM.instance.GetGameState() == IEnum.GameState.gameOver)
                    {
                        dashAccTime = dashTotalTime;
                        EndDash();
                        break;
                    }
                }
            }

            void CheckDashing()
            {
                if (isDashing)
                {
                    _transform.position += new Vector3(20f * Time.deltaTime, 0f, 0f);
                }
            }

            public void CharDestroy()
            {
                alive = false;
                ActivateChar(false);
            }

            void CharDie()
            {
                alive = false;

                isBoosting = false;
                isDashing = false;
                falling = false;
                UpdateBoostMeter(boostMeter);

                rd.gravityScale = charGravity;
                rd.simulated = false;

                allowInputAct(false);
                StartCoroutine(_CharDie());
            }

            IEnumerator _CharDie()
            {
                AppGM.instance.PlayStoppableSFX(IEnum.AudioList.dieFromTrap, 5f);
                yield return null;
                anim.SetTrigger("die");
                yield return new WaitForSeconds(22f / 24f);
                diePSEffect.Play(true);
                yield return new WaitForSeconds(21f / 24f);
                allowInputAct(true);
                charAfterDeathAct();
            }

            [ContextMenu("Start Invulerable")]
            void InvulnerableStart()
            {
                invulnerable = true;
                invulnerableAccTime = 0f;
                invulnerableTotalTime = 3f;

                StartCoroutine(InvulnerableTimer());
            }

            void InvulnerableStart(float timeLimit)
            {
                invulnerable = true;
                invulnerableAct(true);
                invulnerableAccTime = 0f;
                invulnerableTotalTime = timeLimit;
                anim.SetBool("invulnerable", true);

                StartCoroutine(InvulnerableTimer());
            }

            void InvulnerableEnd()
            {
#if _DEBUG_Char
                Debug.Log("End Invulnerable");
#endif
                invulnerable = false;
                invulnerableAct(false);
                anim.SetBool("invulnerable", false);
            }

            IEnumerator InvulnerableTimer()
            {
                float startTime, endTime;

                while (invulnerableAccTime < invulnerableTotalTime && GameGM.instance.GetGameState() != IEnum.GameState.none)
                {
                    if (GameGM.instance.GetGameState() == IEnum.GameState.playing)
                    {
                        startTime = Time.time;
                        yield return null;
                        endTime = Time.time;

                        invulnerableAccTime = invulnerableAccTime + (endTime - startTime);

                        if (invulnerableAccTime >= invulnerableTotalTime)
                        {
                            InvulnerableEnd();
                        }
                    }
                    else if (GameGM.instance.GetGameState() == IEnum.GameState.pause)
                    {
                        yield return new WaitForSecondsRealtime(0.02f);
                    }
                    else if (GameGM.instance.GetGameState() == IEnum.GameState.gameOver)
                    {
                        invulnerableAccTime = invulnerableTotalTime;
                        InvulnerableEnd();
                        break;
                    }
                }
            }

            #region Boost Related
            void Boost(float finalSpeed)
            {
                if (alive)
                {
                    if (!isBoosting)
                    {
                        // These are transitional code, which only run once when the state of Char is from 'non-boosting' to 'boosting'
                        isBoosting = true;
                        BoostAnim(true);
                    }
                    _transform.Translate(Vector3.right * finalSpeed, Space.World);
                    if (!falling)
                    {
                        GameGM.instance.UpdateCoinBar(_speed: finalSpeed);
                    }
                    UpdateBoostMeter(boostMeter - (GameGM.instance.GetBoostEnergyCost() * Time.deltaTime));
                }
            }

            void BoostAnim(bool flag)
            {
                if (flag)
                {
                    ResetEffects();
                    charStartBoostAct();
                }
                else
                {
                    charStopBoostAct();
                }

                anim.SetBool("boost", flag);
                runEffectGO.SetActive(flag);

                // This avoid the 'eat' trigger to be on-hold and pending for consumption during 'eatting food while running'
                if (!flag)
                {
                    anim.ResetTrigger("eat");
                }
                //------------------------
            }

            void BoostReset()
            {
                isBoosting = false;
                UpdateBoostMeter(1f);
            }

            void AccumulateBoostMeter(float inputPulse)
            {
                float rate = inputPulse * boostMeterStackRate * Time.deltaTime;
                UpdateBoostMeter(boostMeter + rate);
            }

            void UpdateBoostMeter(float newValue)
            {
                boostMeter = Mathf.Clamp01(newValue);
                GameGM.instance.gameUI.UpdateBoostMeter(boostMeter);
            }
            #endregion

            #region Power Ups
            void ApplyStartGamePowerUp()
            {
                bubblyShield.SetActive(GameGM.instance.CheckIfPowerUpActive(IEnum.PowerUp.bubblyShield));
            }

            void BubblyShieldBurst()
            {
                bubblyShield.SetActive(false);
                AppGM.instance.PlayStoppableSFX(IEnum.AudioList.eatPop, _pitch: 1f);
                InvulnerableStart(1.5f);
            }

            void ResetAllPowerUps()
            {
                bubblyShield.SetActive(false);
            }
            #endregion

            public void PauseGame(bool _pause)
            {
                PauseRigidBody(_pause);
                PauseAnimController(_pause);
                PauseParticleSystems(_pause);
            }

            void PauseRigidBody(bool pause)
            {
                rd.simulated = !pause;
            }

            void PauseAnimController(bool pause)
            {
                anim.speed = pause? 0f : 1f;
            }

            void PauseParticleSystems(bool pause)
            {
                if (pause)
                {
                    for (int i = 0; i < psList.Count; i++)
                    {
                        var main = psList[i].main;
                        main.simulationSpeed = 0f;
                    }
                }
                else
                {
                    for (int i = 0; i < psList.Count; i++)
                    {
                        var main = psList[i].main;
                        main.simulationSpeed = 1f;
                    }
                }
                
            }

#region Getter
            public float GetHighestY()
            {
                return charHighestY;
            }

            public BlockCreator GetBlockCreator()
            {
                return blockCreator;
            }

            public Transform GetBlockCreatorTransform()
            {
                return blockCreator.gameObject.transform;
            }

            public Vector3 GetPos()
            {
                return _transform.position;
            }

            public bool IsCharBoosting()
            {
                return isBoosting;
            }

            BlockBase GetLastHitBlock()
            {
                return lastHitBlock;
            }

            public BlockBase GetLastBouncedBlock()
            {
                return lastBouncedBlock;
            }

            public bool IsFalling()
            {
                return falling;
            }
            #endregion

            #region Test Code
            [ContextMenu("ImmediatelyFallKillChar")]
            void ImmediatelyFallKillChar()
            {
                alive = false;
                charAfterDeathAct();
            }
            #endregion
        }

    }
}