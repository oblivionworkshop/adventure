using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class Holder : TrapBase
        {
            [Header("Holder")]
            [SerializeField]
            Animator holderAnim;
            [SerializeField]
            Sprite holderStaticSprite;
            [SerializeField]
            Sprite holderNormalSprite;
            [SerializeField]
            List<Sprite> holderFragmentList;
            [SerializeField]
            List<Rigidbody2D> holderFragmentRDList;
            List<int> holderFragmentRDIndexList = new List<int>();

            UnityAction<int> setRdListAct;

            [Header("Rail Config")]
            [SerializeField]
            HolderRail rail;
            [SerializeField]
            float moveSpeed = 0.5f;
            [SerializeField]
            bool startAtRailEnd = false;
            bool backward = false;
            Vector3 movingVector;

            protected override void Awake()
            {
                base.Awake();
            }

            protected override void Start()
            {
                base.Start();
                base.dieAct = HolderDie;

                if (base.invulnerable)
                {
                    for (int i = 0; i < spRendererList.Count; i++)
                    {
                        spRendererList[i].enabled = true;
                        spRendererList[i].sprite = holderStaticSprite;
                    }
                }else if (base.startingHitPoint > 1)
                {
                    for (int i = 0; i < spRendererList.Count; i++)
                    {
                        spRendererList[i].enabled = true;
                        spRendererList[i].sprite = holderNormalSprite;
                        base.fadeAfterHit = true;
                    }
                }
                else
                {
                    setRdListAct = SetFragmentRDListIndex;

                    for (int i = 0; i < spRendererList.Count; i++)
                    {
                        spRendererList[i].enabled = false;
                    }
                    
                    for (int i = 0; i < holderFragmentRDList.Count; i++)
                    {
                        GameGM.instance.RegRDIntoDict(holderFragmentRDList[i], setRdListAct);
                    }

                    holderAnim.gameObject.SetActive(true);
                }

                if (rail.gameObject.activeInHierarchy)
                {
                    _transform.position = startAtRailEnd? rail.GetEndPos() : _transform.position;
                    backward = startAtRailEnd;

                    movingVector = rail.GetEndPos() - rail.GetStartPos();
                }
            }

            protected override void Update()
            {
                if (!stopUpdate)
                {
                    // Handles the ping-pong movement along the rail
                    if (rail != null && rail.gameObject.activeInHierarchy && IsPlayingAndAlive())
                    {
                        if (!backward)
                        {
                            _transform.position = Vector2.MoveTowards(_transform.position, rail.GetEndPos(), moveSpeed * Time.deltaTime);

                            if (Vector2.Distance(_transform.position, rail.GetEndPos()) == 0f)
                            {
                                backward = !backward;
                            }
                        }
                        else
                        {
                            _transform.position = Vector2.MoveTowards(_transform.position, rail.GetStartPos(), moveSpeed * Time.deltaTime);

                            if (Vector2.Distance(_transform.position, rail.GetStartPos()) == 0f)
                            {
                                backward = !backward;
                            }
                        }
                    }
                    //------------------------------------------------

                    if (died)
                    {
                        if (base.rotateAfterDeath)
                        {
                            if (base.deathRotateTarget == null)
                            {
                                _transform.Rotate(base.deathRotateVector * Time.deltaTime);
                            }
                            else
                            {
                                base.deathRotateTarget.Rotate(base.deathRotateVector * Time.deltaTime);
                            }

                            if (holderAnim.gameObject.activeInHierarchy)
                            {
                                for (int i = 0; i < holderFragmentRDList.Count; i++)
                                {
                                    holderFragmentRDList[i].transform.Rotate(base.deathRotateVector * Time.deltaTime);
                                }
                            }
                        }

                        CheckShouldHolderDestroy();
                    }
                }
            }

           void HolderDie()
           {
                if (holderAnim.gameObject.activeInHierarchy)
                {
                    // This is the case for 1-time Holder
                    for (int i = 0; i < holderFragmentRDList.Count; i++)
                    {
                        holderFragmentRDList[i].bodyType = RigidbodyType2D.Dynamic;
                        holderFragmentRDList[i].gravityScale = Random.Range(0.8f, 2f);
                    }
                }

                // if contains holder rail
                if (rail.gameObject.activeInHierarchy)
                {
                    rail.Fall();
                }
                //----------------------------------------
           }

            void CheckShouldHolderDestroy()
            {
                if (died && !stopUpdate)
                {
                    float rangeDiff = Mathf.Abs(holderFragmentRDList[0].transform.position.y - diedPosY);
                    if (rangeDiff >= deathYRange)
                    {
                        stopUpdate = true;

                        if (deathDestroyTarget != null)
                        {
                            deathDestroyTarget.Kill();
                        }
                        else
                        {
                            DestroyThis();
                        }
                    }
                }
            }

            protected override void OnDestroy()
            {
                // If this trap will kill itself when falling, need to notify GameGM RD Dict & Trap Top Parent
                if (rd && rdReg)
                {
                    TrapTopParent trapParent = GetComponentInParent<GamePage.TrapTopParent>();
                    Debug.Log("<color=green>Holder OnDestroy. GameObejct name = " + gameObject.name + "</color>");
                    if (trapParent)
                    {
                        Debug.Log("<color=yellow>Have Trap Parent</color>");
                        trapParent.RemoveTrapIndexFromList(rdIndex);

                        for (int i = 0; i < holderFragmentRDIndexList.Count; i++)
                        {
                            trapParent.RemoveTrapIndexFromList(holderFragmentRDIndexList[i]);
                        }
                    }

                    GameGM.instance.RemoveRDFromDict(rdIndex);
                    for (int i = 0; i < holderFragmentRDIndexList.Count; i++)
                    {
                        GameGM.instance.RemoveRDFromDict(holderFragmentRDIndexList[i]);
                    }

                    // Must be put at last
                    rdReg = false;
                }
            }

            public void SetFragmentRDListIndex(int _index)
            {
                holderFragmentRDIndexList.Add(_index);

                base.AddRDIndexToParentList(_index);
            }
        }
    }
}
