using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class StingCircle : TrapBase
        {
            [Header("Sting Circle")]
            [SerializeField]
            Transform holderSprite;
            [SerializeField]
            bool clockwise = true;
            [SerializeField]
            float speedPerSecond = 10f;

            Vector3 rotationVector;

            protected override void Start()
            {
                base.Start();
                rotationVector = clockwise ? new Vector3(0f, 0f, speedPerSecond * Time.deltaTime * -1f) : new Vector3(0f, 0f, speedPerSecond * Time.deltaTime);
            }

            protected override void Update()
            {
                if (IsPlayingAndAlive())
                {
                    holderSprite.Rotate(rotationVector);
                }
            }
        }
    }
}
