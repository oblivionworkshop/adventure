using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class Nail : TrapBase
        {
            protected override void Start()
            {
                base.Start();
                if (rd)
                {
                    rd.gravityScale = Random.Range(1f, 3f);
                }
            }
        }
    }
}
