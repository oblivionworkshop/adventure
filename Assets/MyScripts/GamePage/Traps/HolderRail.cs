using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class HolderRail : TrapBase
        {
            [Header("Holder Rail")]
            [SerializeField]
            Transform railStartPoint;
            [SerializeField]
            Transform railEndPoint;

            public Vector3 GetStartPos()
            {
                return railStartPoint.position;
            }

            public Vector3 GetEndPos()
            {
                return railEndPoint.position;
            }

        }
    }
}
