using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class RotatingKnife : TrapBase
        {
            [Header("Rotating Knife")]
            [SerializeField]
            Transform knifeParent;
            [SerializeField]
            bool clockwise = true;
            [SerializeField]
            float speedPerSecond;
            [Range(0f,360f)]
            [SerializeField]
            float startingAngle = 0f;

            Vector3 rotationVector;

            protected override void Start()
            {
                base.Start();
                knifeParent.rotation = Quaternion.Euler(new Vector3(0f, 0f, startingAngle));
                rotationVector = clockwise ? new Vector3(0f, 0f, speedPerSecond * Time.deltaTime * -1f) : new Vector3(0f, 0f, speedPerSecond * Time.deltaTime);
            }

            protected override void Update()
            {
                base.Update();
                if (IsPlayingAndAlive())
                {
                    knifeParent.Rotate(rotationVector);
                }
            }
        }
    }
}
