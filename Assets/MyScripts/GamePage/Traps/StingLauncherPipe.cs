using Beebyte.Obfuscator;
using UnityEngine;
using UnityEngine.Events;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class StingLauncherPipe : TrapBase
        {
            [Header("Sting Launcher Pipe")]
            [SerializeField]
            Transform spawnPoint;

            Animator anim;
            IEnum.BulletType bullet;
            float bulletSpeed;

            int animIndex = -1;
            UnityAction<int> setAnimIndexAct;
            public UnityAction afterShootAct;

            protected override void Awake()
            {
                base.Awake();

                anim = gameObject.GetComponent<Animator>();
                if (anim)
                {
                    setAnimIndexAct = SetAnimIndex;
                }
                else
                {
                    Debug.LogError("Cannot find Animator from " + gameObject.name + " !");
                }
            }

            protected override void Start()
            {
                base.Start();

                if (anim)
                {
                    // Reg anim to GameGM pause list
                    GameGM.instance.RegAnimIntoPauseList(anim, setAnimIndexAct);
                    //------------------------------------
                }
            }

            public void BasicSetup(IEnum.BulletType _type, float _speed)
            {
                bullet = _type;
                bulletSpeed = _speed;
            }

            public void Shoot()
            {
                if (!base.died)
                {
                    anim.SetTrigger("shoot");
                }
            }

            [SkipRename]
            void AfterShoot()
            {
                if (!base.died)
                {
                    StingBullet sting = GameGM.instance.GetPoolManager().GetBullet(bullet).GetComponent<StingBullet>();
                    sting.BasicSetup(bulletSpeed);
                    sting._transform.position = spawnPoint.position;
                    sting._transform.rotation = _transform.rotation;

                    sting.gameObject.SetActive(true);

                    if (afterShootAct != null)
                    {
                        afterShootAct();
                    }
                }
            }

            void SetAnimIndex(int _index)
            {
                animIndex = _index;
            }

            protected override void DestroyThis()
            {
                // Need to Remove it from GameGM Pause Anim List
                GameGM.instance.RemoveAnimFromPauseList(animIndex);
                //-----------------------------------------

                base.DestroyThis();
            }
        }
    }
}
