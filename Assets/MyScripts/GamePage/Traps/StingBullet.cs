using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class StingBullet : BulletBase
        {
            protected override void Awake()
            {
                base.Awake();
                base.bulletExpireAct = StingBulletExpire;
            }

            void StingBulletExpire(bool _willDestroy)
            {
                GameGM.instance.GetPoolManager().ReturnStingBullet(id, _willDestroy);
            }
        }
    }
}
