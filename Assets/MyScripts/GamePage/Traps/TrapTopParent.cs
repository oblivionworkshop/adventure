using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class TrapTopParent : MonoBehaviour
        {
            Transform _transform;

            List<int> wholeTrapRDIndexList = new List<int>();        // contains all traps' rd in this trap
            

            private void Awake()
            {
                _transform = transform;
                SetLayer();
            }

            public void AddTrapIndexToList(int _index)
            {
                wholeTrapRDIndexList.Add(_index);
            }

            public void RemoveTrapIndexFromList(int _index)
            {
                Debug.Log("<color=green> RemoveTrapIndexFromList , num = " + _index + "</color>");
                wholeTrapRDIndexList.Remove(_index);
            }

            public void Kill()
            {
#if _DEBUG_Trap
                Debug.Log("Trap Top Parent.Kill()");
#endif

                int counter = _transform.childCount;
#if _DEBUG_Trap
                Debug.Log("counter = " + counter);
#endif

                for (int i = 0; i < counter; i++)
                {
                    Destroy(_transform.GetChild(i).gameObject);

                    #if _DEBUG_Trap
                    Debug.Log("<color=orange>destroy child " + i + "</color>");
#endif

                    if (i == counter - 1)
                    {
                        DestroyThis();
                    }
                }
            }

            private void OnDestroy()
            {
                #if _DEBUG_Trap
                Debug.Log("Trap Top Parent.On Destroy()");
#endif
                
            }

            void DestroyThis()
            {
                Destroy(this.gameObject);
            }

            void SetLayer()
            {
                if (gameObject.layer != LayerMask.NameToLayer("Trap"))
                {
                    foreach (Collider2D col in gameObject.GetComponentsInChildren<Collider2D>(true))
                    {
                        col.gameObject.layer = LayerMask.NameToLayer("Trap");
                    }
                }
            }
        }
    }
	
}
