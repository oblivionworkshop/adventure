using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop{
    namespace GamePage
    {
        public class StingLauncher : TrapBase
        {
            [Header("Sting Launcher")]
            [SerializeField]
            Transform pipeParent;
            [SerializeField]
            float bulletSpeed;
            [SerializeField]
            float shootInterval = 4f;

            [Space(10f)]
            [SerializeField]
            bool rotateAfterShooting;
            [SerializeField]
            bool clockwise = true;
            [SerializeField]
            float rotateAngle;

            float shootTimer;
            float lastShootTime;
            float afterShootTime;       // Handle the rotation after shoot
            bool needRotate;
            bool rotating = false;
            IEnum.BulletType bulletType = IEnum.BulletType.sting;

            StingLauncherPipe pipeRepresentative;

            Quaternion startRotation;

            List<StingLauncherPipe> activePipes = new List<StingLauncherPipe>();
            

            protected override void Awake()
            {
                base.Awake();

                startRotation = _transform.rotation;

                for (int i = 0; i < pipeParent.childCount; i++)
                {
                    GameObject child = pipeParent.GetChild(i).gameObject;

                    if (child.activeInHierarchy)
                    {
                        StingLauncherPipe pipe = child.GetComponent<StingLauncherPipe>();
                        if (pipe)
                        {
                            if (pipeRepresentative == null)
                            {
                                // Set the first picked active pipe as representative
                                pipeRepresentative = pipe;
                            }

                            pipe.BasicSetup(bulletType, bulletSpeed);

                            activePipes.Add(pipe);
                        }
                    }
                }
            }

            protected override void Update()
            {
                base.Update();

                if (!base.stopUpdate && pipeRepresentative.IsPlayingAndAlive())
                {
                    shootTimer += Time.deltaTime;

                    if (shootTimer >= shootInterval && !rotating && !needRotate)
                    {
                        shootTimer = 0f;
                        lastShootTime = Time.time;

                        for (int i = 0; i < activePipes.Count; i++)
                        {
                            if (i == 0)
                            {
                                activePipes[i].afterShootAct = AfterShoot;
                            }

                            activePipes[i].Shoot();
                        }

                        if (rotateAfterShooting)
                        {
                            needRotate = true;
                        }
                    }
                }
            }

            void AfterShoot()
            {
                afterShootTime = Time.time;
                StartCoroutine(_AfterShoot());
            }

            IEnumerator _AfterShoot()
            {
                float targetDelay = shootInterval / 8f;
                float counter = 0f;
                while (needRotate && counter < targetDelay)
                {
                    if (!base.stopUpdate && base.IsPlayingAndAlive())
                    {
                        counter += Time.deltaTime;
                        yield return new WaitForSeconds(Time.deltaTime);
                    }
                    else
                    {
                        yield return null;
                    }
                }
                
                needRotate = false;
                StartCoroutine(_RotateLauncher());
            }

            IEnumerator _RotateLauncher()
            {
                float finalAngle = clockwise? _transform.eulerAngles.z - rotateAngle : _transform.eulerAngles.z + rotateAngle;
                float totalRotationTime = shootInterval / 2f;
                int rotateNum = 20;
                float rotateSector = clockwise? -rotateAngle / (float)rotateNum : rotateAngle / (float)rotateNum;
                Vector3 rotateVec = new Vector3(0f, 0f, rotateSector);

                int counter = 0;
                while (counter < rotateNum)
                {
                    if (!base.stopUpdate && pipeRepresentative.IsPlayingAndAlive())
                    {
                        if (!clockwise)
                        {
                            if (_transform.eulerAngles.z + rotateSector >= finalAngle)
                            {
                                _transform.rotation = Quaternion.Euler(0f, 0f, finalAngle);
                            }
                            else
                            {
                                _transform.Rotate(rotateVec, Space.World);
                            }
                        }
                        else
                        {
                            if (_transform.eulerAngles.z + rotateSector <= finalAngle)
                            {
                                _transform.rotation = Quaternion.Euler(0f, 0f, finalAngle);
                            }
                            else
                            {
                                _transform.Rotate(rotateVec, Space.World);
                            }
                        }

                        rotating = true;
                        counter++;
                        yield return new WaitForSeconds(totalRotationTime / (float)rotateNum);
                    }
                    else
                    {
                        yield return null;
                    }

                    if (counter >= rotateNum)
                    {
                        rotating = false;
                    }
                }
            }

            protected override void DestroyThis()
            {
                // Need to Stop _RotateLauncher() Coroutine before Destroy
                StopAllCoroutines();
                //-----------------------------------------

                base.DestroyThis();
            }

            void Init()
            {
                StopAllCoroutines();

                needRotate = false;
                rotating = false;
                _transform.rotation = startRotation;

                shootTimer = 0f;
                lastShootTime = 0f;
                afterShootTime = 0f;
            }
        }
    }
}
