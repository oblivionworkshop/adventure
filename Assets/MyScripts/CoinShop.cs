using Com.OblivionWorkshop.StartPage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    public class CoinShop : MonoBehaviour
    {
        PopupAnim popUpAnim;
        [SerializeField]
        PopupCloseButton closeBtn;

        private void Awake()
        {
            popUpAnim = GetComponent<PopupAnim>();
        }



        #region Getter
        public PopupAnim GetAnim()
        {
            return popUpAnim;
        }
        #endregion
    }
}
