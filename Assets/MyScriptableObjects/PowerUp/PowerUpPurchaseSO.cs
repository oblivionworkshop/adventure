using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    [CreateAssetMenu(fileName = "PowerUpData", menuName = "ScriptableObjects/PowerUpPurchaseSO", order = 1)]
    public class PowerUpPurchaseSO : ScriptableObject
    {
        public IEnum.PowerUp powerUp;
        public LocalizedString powerUpName;
        public LocalizedString powerUpDescription;
        public Sprite powerUpImg;
        public int unlockUIStage;
    }
}
