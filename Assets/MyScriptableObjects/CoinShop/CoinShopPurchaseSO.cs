using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.OblivionWorkshop
{
    [CreateAssetMenu(fileName = "CoinShopData", menuName = "ScriptableObjects/CoinShopPurchaseSO", order = 2)]
    public class CoinShopPurchaseSO : ScriptableObject
    {
        public int coinAmount;
        public float usd;
    }
}
